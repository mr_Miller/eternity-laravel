# Eternity for Laravel

Project with components to help build services oriented structure

### Structure
Laravel packages:
- [Event Bus](src/Components/Event)
- [Localization](src/Components/Localization)
- [File manager](src/Components/File)
- [Logger](src/Components/Logger) (Laravel integration of Eternity Logger)

Laravel framework related components:
- [Exception Handler](src/Exceptions)
- [Application](src/Application.php) (Extended Laravel Application)
- [Console Input](src/Console) (Console input handler)
- [Response middleware](src/Middleware)

## Installation
##### 1. Add into `composer.json`
```jsons
"require": {
    ..........
    "eternity/laravel": "*",
    ..........
},
..........
"repositories": [
    {
        "type": "vcs",
        "url": "git@bitbucket.org:mr_Miller/eternity-laravel.git"
    },
    {
        "type": "vcs",
        "url": "git@bitbucket.org:mr_Miller/eternity.git"
    }
]
```
##### 2. Create auth.json file, don't forget to set correct `CONSUMER_KEY` and `CONSUMER_SECRET`:
```jsons
{
    "bitbucket-oauth": {
        "bitbucket.org": {
            "consumer-key": CONSUMER_KEY,
            "consumer-secret": CONSUMER_SECRET,
        }
    }
}
```
## Debug package locally
```jsons
{
    "require": {
        // * important, allows to use any branch, state from local repo
        "eternity/laravel": "*",
    },
    "minimum-stability": "dev",
    "repositories": [
        {
            "type": "path",
            // absolute path inside container or relative path
            "url": "/var/www/eternity-laravel",
            "options": {
                // symlink to folder instead of copying
                "symlink": true
            }
        }
    ]
}
```

## Changes
When applying changes Eternity package version in `composer.json` must be changed.

- `MAJOR` version when you make numerous incompatible changes or when new release is ready.
- `MINOR` version when you make backwards incompatible changes
- `PATCH` version when you add functionality in a backwards compatible manner or when you make backwards compatible bug fixes.

Example of PATCH changes: `1.1.5` -> `1.1.6`
Example of Minor changes (backwards compatibility is broken): `1.1.6` -> `1.2.0`

## Reviewers
Reviewers should set proper tag same as package version in `composer.json`
Example: 
If version is `1.2.5`, tag name must be `1.2.5`

The condition of success package updating is that the tag and version in `composer.json` must be __equal__

## Testing
To run tests:
```
php vendor/bin/phpunit tests/Unit
```
