<?php

namespace Eternity\Laravel\Metadata\Factories;

use Eternity\Definitions\HeadersDefinition;
use Eternity\Metadata\Contracts\MetadataInterface;
use Eternity\Metadata\Metadata;
use Eternity\Resource\Objects\Filter\FilterCollection;
use Eternity\Resource\Objects\Order;
use Eternity\Resource\Objects\Page;
use Illuminate\Http\Request;

/**
 * Class MetadataFactory
 * @package Eternity\Laravel\Metadata\Factories
 */
class MetadataFactory extends AbstractMetadataFactory
{
    /**
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * @var string|null Filters serialized in Json. Filter is nullable.
     */
    protected $filterJson;

    /**
     * @var string|null
     */
    protected $orderDirection;

    /**
     * @var string|null
     */
    protected $orderBy;

    /**
     * @var string|null
     */
    protected $pageSize;

    /**
     * @var string|null
     */
    protected $pageCurrent;

    /**
     * AnalyticsMetadataFactory constructor.
     * @param \Illuminate\Http\Request $request
     * @throws \Eternity\Laravel\Metadata\Exceptions\ValidationException
     */
    public function __construct(Request $request)
    {
        $this->request = $request;

        // Parse request data
        // Filter
        $this->filterJson = $this->request->header(HeadersDefinition::FILTER);

        // Order
        $this->orderDirection = $this->request->header(
            HeadersDefinition::ORDER_DIRECTION,
            $this->getDefaultOrderDirection()
        );
        $this->orderBy = $this->request->header(
            HeadersDefinition::ORDER_BY,
            $this->getDefaultOrderBy()
        );

        // Page
        $this->pageSize = $this->request->header(HeadersDefinition::PAGE_SIZE);
        $this->pageCurrent = $this->request->header(HeadersDefinition::PAGE_CURRENT);

        // Validate data
        $this->validate();
    }

    /**
     * Creates Metadata
     * @return \Eternity\Metadata\Contracts\MetadataInterface|\Eternity\Metadata\Metadata
     * @throws \Eternity\Resource\Exceptions\MetadataException
     */
    public function create(): MetadataInterface
    {
        return new Metadata(
            new FilterCollection(json_decode($this->filterJson, true) ?: []),
            new Order($this->orderDirection, $this->orderBy),
            new Page($this->pageSize, $this->pageCurrent)
        );
    }

    /**
     * Creates Metadata statically
     * @param \Illuminate\Http\Request|null $request
     * @return \Eternity\Metadata\Contracts\MetadataInterface|\Eternity\Metadata\Metadata
     * @throws \Eternity\Laravel\Metadata\Exceptions\ValidationException
     * @throws \Eternity\Resource\Exceptions\MetadataException
     */
    public static function createStatic(Request $request = null): MetadataInterface
    {
        if ($request === null) {
            $request = \Illuminate\Support\Facades\Request::instance();
        }

        $factory = new static($request);
        return $factory->create();
    }

    /**
     *
     * @throws \Eternity\Laravel\Metadata\Exceptions\ValidationException
     */
    public function validate(): void
    {
        $this->validateFilter($this->filterJson);
        $this->validateCurrentPage($this->pageCurrent);
        $this->validatePageSize($this->pageSize);
        $this->validateOrderDirection($this->orderDirection);
    }

}