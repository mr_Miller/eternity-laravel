<?php

namespace Eternity\Laravel\Metadata\Factories;

use Eternity\Definitions\HeadersDefinition;
use Eternity\Metadata\Contracts\MetadataInterface;
use Eternity\Metadata\FilterMetadata;
use Eternity\Resource\Objects\Filter\FilterCollection;
use Illuminate\Http\Request;

/**
 * Class FilterMetadataFactory
 * @package Eternity\Laravel\Metadata\Factories
 */
class FilterMetadataFactory extends AbstractMetadataFactory
{
    /**
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * @var string|null Filters serialized in Json. Filter is nullable.
     */
    private $filterJson;

    /**
     * AnalyticsMetadataFactory constructor.
     * @param \Illuminate\Http\Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        // Parse request data
        $this->filterJson = $this->request->header(HeadersDefinition::FILTER);

        // Validate data
        $this->validate();
    }

    /**
     * Creates AnalyticsMetadata
     *
     * @return \Eternity\Metadata\Contracts\MetadataInterface|\Eternity\Metadata\AnalyticsMetadata
     * @throws \Eternity\Resource\Exceptions\MetadataException
     */
    public function create(): MetadataInterface
    {
        return new FilterMetadata(
            new FilterCollection(json_decode($this->filterJson, true) ?: [])
        );
    }

    /**
     * Creates AnalyticsMetadata statically
     *
     * @param \Illuminate\Http\Request|null $request
     * @return \Eternity\Metadata\Contracts\MetadataInterface|\Eternity\Metadata\AnalyticsMetadata
     * @throws \Eternity\Resource\Exceptions\MetadataException
     */
    public static function createStatic(Request $request = null): MetadataInterface
    {
        if ($request === null) {
            $request = \Illuminate\Support\Facades\Request::instance();
        }

        $factory = new static($request);
        return $factory->create();
    }

    /**
     * Validation
     */
    public function validate(): void
    {
        $this->validateFilter($this->filterJson);
    }
}