<?php

namespace Eternity\Laravel\Metadata\Factories;

use Eternity\Laravel\Exceptions\Codes;
use Eternity\Laravel\Metadata\Exceptions\ValidationException;
use Eternity\Metadata\Contracts\MetadataInterface;
use Eternity\Resource\Objects\Filter\Type;

/**
 * Class AbstractMetadataFactory
 * @package Eternity\Laravel\Metadata\Factories
 */
abstract class AbstractMetadataFactory
{
    private const ASCENDING_DIRECTION = 'ASC';
    private const DESCENDING_DIRECTION = 'DESC';
    private const MAXIMUM_PAGE_SIZE = 50;
    private const DEFAULT_PAGE_SIZE = 10;
    private const ALLOWED_DIRECTIONS = [
        self::ASCENDING_DIRECTION,
        self::DESCENDING_DIRECTION,
    ];
    private const DEFAULT_CURRENT_PAGE = 1;

    /**
     * @return \Eternity\Metadata\Contracts\MetadataInterface
     */
    abstract public function create(): MetadataInterface;

    /**
     * This method must throw exception
     *
     * @param string|null $filterJson
     * @throws \Eternity\Laravel\Metadata\Exceptions\ValidationException
     */
    protected function validateFilter(?string $filterJson): void
    {
        if ($filterJson === null) {
            return;
        }
        $filters = json_decode($filterJson, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new ValidationException(0, 'Filter error', 'Incorrect json value');
        }
        if (!is_array($filters)) {
            throw new ValidationException(0, 'Filter error', 'Filter must be an array');
        }
        foreach ($filters as $filter) {
            if (isset($filter['type']) && !in_array($filter['type'], $this->getAllowedFilterTypes())) {
                throw new ValidationException(
                    Codes::METADATA_FILTER_TYPE_INCORRECT,
                    'Filter error',
                    'Incorrect filter type. Use these: ' . implode(',', $this->getAllowedFilterTypes())
                );
            }
        }
    }

    /**
     * Checks order direction
     * @param string|null $orderDirection
     * @return void
     * @throws \Eternity\Laravel\Metadata\Exceptions\ValidationException
     */
    protected function validateOrderDirection(?string $orderDirection): void
    {
        if ($orderDirection
            && !in_array($orderDirection, $this->getAllowedOrderDirections())
        ) {
            throw new ValidationException(
                Codes::METADATA_ORDER_INVALID,
                'Metadata validation error',
                'Invalid order direction'
            );
        }
    }

    /**
     * Checks current page
     * @param string|null $pageCurrent
     * @throws \Eternity\Laravel\Metadata\Exceptions\ValidationException
     */
    protected function validateCurrentPage(?string $pageCurrent): void
    {
        if ($pageCurrent === null) {
            throw new ValidationException(
                Codes::METADATA_CURRENT_PAGE_MISSING_ERROR,
                'Metadata validation error',
                'Current page is missing'
            );
        }
        if (!$this->isInteger($pageCurrent)) {
            throw new ValidationException(
                0,
                'Metadata validation error',
                'Current page must be integer'
            );
        }
    }

    /**
     * Checks page size
     * @param string|null $pageSize
     * @throws \Eternity\Laravel\Metadata\Exceptions\ValidationException
     */
    protected function validatePageSize(?string $pageSize): void
    {
        if ($pageSize === null) {
            throw new ValidationException(
                Codes::METADATA_PAGE_MISSING_ERROR,
                'Metadata validation error',
                'Page size is missing'
            );
        }
        if (!$this->isInteger($pageSize)) {
            throw new ValidationException(
                0,
                'Metadata validation error',
                'Page size must be integer'
            );
        }

        if ($pageSize > $this->getMaximumPageSize()) {
            throw new ValidationException(
                Codes::METADATA_PAGE_SIZE_ERROR,
                'Metadata validation error',
                'Page size exceeded limit of ' . $this->getMaximumPageSize()
            );
        }
    }

    /**
     * Checks value for integer
     * @param $value
     * @return bool
     */
    private function isInteger($value): bool
    {
        return(ctype_digit(strval($value)));
    }

    /**
     * Return maximum page size
     *
     * Override function to change value
     * @return int
     */
    public function getMaximumPageSize(): int
    {
        return self::MAXIMUM_PAGE_SIZE;
    }

    /**
     * Returns list of allowed filter types
     * @return array
     */
    public function getAllowedFilterTypes(): array
    {
        return Type::list();
    }

    /**
     * Returns list of allowed Order Directions
     * @return string[]
     */
    public function getAllowedOrderDirections(): array
    {
        return self::ALLOWED_DIRECTIONS;
    }

    /**
     * Return default current page number
     * @return int
     */
    public function getDefaultCurrentPage(): int
    {
        return self::DEFAULT_CURRENT_PAGE;
    }

    /**
     * Returns default page size
     *
     * Override function to change value
     *
     * @return int
     */
    public function getDefaultPageSize(): int
    {
        return self::DEFAULT_PAGE_SIZE;
    }

    /**
     * Returns default By
     * @return string
     */
    public function getDefaultOrderBy(): string
    {
        return 'id';
    }

    /**
     * Returns default Direction
     * @return string
     */
    public function getDefaultOrderDirection(): string
    {
        return self::DESCENDING_DIRECTION;
    }
}