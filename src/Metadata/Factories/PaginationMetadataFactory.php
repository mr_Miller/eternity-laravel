<?php

namespace Eternity\Laravel\Metadata\Factories;

use Eternity\Metadata\Contracts\MetadataInterface;
use Eternity\Metadata\Metadata;
use Eternity\Metadata\PaginationMetadata;
use Eternity\Resource\Objects\Pagination;

/**
 * Class PaginatedMetadataFactory
 * @package Eternity\Laravel\Metadata\Factories
 */
class PaginationMetadataFactory extends AbstractMetadataFactory
{
    /**
     * @var \Eternity\Metadata\Metadata
     */
    private $metadata;

    /**
     * @var \Eternity\Resource\Objects\Pagination
     */
    private $pagination;

    /**
     * AnalyticsMetadataFactory constructor.
     * @param \Eternity\Metadata\Metadata $metadata
     * @param \Eternity\Resource\Objects\Pagination $pagination
     */
    public function __construct(Metadata $metadata, Pagination $pagination)
    {
        $this->metadata = $metadata;
        $this->pagination = $pagination;
    }

    /**
     * @return \Eternity\Metadata\Contracts\MetadataInterface|PaginationMetadata
     */
    public function create(): MetadataInterface
    {
        return new PaginationMetadata(
            $this->metadata->filter(),
            $this->metadata->order(),
            $this->metadata->page(),
            $this->pagination
        );
    }

    /**
     * @param \Eternity\Metadata\Metadata $metadata
     * @param \Eternity\Resource\Objects\Pagination $pagination
     * @return \Eternity\Metadata\Contracts\MetadataInterface|PaginationMetadata
     */
    public static function createStatic(Metadata $metadata, Pagination $pagination): MetadataInterface
    {
        return (new static($metadata, $pagination))->create();
    }

}