### Required components:
- Http Client;
- Trace ID;
- Localization;

To publish all required components:

##### 1. Execute publish command to publish the components:
```
php artisan vendor:publish --tag=eternity
```
##### 2. Add facades to config/app in facades section after laravel facades.
```
'aliases' => [
    // Default facades
    ...
    // Custom facades    
    'TraceId' => Eternity\Laravel\Facades\TraceId::class,
]
```

#### Application core
Replace `\Illuminate\Foundation\Application` with this application class in `bootstrap\app.php` as showed below:

```php
$app = new \Eternity\Laravel\Application(
    $_ENV['APP_BASE_PATH'] ?? dirname(__DIR__)
);
```

All file must be like:
```php
<?php

$app = new \Eternity\Laravel\Application(
    $_ENV['APP_BASE_PATH'] ?? dirname(__DIR__)
);

$app->singleton(
    Illuminate\Contracts\Http\Kernel::class,
    App\Interfaces\Http\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Interfaces\Console\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Application\Exceptions\Handler::class
);

return $app;

```