<?php

namespace Eternity\Laravel\Middleware;

use Closure;
use Illuminate\Http\Response;

/**
 * Response middleware
 *
 * Middleware responsible preparing response
 *
 * Class ResponseMiddleware
 * @package App\Application\Middleware
 */
class ResponseMiddleware
{
    /**
     * @var array Array of routes that middleware ignores
     */
    protected $ignoredRoutes = [
        '/v1/subscription/subscriptions/apple/notify'
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if ($this->isIgnoredRoute($request->getRequestUri())) {
            return $response;
        }

        if ($response instanceof Response && $this->isContentEmpty($response)) {
            // send response with status 204 (No content) if Response has empty content
            $response->setStatusCode(Response::HTTP_NO_CONTENT);
        }

        // For unexpected output return response as it is
        return $response;
    }

    /**
     * Checks if route is ignored
     * @param string $route
     * @return bool
     */
    protected function isIgnoredRoute(string $route): bool
    {
        return in_array($route, $this->ignoredRoutes);
    }

    /**
     * Checks if content is empty
     * @param \Illuminate\Http\Response $response
     * @return bool
     */
    protected function isContentEmpty(Response $response): bool
    {
        return $response->content() === '' || $response->content() === null;
    }
}
