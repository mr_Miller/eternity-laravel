## ResponseMiddleware
Class reference
```
Eternity\Middleware\ResponseMiddleware
```

Automatic response handling. For purpose to response formatting dut to documentation. For example adding HTTP status code 204 on empty response body.

#### Usage
Register middleware as route middleware in `Kernel.php` and assign it to route group. As the result middleware will handle all outgoing responses of that route group. 
By default `Kernel.php` located in `app\Http\Kernel.php`.

1. Register route middle ware
```php
    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'response' => \Eternity\Middleware\ResponseMiddleware::class,
    ];
``` 

1. Add to route group
```php
    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'api-v1' => [
            'response',
        ],
    ];
```
