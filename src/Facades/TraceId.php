<?php

namespace Eternity\Laravel\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class TraceId
 *
 * @method static string value()
 * @method static void init(string $value)
 * @method static void generate(string $prefix = 'dev')
 *
 * @package Eternity\Laravel\Facades
 */
class TraceId extends Facade
{
    /**
     * @return string|void
     */
    protected static function getFacadeAccessor()
    {
        return 'traceId';
    }
}