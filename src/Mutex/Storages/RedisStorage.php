<?php

namespace Eternity\Laravel\Mutex\Storages;

use Eternity\Mutex\Contracts\Storage;
use Eternity\Mutex\Exceptions\LockStorageException;
use Eternity\Mutex\Token;
use Illuminate\Contracts\Redis\Connection;

/**
 * Class RedisStorage
 * @package Eternity\Mutex\Storages
 */
class RedisStorage implements Storage
{
    /**
     * @var \Illuminate\Contracts\Redis\Connection
     */
    private $client;

    /**
     * RedisStore constructor.
     *
     * @param \Illuminate\Contracts\Redis\Connection|\Illuminate\Redis\Connections\PhpRedisConnection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->client = $connection;
    }

    /**
     * @inheritDoc
     */
    public function ttl(Token $token): ?int
    {
        return $this->client->ttl($token->name());
    }

    /**
     * Saves record in storage
     *
     * @param \Eternity\Mutex\Token $token
     *
     * @throws \Eternity\Mutex\Exceptions\LockStorageException
     */
    public function store(Token $token): void
    {
        $script = <<<'LUA'
            if redis.call("GET", KEYS[1]) == ARGV[1] then
                return redis.call("PEXPIRE", KEYS[1], ARGV[2])
            elseif redis.call("SET", KEYS[1], ARGV[1], "NX", "PX", ARGV[2]) then
                return 1
            else
                return 0
            end
        LUA;

        if (! $this->evaluate($script, $token->name(), [$token->value(), $token->timeout() * 1000]))
        {
            throw new LockStorageException(sprintf('Cannot store mutex %s', $token->name()));
        }
    }

    /**
     * Removes mutex from storage
     *
     * @param \Eternity\Mutex\Token $token
     */
    public function delete(Token $token): void
    {
        $script = <<<'LUA'
            if redis.call("GET", KEYS[1]) == ARGV[1] then
                return redis.call("DEL", KEYS[1])
            else
                return 0
            end
        LUA;

        $this->evaluate($script, $token->name(), [$token->value()]);
    }

    /**
     * Check if exists
     *
     * @param \Eternity\Mutex\Token $token
     *
     * @return bool
     */
    public function exists(Token $token): bool
    {
        return $this->client->get($token->name()) === $token->value();
    }

    /**
     * Updates an expiration timeout for the key
     * @param \Eternity\Mutex\Token $token
     *
     * @throws \Eternity\Mutex\Exceptions\LockStorageException
     */
    public function update(Token $token): void
    {
        $script = <<<'LUA'
            if redis.call("GET", KEYS[1]) == ARGV[1] then
                return redis.call("PEXPIRE", KEYS[1], ARGV[2])
            else
                return 0
            end
        LUA;

        if (! $this->evaluate($script, $token->name(), [$token->value(), $token->timeout() * 1000]))
        {
            throw new LockStorageException(sprintf('Cannot update expiration for resource: %s', $token->name()));
        }
    }

    /**
     * Evaluates a script in the corresponding redis client.
     *
     * @param string $script
     * @param string $resource
     * @param array $arguments
     *
     * @return mixed
     */
    private function evaluate(string $script, string $resource, array $arguments)
    {
        return $this->client->eval($script, 1, $resource, ...$arguments);
    }
}
