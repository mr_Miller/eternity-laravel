<?php

namespace Eternity\Laravel\Contracts;

use Illuminate\Support\Collection;

/**
 * Interface for pagination transferring between the layers
 *
 * Class PaginatorInterface
 * @package Eternity\Laravel\Contracts
 */
interface PaginatorInterface
{
    /**
     * Returns number of items on page
     * @return int
     */
    public function getPageSize(): int;

    /**
     * Returns current page number
     * @return int
     */
    public function getCurrentPage(): int;

    /**
     * Returns overall count of items in storage
     * @return int
     */
    public function getTotalItems(): int;

    /**
     * Returns overall count of pages
     * @return int
     */
    public function getTotalPages(): int;

    /**
     * Returns collection of items on current page
     * @return \Illuminate\Support\Collection
     */
    public function getItems(): Collection;
}