<?php

namespace Eternity\Laravel\Factories;

use Eternity\Exceptions\EternityException;
use Eternity\Exceptions\UnknownException;
use Eternity\Exceptions\ValidationException as EternityValidationException;
use Eternity\Http\Exceptions\ResponseException;
use Eternity\Laravel\Factories\Exceptions\EmptyBodyException;
use Eternity\Logger\Messages\ExceptionLogMessage;
use Eternity\Logger\Messages\ValidationLogMessage;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

/**
 * Response factory
 *
 * Config:
 * app.debug = true    -   For outputting error trace
 *
 * Class ResponseFactory
 * @property \Throwable exception
 * @package App\Application\Components
 */
final class ErrorResponseFactory
{
    /**
     * Errors content's section
     */
    private const ERRORS = 'errors';

    /**
     * Error trace
     * Only when debug is enabled
     */
    private const TRACE = 'trace';

    /**
     * @var array
     */
    private $errors = [];

    /**
     * If to show error trace
     * @return bool
     */
    private function showTrace(): bool
    {
        return config('app.debug') === true;
    }

    /**
     * Returns errors
     * @return array
     */
    private function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * Adds error
     * @param array $error
     */
    private function addError(array $error): void
    {
        $this->errors[] = $error;
    }

    /**
     * Builds response content
     * @param \Eternity\Exceptions\EternityException $exception
     * @param array|null $trace
     * @return array
     */
    private function buildContent(EternityException $exception, array $trace = null): array
    {
        $this->addError($this->prepareError($exception));
        $content[self::ERRORS] = $this->getErrors();
        // Add trace if you need
        if ($this->showTrace()) {
            if ($trace === null) {
                // If trace is not provided, build trace from current exception
                $trace = $this->buildTrace($exception);
            }
            $content[self::TRACE] = $trace;
        }

        return $content;
    }

    /**
     * @param \Eternity\Exceptions\EternityException $exception
     * @return array
     */
    private function prepareError(EternityException $exception): array
    {
        $error = $exception->serialize();
        if ($exception->getTitle() === null) {
            $error['title'] = $exception->getType();
        }

        return $error;
    }

    /**
     * Build error response from common exception
     * @param \Throwable $exception
     * @return \Illuminate\Http\JsonResponse
     */
    public function createFromUnknown(\Throwable $exception): JsonResponse
    {
        $unknownException = new UnknownException(get_class($exception), $exception->getMessage());
        Log::error(new ExceptionLogMessage($unknownException));

        return response()->json(
            $this->buildContent($unknownException, $this->buildTrace($exception)),
            $unknownException->getStatus()
        );
    }

    /**
     * Build error response from Eternity Exception
     * @param \Eternity\Exceptions\EternityException $exception
     * @return \Illuminate\Http\JsonResponse
     */
    public function createFromEternity(EternityException $exception): JsonResponse
    {
        return response()->json($this->buildContent($exception), $exception->getStatus());
    }

    /**
     * Build error response from Response Exception
     * @param \Eternity\Http\Exceptions\ResponseException $exception
     * @return \Illuminate\Http\JsonResponse
     */
    public function createFromResponse(ResponseException $exception): JsonResponse
    {
        if ($exception->getResponse()->getBody() === null) {
            // For situation on remote service returned unexpected HTTP status and empty body in stead of error,
            // possibly remote service is down.
            $emptyBodyException = new EmptyBodyException(
                'Remote service error',
                'Response error body is missing. Remote service probably is down.'
            );

            return response()->json($this->buildContent($emptyBodyException, $this->buildTrace($exception)), 500);
        }

        // Proxy remote service error
        return response()->json($exception->getResponse()->getBody(), $exception->getResponse()->getHttpStatus());
    }

    /**
     * Build error response from Laravel Validation Exception
     * @param \Illuminate\Validation\ValidationException $exception
     * @return \Illuminate\Http\JsonResponse
     */
    public function createFromValidation(ValidationException $exception): JsonResponse
    {
        $eternityValidation = new EternityValidationException($this->prepareValidationErrors($exception));
        Log::error(new ValidationLogMessage($eternityValidation));

        return response()->json($this->buildContent($eternityValidation), $eternityValidation->getStatus());
    }

    /**
     * Formatting errors to proper response structure
     *
     * Input:
     * [
     *     "field name" => ["Error"],
     * ]
     * Output:
     * [
     *     [
     *         "field" => "field name",
     *         "errors" => ["Error"],
     *     ]
     * ]
     *
     * @param \Illuminate\Validation\ValidationException $exception
     * @return array
     */
    private function prepareValidationErrors(ValidationException $exception): array
    {
        $result = [];
        foreach ($exception->errors() as $fieldName => $validationErrors) {
            /** @var array $errors */
            $result[] = [
                'field'  => $fieldName,
                'errors' => $validationErrors,
            ];
        }

        return $result;
    }

    /**
     * Builds trace
     * @param \Throwable $e
     * @return array
     */
    private function buildTrace(\Throwable $e): array
    {
        return [
            'file'  => $e->getFile(),
            'line'  => $e->getLine(),
            'trace' => $e->getTrace(), // todo Check if there will be no problems with printing function args to output
        ];
    }
}
