<?php

namespace Eternity\Laravel\Factories\Exceptions;

use Eternity\Exceptions\ServerException;

/**
 * In case if error is not present in body
 *
 * Class EmptyBodyException
 * @package Eternity\Laravel\Factories\Exceptions
 */
class EmptyBodyException extends ServerException
{

}