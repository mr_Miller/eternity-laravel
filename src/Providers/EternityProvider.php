<?php

namespace Eternity\Laravel\Providers;

use Eternity\Http\Client;
use Eternity\Http\GuzzleFactory;
use Eternity\Laravel\Components\Configurator\Services\ConfigurationService;
use Eternity\Laravel\Components\Event\Dispatcher\EventDispatcher;
use Eternity\Laravel\Components\Event\Dispatcher\States\DeferredState;
use Eternity\Laravel\Components\Event\EventBus;
use Eternity\Laravel\Components\Localization\Localization;
use Eternity\Laravel\Components\Redis\RedisManager;
use Eternity\Laravel\Components\Trace\Factories\TraceIdFactory;
use Eternity\Laravel\Console\Commands\Configurator\EnvToRedisCommand;
use Eternity\Laravel\Console\Commands\Configurator\MakeEnv;
use Eternity\Laravel\Security\Keeper\Console\KeeperCommand;
use Eternity\Microservices;
use Illuminate\Database\Events\TransactionBeginning;
use Illuminate\Database\Events\TransactionCommitted;
use Illuminate\Database\Events\TransactionRolledBack;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;

/**
 * Class EternityProvider
 * @package Eternity\Laravel\Providers
 */
class EternityProvider extends ServiceProvider
{
    /**
     * Register special Eternity Services
     *
     *  - Http Client
     *  - Trace ID
     *  - Http Logger
     */
    public function register()
    {
        $this->checkMicroserviceConfig();
        $this->registerCommands();

        $this->initLocalization();
        $this->initTraceId();
        $this->initHttpClient();
        $this->extendRedisManager();
        $this->registerTransactionListeners();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../Components/Localization/Config/localization.php' => config_path('localization.php'),
            __DIR__ . '/../Config/microservice.php'                         => config_path('microservice.php'),
        ], 'eternity');

        app(ConfigurationService::class)->loadMobileAppConfiguration();
    }

    /**
     * Register service's commands with artisan.
     *
     * @return void
     */
    protected function registerCommands(): void
    {
        $this->commands(EnvToRedisCommand::class);
        $this->commands(MakeEnv::class);
        $this->commands(KeeperCommand::class);
    }

    /**
     * Check microservice config
     */
    private function checkMicroserviceConfig(): void
    {
        if (config('microservice.name') === null) {
            error_log(PHP_EOL . 'Microservice property "name" is not set in "config/microservice.php"' . PHP_EOL);
        }
    }

    /**
     * Init Http Client
     *
     * For messaging between microservices
     */
    private function initHttpClient(): void
    {
        $this->app->singleton(Client::class, function () {
            return GuzzleFactory::make(config('http-client.options'));
        });
    }

    /**
     * Init Trace ID
     */
    private function initTraceId(): void
    {
        $this->app->singleton('traceId', function () {
            // If region is set use current region in logger
            $region = config('localization.region', '');
            $traceIdFactory = new TraceIdFactory(
                config('app.env') . '_' . $region,
                $this->app->mode()
            );

            // For core microservice
            if (config('microservice.name') === Microservices::GATEWAY) {
                return $traceIdFactory->createCore();
            }

            return $traceIdFactory->create();
        });
    }

    /**
     * Init Localization
     */
    private function initLocalization(): void
    {
        $this->app->singleton(Localization::class, Localization::class);
    }

    /**
     * Redeclare Redis Manager
     */
    private function extendRedisManager(): void
    {
        $this->app->extend('redis', function ($service, $app) {
            $config = $app->make('config')->get('database.redis', []);

            return new RedisManager($app, Arr::pull($config, 'client', 'phpredis'), $config);
        });
    }

    /**
     * Register event listeners on Db events
     *
     * Dispatches queued events on DB transaction commit
     */
    private function registerTransactionListeners(): void
    {
        Event::listen(TransactionBeginning::class, function () {
            if ($this->isDispatcherInitiated()) {
                $eventBus = $this->app->make(EventBus::class);
                $this->app->make(EventDispatcher::class)->changeState(new DeferredState($eventBus));
            }
        });
        Event::listen(TransactionCommitted::class, function () {
            if ($this->isDispatcherInitiated()) {
                $this->app->make(EventDispatcher::class)->dispatch();
            }
        });
        Event::listen(TransactionRolledBack::class, function () {
            if ($this->isDispatcherInitiated()) {
                $this->app->make(EventDispatcher::class)->clear();
            }
        });
    }

    /**
     * @return bool
     */
    private function isDispatcherInitiated(): bool
    {
        return $this->app->bound(EventBus::class) && $this->app->bound(EventDispatcher::class);
    }
}