<?php

namespace Eternity\Laravel\Rbac\Console;

use Eternity\Laravel\Rbac\Database\Models;
use Illuminate\Support\Facades\DB;

/**
 * Class ConfigureCommand
 * @package Eternity\Laravel\Rbac\Console
 */
class ConfigureCommand extends AbstractSetupCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rbac:configure';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Configure RBAC with predefined settings. All current settings will be overridden.';

    /**
     * Truncate rbac tables
     *
     * @return void
     */
    protected function cleanup(): void
    {
        $this->comment('Cleanup rbac tables');

        DB::transaction(function () {
            DB::table(Models::table('assigned_roles'))->truncate();
            DB::table(Models::table('permissions'))->truncate();
            DB::table(Models::table('abilities'))->truncate();
            DB::table(Models::table('roles'))->truncate();
        });

        $this->comment('RBAC tables cleaned');
    }

    /**
     * Apllies RBAC configuration
     *
     * @param array $config
     *
     * @throws \Throwable
     */
    protected function apply(array $config)
    {
        $this->comment('Applying "rbac" configuration');

        $this->importAbilities($config[self::ABILITIES]);
        $this->importRoles($config[self::ROLES]);
        $this->importAssignments($config[self::ASSIGNMENTS]);

        $this->comment('RBAC configuration applied');
    }
}
