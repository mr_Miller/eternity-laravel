<?php

namespace Eternity\Laravel\Rbac\Console;

use Eternity\Laravel\Rbac\Inspector;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

/**
 * Class AbstractConfiguration
 * @package Eternity\Laravel\Rbac\Console
 */
abstract class AbstractSetupCommand extends Command
{
    protected const ROLES = 'roles';
    protected const ABILITIES = 'abilities';
    protected const ASSIGNMENTS = 'assignments';

    /**
     * @var \Eternity\Laravel\Rbac\Inspector
     */
    protected $inspector;

    /**
     * Apllies RBAC configuration
     *
     * @param array $config
     *
     * @throws \Throwable
     */
    abstract protected function apply(array $config);

    /**
     * Truncate rbac tables
     */
    abstract protected function cleanup(): void;

    /**
     * Execute the console command.
     * @return void
     * @throws \Throwable
     */
    public function handle(): void
    {
        $config = Config::get('rbac', []);

        if (!$this->checkConfig($config)) {
            return;
        }

        $this->cleanup();
        $this->apply($config);
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->inspector = Inspector::create();
    }

    /**
     * Validate rbac configuration
     * @param array $config
     *
     * @return bool
     */
    protected function checkConfig(array $config = []): bool
    {
        $check = true;

        if (!file_exists(config_path() . '/rbac.php')) {
            $check = false;
            $this->error('RBAC configuration does not exist');
        }

        if (empty($config)) {
            $check = false;
            $this->error('RBAC configuration could not be empty');
        }

        if (empty($config[self::ABILITIES])) {
            $check = false;
            $this->error('RBAC configuration does not contain "abilities"');
        }

        if (empty($config[self::ROLES])) {
            $check = false;
            $this->error('RBAC configuration does not contain "roles"');
        }

        if (empty($config[self::ASSIGNMENTS])) {
            $check = false;
            $this->error('RBAC configuration does not contain "assignments"');
        }

        return $check;
    }

    /**
     * Import abilities
     * @param array $abilities
     *
     * @throws \Throwable
     */
    protected function importAbilities(array $abilities): void
    {
        $this->comment('Import "rbac" abilities');

        DB::transaction(function () use ($abilities) {
            foreach ($abilities as $a) {
                $this->inspector->ability($a)->save();
            }
        });

        $this->comment('Imported abilities: ' . count($abilities));
    }

    /**
     * Import roles
     * @param $roles
     *
     * @throws \Throwable
     */
    protected function importRoles($roles): void
    {
        $this->comment('Import "rbac" roles');

        DB::transaction(function () use ($roles) {
            foreach ($roles as $r) {
                $this->inspector->role($r)->save();
            }
        });

        $this->comment('Imported roles: ' . count($roles));
    }

    /**
     * Updates roles
     * @param $roles
     * @return void
     */
    protected function importAddedRoles($roles): void
    {
        $this->comment('Import new "rbac" roles');

        $counterAddedRoles = DB::transaction(function () use ($roles) {
            $counterAddedRoles = 0;
            foreach ($roles as $r) {
                $role = $this->inspector->role($r);
                $hasRole = $role::query()->where('name', '=', $r['name'])->exists();
                if (!$hasRole) {
                    $counterAddedRoles++;
                    $role->save();
                }
            }

            return $counterAddedRoles;
        });

        $this->comment('Imported new roles: ' . $counterAddedRoles);
    }

    /**
     * Configure assignments between roles and abilities
     * @param $assignments
     *
     * @throws \Throwable
     */
    protected function importAssignments($assignments): void
    {
        $this->comment('Import "assignments" roles');

        DB::transaction(function () use ($assignments) {
            foreach ($assignments as $role => $rules) {
                if (isset($rules['everything']) && $rules['everything']) {
                    $this->inspector->allow($role)->everything();
                }

                if (isset($rules['own_everything']) && $rules['own_everything']) {
                    $this->inspector->allow($role)->toOwnEverything();
                }

                if (!empty($rules['to'])) {
                    $this->inspector->allow($role)->to($rules['to']);
                }

                if (!empty($rules['own'])) {
                    $this->inspector->allow($role)->toOwn($rules['own']);
                }

                if (!empty($rules['manage'])) {
                    $this->inspector->allow($role)->toManage($rules['manage']);
                }
            }
        });

        $this->comment('Assignments configured');
    }
}
