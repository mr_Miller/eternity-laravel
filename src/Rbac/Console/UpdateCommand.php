<?php

namespace Eternity\Laravel\Rbac\Console;

use Eternity\Laravel\Rbac\Database\Models;
use Illuminate\Support\Facades\DB;

/**
 * Class UpdateAbilities
 * @package Eternity\Laravel\Rbac\Console
 */
class UpdateCommand extends AbstractSetupCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rbac:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update abilities and permissions and import new roles';

    /**
     * Truncate rbac tables
     *
     * @return void
     */
    protected function cleanup(): void
    {
        $this->comment('Cleanup "permissions" and "abilities" tables');

        DB::transaction(function () {
            DB::table(Models::table('permissions'))->truncate();
            DB::table(Models::table('abilities'))->truncate();
        });

        $this->comment('Tables cleaned');
    }

    /**
     * Update abilities and permissions
     *
     * @param array $config
     *
     * @throws \Throwable
     */
    protected function apply(array $config)
    {
        $this->comment('Updating "rbac" configuration');

        $this->importAddedRoles($config[self::ROLES]);
        $this->importAbilities($config[self::ABILITIES]);
        $this->importAssignments($config[self::ASSIGNMENTS]);

        $this->comment('RBAC configuration updated');
    }
}