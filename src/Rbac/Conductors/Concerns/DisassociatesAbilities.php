<?php

namespace Eternity\Laravel\Rbac\Conductors\Concerns;

use Eternity\Laravel\Rbac\Database\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

/**
 * Trait DisassociatesAbilities
 * @package Eternity\Laravel\Rbac\Conductors\Concerns
 */
trait DisassociatesAbilities
{
    use ConductsAbilities, FindsAndCreatesAbilities;

    /**
     * Remove the given ability from the model.
     *
     * @param  mixed  $abilities
     * @param  \Illuminate\Database\Eloquent\Model|string|null  $entity
     * @param  array  $attributes
     * @return bool|\Eternity\Laravel\Rbac\Conductors\Lazy\ConductsAbilities
     */
    public function to($abilities, $entity = null, array $attributes = [])
    {
        if ($this->shouldConductLazy(...func_get_args())) {
            return $this->conductLazy($abilities);
        }

        if ($ids = $this->getAbilityIds($abilities, $entity, $attributes)) {
            $this->disassociateAbilities($this->getAuthority(), $ids);
        }

        return true;
    }

    /**
     * Detach the given IDs from the authority.
     *
     * @param  \Illuminate\Database\Eloquent\Model|null  $authority
     * @param  array  $ids
     * @return void
     */
    protected function disassociateAbilities($authority, array $ids): void
    {
        if ($authority === null) {
            $this->disassociateEveryone($ids);
        } else {
            $this->disassociateAuthority($authority, $ids);
        }
    }

    /**
     * Disassociate the authority from the abilities with the given IDs.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $authority
     * @param  array  $ids
     * @return void
     */
    protected function disassociateAuthority(Model $authority, array $ids): void
    {
        $this->getAbilitiesPivotQuery($authority, $ids)
             ->where($this->constraints())
             ->delete();
    }

    /**
     * Get the base abilities pivot query.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  array  $ids
     * @return \Illuminate\Database\Query\Builder
     */
    protected function getAbilitiesPivotQuery(Model $model, $ids): Builder
    {
        $relation = $model->abilities();

        $query = $relation
            ->newPivotStatement()
            ->where($relation->getQualifiedForeignPivotKeyName(), $model->getKey())
            ->where('entity_type', $model->getMorphClass())
            ->whereIn($relation->getQualifiedRelatedPivotKeyName(), $ids);

        return Models::scope()->applyToRelationQuery(
            $query, $relation->getTable()
        );
    }

    /**
     * Disassociate everyone from the abilities with the given IDs.
     *
     * @param  array  $ids
     * @return void
     */
    protected function disassociateEveryone(array $ids): void
    {
        $query = Models::query('permissions')
            ->whereNull('entity_id')
            ->where($this->constraints())
            ->whereIn('ability_id', $ids);

        Models::scope()->applyToRelationQuery($query, $query->from);

        $query->delete();
    }

    /**
     * Get the authority from which to disassociate the abilities.
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    protected function getAuthority(): ?Model
    {
        if ($this->authority === null) {
            return null;
        }

        if ($this->authority instanceof Model) {
            return $this->authority;
        }

        return Models::role()->where('name', $this->authority)->firstOrFail();
    }

    /**
     * Get the additional constraints for the detaching query.
     *
     * @return array
     */
    protected function constraints(): array
    {
        return property_exists($this, 'constraints') ? $this->constraints : [];
    }
}
