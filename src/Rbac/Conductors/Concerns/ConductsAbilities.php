<?php

namespace Eternity\Laravel\Rbac\Conductors\Concerns;

use Eternity\Laravel\Rbac\Conductors\Lazy;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

/**
 * Trait ConductsAbilities
 * @package Eternity\Laravel\Rbac\Conductors\Concerns
 * @method mixed to($abilities, $model = null, array $attributes = [])
 */
trait ConductsAbilities
{
    /**
     * Allow/disallow all abilities on everything.
     *
     * @param  array  $attributes
     * @return mixed
     */
    public function everything(array $attributes = [])
    {
        return $this->to('*', '*', $attributes);
    }

    /**
     * Allow/disallow all abilities on the given model.
     *
     * @param  string|array|\Illuminate\Database\Eloquent\Model  $models
     * @param  array  $attributes
     * @return void
     */
    public function toManage($models, array $attributes = []): void
    {
        if (is_array($models)) {
            foreach ($models as $model) {
                $this->to('*', $model, $attributes);
            }
        } else {
            $this->to('*', $models, $attributes);
        }
    }

    /**
     * Allow/disallow owning the given model.
     *
     * @param  string|object  $model
     * @param  array  $attributes
     * @return \Eternity\Laravel\Rbac\Conductors\Lazy\HandlesOwnership
     */
    public function toOwn($model, array $attributes = []): Lazy\HandlesOwnership
    {
        return new Lazy\HandlesOwnership($this, $model, $attributes);
    }

    /**
     * Allow/disallow owning all models.
     *
     * @param  array  $attributes
     * @return \Eternity\Laravel\Rbac\Conductors\Lazy\HandlesOwnership
     */
    public function toOwnEverything(array $attributes = []): Lazy\HandlesOwnership
    {
        return $this->toOwn('*', $attributes);
    }

    /**
     * Determines whether a call to "to" with the given parameters should be conducted lazily.
     *
     * @param  mixed  $abilities
     *
     * @return bool
     */
    protected function shouldConductLazy($abilities): bool
    {
        // We'll only create a lazy conductor if we got a single
        // param, and that single param is either a string or
        // a numerically-indexed array (of simple strings).
        if (func_num_args() > 1) {
            return false;
        }

        if (is_string($abilities)) {
            return true;
        }

        if (! is_array($abilities) || ! Arr::isAssoc($abilities)) {
            return false;
        }

        return (new Collection($abilities))->every('is_string');
    }

    /**
     * Create a lazy abilities conductor.
     *
     * @param $abilities
     *
     * @return \Eternity\Laravel\Rbac\Conductors\Lazy\ConductsAbilities
     */
    protected function conductLazy($abilities): Lazy\ConductsAbilities
    {
        return new Lazy\ConductsAbilities($this, $abilities);
    }
}
