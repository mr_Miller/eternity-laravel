<?php

namespace Eternity\Laravel\Rbac\Conductors;

use Eternity\Laravel\Rbac\Contracts\Clipboard;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ChecksRoles
 * @package Eternity\Laravel\Rbac\Conductors
 */
class ChecksRoles
{
    /**
     * The authority against which to check for roles.
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $authority;

    /**
     * The rbac clipboard instance.
     *
     * @var \Eternity\Laravel\Rbac\Contracts\Clipboard
     */
    protected $clipboard;

    /**
     * Constructor.
     *
     * @param \Illuminate\Database\Eloquent\Model  $authority
     * @param \Eternity\Laravel\Rbac\Contracts\Clipboard  $clipboard
     */
    public function __construct(Model $authority, Clipboard $clipboard)
    {
        $this->authority = $authority;
        $this->clipboard = $clipboard;
    }

    /**
     * Check if the authority has any of the given roles.
     *
     * @param  string  ...$roles
     * @return bool
     */
    public function a(...$roles): bool
    {
        return $this->clipboard->checkRole($this->authority, $roles, 'or');
    }

    /**
     * Check if the authority doesn't have any of the given roles.
     *
     * @param  string  ...$roles
     * @return bool
     */
    public function notA(...$roles): bool
    {
        return $this->clipboard->checkRole($this->authority, $roles, 'not');
    }

    /**
     * Alias to the "a" method.
     *
     * @param  string  ...$roles
     * @return bool
     */
    public function an(...$roles): bool
    {
        return $this->clipboard->checkRole($this->authority, $roles, 'or');
    }

    /**
     * Alias to the "notA" method.
     *
     * @param  string  ...$roles
     * @return bool
     */
    public function notAn(...$roles): bool
    {
        return $this->clipboard->checkRole($this->authority, $roles, 'not');
    }

    /**
     * Check if the authority has all of the given roles.
     *
     * @param  string  ...$roles
     * @return bool
     */
    public function all(...$roles): bool
    {
        return $this->clipboard->checkRole($this->authority, $roles, 'and');
    }
}
