<?php

namespace Eternity\Laravel\Rbac;

use Eternity\Laravel\Rbac\Clipboards\CachedClipboard;
use Eternity\Laravel\Rbac\Console\CleanCommand;
use Eternity\Laravel\Rbac\Console\ConfigureCommand;
use Eternity\Laravel\Rbac\Console\UpdateCommand;
use Eternity\Laravel\Rbac\Database\Models;
use Illuminate\Cache\ArrayStore;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Support\ServiceProvider;

/**
 * Class RbacServiceProvider
 * @package Eternity\Laravel\Rbac
 */
class RbacServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->registerService();
        $this->registerCommands();
    }

    /**
     * Bootstrap any application services.
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function boot(): void
    {
        $this->registerMorphs();
        $this->setUserModel();

        $this->registerAtGate();

        if ($this->app->runningInConsole()) {
            $this->publishMiddleware();
            $this->publishMigrations();
        }
    }

    /**
     * Register service as a singleton.
     *
     * @return void
     */
    protected function registerService(): void
    {
        $this->app->singleton(Inspector::class, function () {
            return Inspector::make()
                    ->withClipboard(new CachedClipboard(new ArrayStore))
                    ->withGate($this->app->make(Gate::class))
                    ->create();
        });
    }

    /**
     * Register service's commands with artisan.
     *
     * @return void
     */
    protected function registerCommands(): void
    {
        $this->commands(CleanCommand::class);
        $this->commands(ConfigureCommand::class);
        $this->commands(UpdateCommand::class);
    }

    /**
     * Register service's models in the relation morph map.
     *
     * @return void
     */
    protected function registerMorphs(): void
    {
        Models::updateMorphMap();
    }

    /**
     * Publish the package's middleware.
     *
     * @return void
     */
    protected function publishMiddleware(): void
    {
        $stub = __DIR__.'/Middlewares/RbacInspection.php';

        $target = app_path('Application/Middleware/RbacInspection.php');

        $this->publishes([$stub => $target], 'rbac.middleware');
    }

    /**
     * Publish the package's migrations.
     *
     * @return void
     */
    protected function publishMigrations(): void
    {
        if (class_exists('CreateRbacTables')) {
            return;
        }

        $timestamp = date('Y_m_d_His');

        $stub = __DIR__.'/files/migrations/create_rbac_tables.php';

        $target = $this->app->databasePath().'/migrations/'.$timestamp.'_create_rbac_tables.php';

        $this->publishes([$stub => $target], 'rbac.migrations');
    }

    /**
     * Set the classname of the user model to be used by rbac.
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function setUserModel(): void
    {
        Models::setUsersModel($this->getUserModel());
    }

    /**
     * Get the user model from the application's auth config.
     * @return string
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function getUserModel(): ?string
    {
        $config = $this->app->make('config');

        if (null === ($guard = $config->get('auth.defaults.guard'))) {
            return null;
        }

        if (null === ($provider = $config->get("auth.guards.{$guard}.provider"))) {
            return null;
        }

        return $config->get("auth.providers.{$provider}.model");
    }

    /**
     * Register the rbac's clipboard at the gate.
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function registerAtGate(): void
    {
        // When creating a Inspector instance thru the Factory class, it'll
        // auto-register at the gate. We already registered Inspector in
        // the container using the Factory, so now we'll resolve it.
        $this->app->make(Inspector::class);
    }
}
