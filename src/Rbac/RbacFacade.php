<?php

namespace Eternity\Laravel\Rbac;

use Illuminate\Support\Facades\Facade;

/**
 * Class RbacFacade
 * @package Eternity\Laravel\Rbac
 */
class RbacFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return Inspector::class;
    }
}
