<?php

namespace Eternity\Laravel\Rbac;

use Eternity\Laravel\Rbac\Clipboards\CachedClipboard;
use Eternity\Laravel\Rbac\Contracts\Scope;
use Eternity\Laravel\Rbac\Database\Models;
use Illuminate\Container\Container;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Cache\Repository as CacheRepository;
use Illuminate\Contracts\Cache\Store;
use Illuminate\Database\Eloquent\Model;
use RuntimeException;

/**
 * Class Inspector
 * @package Eternity\Laravel\Rbac
 */
class Inspector
{
    /**
     * The rbac guard instance.
     * @var \Eternity\Laravel\Rbac\Guard
     */
    protected $guard;

    /**
     * The access gate instance.
     * @var \Illuminate\Contracts\Auth\Access\Gate|null
     */
    protected $gate;

    /**
     * Constructor.
     *
     * @param  \Eternity\Laravel\Rbac\Guard  $guard
     */
    public function __construct(Guard $guard)
    {
        $this->guard = $guard;
    }

    /**
     * Create a new Inspector instance.
     *
     * @param  mixed  $user
     *
     * @return static
     */
    public static function create($user = null): Inspector
    {
        return static::make($user)->create();
    }

    /**
     * Create a rbac factory instance.
     *
     * @param  mixed  $user
     *
     * @return \Eternity\Laravel\Rbac\Factory
     */
    public static function make($user = null): Factory
    {
        return new Factory($user);
    }

    /**
     * Start a chain, to allow the given authority an ability.
     *
     * @param  \Illuminate\Database\Eloquent\Model|string  $authority
     *
     * @return \Eternity\Laravel\Rbac\Conductors\GivesAbilities
     */
    public function allow($authority): Conductors\GivesAbilities
    {
        return new Conductors\GivesAbilities($authority);
    }

    /**
     * Start a chain, to allow everyone an ability.
     * @return \Eternity\Laravel\Rbac\Conductors\GivesAbilities
     */
    public function allowEveryone(): Conductors\GivesAbilities
    {
        return new Conductors\GivesAbilities();
    }

    /**
     * Start a chain, to disallow the given authority an ability.
     *
     * @param  \Illuminate\Database\Eloquent\Model|string  $authority
     *
     * @return \Eternity\Laravel\Rbac\Conductors\RemovesAbilities
     */
    public function disallow($authority): Conductors\RemovesAbilities
    {
        return new Conductors\RemovesAbilities($authority);
    }

    /**
     * Start a chain, to disallow everyone the an ability.
     * @return \Eternity\Laravel\Rbac\Conductors\RemovesAbilities
     */
    public function disallowEveryone(): Conductors\RemovesAbilities
    {
        return new Conductors\RemovesAbilities();
    }

    /**
     * Start a chain, to forbid the given authority an ability.
     *
     * @param  \Illuminate\Database\Eloquent\Model|string  $authority
     *
     * @return \Eternity\Laravel\Rbac\Conductors\ForbidsAbilities
     */
    public function forbid($authority): Conductors\ForbidsAbilities
    {
        return new Conductors\ForbidsAbilities($authority);
    }

    /**
     * Start a chain, to forbid everyone an ability.
     * @return \Eternity\Laravel\Rbac\Conductors\ForbidsAbilities
     */
    public function forbidEveryone(): Conductors\ForbidsAbilities
    {
        return new Conductors\ForbidsAbilities();
    }

    /**
     * Start a chain, to unforbid the given authority an ability.
     *
     * @param  \Illuminate\Database\Eloquent\Model|string  $authority
     *
     * @return \Eternity\Laravel\Rbac\Conductors\UnforbidsAbilities
     */
    public function unforbid($authority): Conductors\UnforbidsAbilities
    {
        return new Conductors\UnforbidsAbilities($authority);
    }

    /**
     * Start a chain, to unforbid an ability from everyone.
     * @return \Eternity\Laravel\Rbac\Conductors\UnforbidsAbilities
     */
    public function unforbidEveryone(): Conductors\UnforbidsAbilities
    {
        return new Conductors\UnforbidsAbilities();
    }

    /**
     * Start a chain, to assign the given role to a model.
     *
     * @param  \Eternity\Laravel\Rbac\Database\Role|\Illuminate\Support\Collection|string  $roles
     *
     * @return \Eternity\Laravel\Rbac\Conductors\AssignsRoles
     */
    public function assign($roles): Conductors\AssignsRoles
    {
        return new Conductors\AssignsRoles($roles);
    }

    /**
     * Start a chain, to retract the given role from a model.
     *
     * @param  \Illuminate\Support\Collection|\Eternity\Laravel\Rbac\Database\Role|string  $roles
     *
     * @return \Eternity\Laravel\Rbac\Conductors\RemovesRoles
     */
    public function retract($roles): Conductors\RemovesRoles
    {
        return new Conductors\RemovesRoles($roles);
    }

    /**
     * Start a chain, to sync roles/abilities for the given authority.
     *
     * @param  \Illuminate\Database\Eloquent\Model|string  $authority
     *
     * @return \Eternity\Laravel\Rbac\Conductors\SyncsRolesAndAbilities
     */
    public function sync($authority): Conductors\SyncsRolesAndAbilities
    {
        return new Conductors\SyncsRolesAndAbilities($authority);
    }

    /**
     * Start a chain, to check if the given authority has a certain role.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $authority
     *
     * @return \Eternity\Laravel\Rbac\Conductors\ChecksRoles
     */
    public function is(Model $authority): Conductors\ChecksRoles
    {
        return new Conductors\ChecksRoles($authority, $this->getClipboard());
    }

    /**
     * Get the clipboard instance.
     * @return \Eternity\Laravel\Rbac\Contracts\Clipboard
     */
    public function getClipboard(): Contracts\Clipboard
    {
        return $this->guard->getClipboard();
    }

    /**
     * Set the clipboard instance used by rbac.
     * Will also register the given clipboard with the container.
     *
     * @param  \Eternity\Laravel\Rbac\Contracts\Clipboard
     *
     * @return $this
     */
    public function setClipboard(Contracts\Clipboard $clipboard): self
    {
        $this->guard->setClipboard($clipboard);

        return $this->registerClipboardAtContainer();
    }

    /**
     * Register the guard's clipboard at the container.
     * @return $this
     */
    public function registerClipboardAtContainer(): self
    {
        $clipboard = $this->guard->getClipboard();

        Container::getInstance()->instance(Contracts\Clipboard::class, $clipboard);

        return $this;
    }

    /**
     * Use a cached clipboard with the given cache instance.
     *
     * @param  \Illuminate\Contracts\Cache\Store|null  $cache
     *
     * @return $this
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function cache(Store $cache = null): self
    {
        $cache = $cache ?: $this->resolve(CacheRepository::class)->getStore();

        if ($this->usesCachedClipboard()) {
            $this->guard->getClipboard()->setCache($cache);

            return $this;
        }

        return $this->setClipboard(new CachedClipboard($cache));
    }

    /**
     * Fully disable all query caching.
     * @return $this
     */
    public function dontCache(): self
    {
        return $this->setClipboard(new Clipboard);
    }

    /**
     * Clear the cache.
     *
     * @param  null|\Illuminate\Database\Eloquent\Model  $authority
     *
     * @return $this
     */
    public function refresh(Model $authority = null): self
    {
        if ($this->usesCachedClipboard()) {
            $this->getClipboard()->refresh($authority);
        }

        return $this;
    }

    /**
     * Clear the cache for the given authority.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $authority
     *
     * @return $this
     */
    public function refreshFor(Model $authority): self
    {
        if ($this->usesCachedClipboard()) {
            $this->getClipboard()->refreshFor($authority);
        }

        return $this;
    }

    /**
     * Set the access gate instance.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     *
     * @return $this
     */
    public function setGate(Gate $gate): self
    {
        $this->gate = $gate;

        return $this;
    }

    /**
     * Get the gate instance.
     * @return \Illuminate\Contracts\Auth\Access\Gate|null
     */
    public function getGate(): ?Gate
    {
        return $this->gate;
    }

    /**
     * Get the gate instance. Throw if not set.
     * @return \Illuminate\Contracts\Auth\Access\Gate
     * @throws \RuntimeException
     */
    public function gate(): ?Gate
    {
        if ($this->gate === null) {
            throw new RuntimeException('The gate instance has not been set.');
        }

        return $this->gate;
    }

    /**
     * Determine whether the clipboard used is a cached clipboard.
     * @return bool
     */
    public function usesCachedClipboard(): bool
    {
        return $this->guard->usesCachedClipboard();
    }

    /**
     * Define a new ability using a callback.
     *
     * @param  string  $ability
     * @param  callable|string  $callback
     *
     * @return \Illuminate\Contracts\Auth\Access\Gate
     * @throws \InvalidArgumentException
     */
    public function define($ability, $callback): ?Gate
    {
        return $this->gate()->define($ability, $callback);
    }

    /**
     * Determine if the given ability should be granted for the current user.
     *
     * @param  string  $ability
     * @param  array|mixed  $arguments
     *
     * @return \Illuminate\Auth\Access\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function authorize($ability, $arguments = [])
    {
        return $this->gate()->authorize($ability, $arguments);
    }

    /**
     * Determine if the given ability is allowed.
     *
     * @param  string  $ability
     * @param  array|mixed  $arguments
     *
     * @return bool
     */
    public function can($ability, $arguments = []): bool
    {
        return $this->gate()->allows($ability, $arguments);
    }

    /**
     * Determine if any of the given abilities are allowed.
     *
     * @param  array  $abilities
     * @param  array|mixed  $arguments
     *
     * @return bool
     */
    public function canAny($abilities, $arguments = []): bool
    {
        return $this->gate()->any($abilities, $arguments);
    }

    /**
     * Determine if the given ability is denied.
     *
     * @param  string  $ability
     * @param  array|mixed  $arguments
     *
     * @return bool
     */
    public function cannot($ability, $arguments = []): bool
    {
        return $this->gate()->denies($ability, $arguments);
    }

    /**
     * Determine if the given ability is allowed.
     * Alias for the "can" method.
     *
     * @param  string  $ability
     * @param  array|mixed  $arguments
     *
     * @return bool
     * @deprecated
     */
    public function allows($ability, $arguments = []): bool
    {
        return $this->can($ability, $arguments);
    }

    /**
     * Determine if the given ability is denied.
     * Alias for the "cannot" method.
     *
     * @param  string  $ability
     * @param  array|mixed  $arguments
     *
     * @return bool
     * @deprecated
     */
    public function denies($ability, $arguments = []): bool
    {
        return $this->cannot($ability, $arguments);
    }

    /**
     * Get an instance of the role model.
     *
     * @param  array  $attributes
     *
     * @return \Eternity\Laravel\Rbac\Database\Role
     */
    public function role(array $attributes = []): Database\Role
    {
        return Models::role($attributes);
    }

    /**
     * Get an instance of the ability model.
     *
     * @param  array  $attributes
     *
     * @return \Eternity\Laravel\Rbac\Database\Ability
     */
    public function ability(array $attributes = []): Database\Ability
    {
        return Models::ability($attributes);
    }

    /**
     * Set Inspector to run its checks after the policies.
     *
     * @param  bool  $boolean
     *
     * @return $this
     */
    public function runAfterPolicies($boolean = true): self
    {
        $this->guard->slot($boolean ? 'after' : 'before');

        return $this;
    }

    /**
     * Register an attribute/callback to determine if a model is owned by a given authority.
     *
     * @param  string|\Closure  $model
     * @param  string|\Closure|null  $attribute
     *
     * @return $this
     */
    public function ownedVia($model, $attribute = null): self
    {
        Models::ownedVia($model, $attribute);

        return $this;
    }

    /**
     * Set the model to be used for abilities.
     *
     * @param  string  $model
     *
     * @return $this
     */
    public function useAbilityModel($model): self
    {
        Models::setAbilitiesModel($model);

        return $this;
    }

    /**
     * Set the model to be used for roles.
     *
     * @param  string  $model
     *
     * @return $this
     */
    public function useRoleModel($model): self
    {
        Models::setRolesModel($model);

        return $this;
    }

    /**
     * Set the model to be used for users.
     *
     * @param  string  $model
     *
     * @return $this
     */
    public function useUserModel($model): self
    {
        Models::setUsersModel($model);

        return $this;
    }

    /**
     * Set custom table names.
     *
     * @param  array  $map
     *
     * @return $this
     */
    public function tables(array $map): self
    {
        Models::setTables($map);

        return $this;
    }

    /**
     * Get the model scoping instance.
     *
     * @param  \Eternity\Laravel\Rbac\Contracts\Scope|null  $scope
     *
     * @return mixed
     */
    public function scope(Scope $scope = null)
    {
        return Models::scope($scope);
    }

    /**
     * Resolve the given type from the container.
     *
     * @param  string  $abstract
     * @param  array  $parameters
     *
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function resolve($abstract, array $parameters = [])
    {
        return Container::getInstance()->make($abstract, $parameters);
    }
}
