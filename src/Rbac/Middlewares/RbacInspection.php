<?php

namespace Eternity\Laravel\Rbac\Middlewares;

use Closure;
use Eternity\Laravel\Rbac\Inspector;

class RbacInspection
{
    /**
     * The rbac inspector instance.
     * @var \Eternity\Laravel\Rbac\Inspector
     */
    protected $rbac;

    /**
     * Constructor.
     *
     * @param \Eternity\Laravel\Rbac\Inspector  $rbac
     */
    public function __construct(Inspector $rbac)
    {
        $this->rbac = $rbac;
    }

    /**
     * Set the proper rbac inspector scope for the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Here you may use whatever mechanism you use in your app
        // to determine the current tenant. To demonstrate, the
        // $tenantId is set here from the user's account_id.
        $tenantId = $request->user()->account_id;

        $this->rbac->scope()->to($tenantId);

        return $next($request);
    }
}
