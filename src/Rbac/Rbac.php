<?php

namespace Eternity\Laravel\Rbac;

use Eternity\Definitions\HeadersDefinition;

/**
 * Class Rbac
 * @package Eternity\Laravel\Rbac
 */
class Rbac extends Broadcast\Rbac
{
    /**
     * Initialization
     * @return static
     * @throws \JsonException
     */
    public static function init(): self
    {
        $request = request();

        if ($header = $request->headers->get(HeadersDefinition::AUTHORIZED_FOR)) {
            return self::decrypt($header);
        }

        return static::create();
    }
}