<?php

namespace Eternity\Laravel\Rbac\Constraints;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ColumnConstraint
 * @package Eternity\Laravel\Rbac\Constraints
 */
class ColumnConstraint extends Constraint
{

    /**
     * The column on the entity against which to compare.
     *
     * @var string
     */
    protected $a;

    /**
     * The column on the authority against which to compare.
     *
     * @var mixed
     */
    protected $b;

    /**
     * Constructor.
     *
     * @param string  $a
     * @param string  $operator
     * @param mixed  $b
     */
    public function __construct($a, $operator, $b)
    {
        $this->a = $a;
        $this->b = $b;
        $this->operator = $operator;
    }

    /**
     * Determine whether the given entity/authority passes the constraint.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $entity
     * @param  \Illuminate\Database\Eloquent\Model|null  $authority
     * @return bool
     */
    public function check(Model $entity, Model $authority = null): bool
    {
        if ($authority === null) {
            return false;
        }

        return $this->compare($entity->{$this->a}, $authority->{$this->b});
    }

    /**
     * Create a new instance from the raw data.
     *
     * @param  array  $data
     * @return static
     */
    public static function fromData(array $data): ColumnConstraint
    {
        $constraint = new static(
            $data['a'],
            $data['operator'],
            $data['b']
        );

        return $constraint->logicalOperator($data['logicalOperator']);
    }

    /**
     * Get the JSON-able data of this object.
     *
     * @return array
     */
    public function data(): array
    {
        return [
            'class' => static::class,
            'params' => [
                'a' => $this->a,
                'operator' => $this->operator,
                'b' => $this->b,
                'logicalOperator' => $this->logicalOperator,
            ],
        ];
    }
}
