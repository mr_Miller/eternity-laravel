<?php

namespace Eternity\Laravel\Rbac\Constraints;

use Eternity\Laravel\Rbac\Helpers;
use Illuminate\Database\Eloquent\Model;
use InvalidArgumentException;

/**
 * Class Constraint
 * @package Eternity\Laravel\Rbac\Constraints
 */
abstract class Constraint implements Constrainer
{
    /**
     * The operator to use for the comparison.
     *
     * @var string
     */
    protected $operator;

    /**
     * The logical operator to use when checked after a previous constaint.
     *
     * @var string
     */
    protected $logicalOperator = 'and';

    /**
     * Determine whether the given entity/authority passes this constraint.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $entity
     * @param  \Illuminate\Database\Eloquent\Model|null  $authority
     * @return bool
     */
    abstract public function check(Model $entity, Model $authority = null): bool;

    /**
     * Create a new constraint for where a column matches the given value.
     *
     * @param  string  $column
     * @param  mixed  $operator
     * @param  mixed  $value
     * @return \Eternity\Laravel\Rbac\Constraints\ValueConstraint
     */
    public static function where($column, $operator, $value = null): ValueConstraint
    {
        [$operator, $value] = static::prepareOperatorAndValue(
            $operator, $value, func_num_args() === 2
        );

        return new ValueConstraint($column, $operator, $value);
    }


    /**
     * Create a new constraint for where a column matches the given value,
     * with the logical operator set to "or".
     *
     * @param  string  $column
     * @param  mixed  $operator
     * @param  mixed  $value
     * @return \Eternity\Laravel\Rbac\Constraints\ValueConstraint
     */
    public static function orWhere($column, $operator, $value = null): ValueConstraint
    {
        return static::where(...func_get_args())->logicalOperator('or');
    }

    /**
     * Create a new constraint for where a column matches the given column on the authority.
     *
     * @param  string  $a
     * @param  mixed  $operator
     * @param  mixed  $b
     * @return \Eternity\Laravel\Rbac\Constraints\ColumnConstraint
     */
    public static function whereColumn($a, $operator, $b = null): ColumnConstraint
    {
        [$operator, $b] = static::prepareOperatorAndValue(
            $operator, $b, func_num_args() === 2
        );

        return new ColumnConstraint($a, $operator, $b);
    }

    /**
     * Create a new constraint for where a column matches the given column on the authority,
     * with the logical operator set to "or".
     *
     * @param  string  $a
     * @param  mixed  $operator
     * @param  mixed  $b
     * @return \Eternity\Laravel\Rbac\Constraints\ColumnConstraint
     */
    public static function orWhereColumn($a, $operator, $b = null): ColumnConstraint
    {
        return static::whereColumn(...func_get_args())->logicalOperator('or');
    }

    /**
     * Set the logical operator to use when checked after a previous constraint.
     *
     * @param  string|null  $operator
     * @return $this|string
     */
    public function logicalOperator($operator = null)
    {
        if ($operator === null) {
            return $this->logicalOperator;
        }

        Helpers::ensureLogicalOperator($operator);

        $this->logicalOperator = $operator;

        return $this;
    }

    /**
     * Checks whether the logical operator is an "and" operator.
     *
     */
    public function isAnd(): bool
    {
        return $this->logicalOperator === 'and';
    }

    /**
     * Checks whether the logical operator is an "and" operator.
     *
     */
    public function isOr(): bool
    {
        return $this->logicalOperator === 'or';
    }

    /**
     * Determine whether the given constrainer is equal to this object.
     *
     * @param  \Eternity\Laravel\Rbac\Constraints\Constrainer  $constrainer
     * @return bool
     */
    public function equals(Constrainer $constrainer): bool
    {
        if (! $constrainer instanceof static) {
            return false;
        }

        return $this->data()['params'] === $constrainer->data()['params'];
    }

    /**
     * Prepare the value and operator.
     *
     * @param  string  $operator
     * @param  string  $value
     * @param  bool  $usesDefault
     * @return array
     *
     * @throws \InvalidArgumentException
     */
    protected static function prepareOperatorAndValue($operator, $value, $usesDefault): array
    {
        if ($usesDefault) {
            return ['=', $operator];
        }

        if (! in_array($operator, ['=', '==', '!=', '<', '>', '<=', '>='])) {
            throw new InvalidArgumentException("{$operator} is not a valid operator");
        }

        return [$operator, $value];
    }

    /**
     * Compare the two values by the constraint's operator.
     *
     * @param  mixed  $a
     * @param  mixed  $b
     * @return bool
     */
    protected function compare($a, $b): ?bool
    {
        switch ($this->operator) {
            case '=':
            case '==': return $a === $b;
            case '!=': return $a !== $b;
            case '<':  return $a < $b;
            case '>':  return $a > $b;
            case '<=': return $a <= $b;
            case '>=': return $a >= $b;
        }
    }
}
