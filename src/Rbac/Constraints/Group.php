<?php

namespace Eternity\Laravel\Rbac\Constraints;

use Eternity\Laravel\Rbac\Helpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class Group
 * @package Eternity\Laravel\Rbac\Constraints
 */
class Group implements Constrainer
{
    /**
     * The list of constraints.
     *
     * @var \Illuminate\Support\Collection<\Eternity\Laravel\Rbac\Constraints\Constrainer>
     */
    protected $constraints;

    /**
     * The logical operator to use when checked after a previous constrainer.
     *
     * @var string
     */
    protected $logicalOperator = 'and';

    /**
     * Constructor.
     *
     * @param \Eternity\Laravel\Rbac\Constraints\Constrainer[]  $constraints
     */
    public function __construct($constraints = [])
    {
        $this->constraints = new Collection($constraints);
    }

    /**
     * Create a new "and" group.
     *
     * @return static
     */
    public static function withAnd(): Group
    {
        return new static;
    }

    /**
     * Create a new "and" group.
     *
     * @return static
     */
    public static function withOr(): Group
    {
        return (new static)->logicalOperator('or');
    }

    /**
     * Create a new instance from the raw data.
     *
     * @param  array  $data
     * @return static
     */
    public static function fromData(array $data): Group
    {
        $group = new static(array_map(function ($data) {
            return $data['class']::fromData($data['params']);
        }, $data['constraints']));

        return $group->logicalOperator($data['logicalOperator']);
    }

    /**
     * Add the given constraint to the list of constraints.
     *
     * @param \Eternity\Laravel\Rbac\Constraints\Constrainer  $constraint
     */
    public function add(Constrainer $constraint): Group
    {
        $this->constraints->push($constraint);

        return $this;
    }

    /**
     * Determine whether the given entity/authority passes this constraint.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $entity
     * @param  \Illuminate\Database\Eloquent\Model|null  $authority
     * @return bool
     */
    public function check(Model $entity, Model $authority = null): bool
    {
        if ($this->constraints->isEmpty()) {
            return true;
        }

        return $this->constraints->reduce(function ($result, $constraint) use ($entity, $authority) {
            $passes = $constraint->check($entity, $authority);

            if ($result === null) {
                return $passes;
            }

            return $constraint->isOr() ? ($result || $passes) : ($result && $passes);
        });
    }

    /**
     * Set the logical operator to use when checked after a previous constrainer.
     *
     * @param  string|null  $operator
     * @return $this|string
     */
    public function logicalOperator($operator = null)
    {
        if ($operator === null) {
            return $this->logicalOperator;
        }

        Helpers::ensureLogicalOperator($operator);

        $this->logicalOperator = $operator;

        return $this;
    }

    /**
     * Checks whether the logical operator is an "and" operator.
     *
     */
    public function isAnd(): bool
    {
        return $this->logicalOperator === 'and';
    }

    /**
     * Checks whether the logical operator is an "and" operator.
     *
     */
    public function isOr(): bool
    {
        return $this->logicalOperator === 'or';
    }

    /**
     * Determine whether the given constrainer is equal to this object.
     *
     * @param  \Eternity\Laravel\Rbac\Constraints\Constrainer  $constrainer
     * @return bool
     */
    public function equals(Constrainer $constrainer): bool
    {
        if (! $constrainer instanceof static) {
            return false;
        }

        if ($this->constraints->count() !== $constrainer->constraints->count()) {
            return false;
        }

        foreach ($this->constraints as $index => $constraint) {
            if (! $constrainer->constraints[$index]->equals($constraint)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the JSON-able data of this object.
     *
     * @return array
     */
    public function data(): array
    {
        return [
            'class' => static::class,
            'params' => [
                'logicalOperator' => $this->logicalOperator,
                'constraints' => $this->constraints->map(function ($constraint) {
                    return $constraint->data();
                })->all(),
            ],
        ];
    }
}
