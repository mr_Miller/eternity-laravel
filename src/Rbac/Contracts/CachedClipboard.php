<?php

namespace Eternity\Laravel\Rbac\Contracts;

use Illuminate\Contracts\Cache\Store;
use Illuminate\Database\Eloquent\Model;

/**
 * Interface CachedClipboard
 * @package Eternity\Laravel\Rbac\Contracts
 */
interface CachedClipboard extends Clipboard
{
    /**
     * Set the cache instance.
     *
     * @param  \Illuminate\Contracts\Cache\Store  $cache
     * @return $this
     */
    public function setCache(Store $cache);

    /**
     * Get the cache instance.
     *
     * @return \Illuminate\Contracts\Cache\Store
     */
    public function getCache(): Store;

    /**
     * Clear the cache.
     *
     * @param  null|\Illuminate\Database\Eloquent\Model  $authority
     * @return $this
     */
    public function refresh($authority = null);

    /**
     * Clear the cache for the given authority.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $authority
     * @return $this
     */
    public function refreshFor(Model $authority);
}
