<?php

namespace Eternity\Laravel\Rbac\Broadcast;

use Eternity\Contracts\Arrayable;

/**
 * Class Ability
 * @package Eternity\Laravel\Rbac\Broadcast
 */
class Ability implements Arrayable
{
    protected $name;

    protected $description;

    /**
     * Ability constructor.
     * @param string $name
     * @param string $description
     */
    public function __construct(string $name, string $description)
    {
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            $this->name => $this->description
        ];
    }
}