<?php

namespace Eternity\Laravel\Rbac\Broadcast;

use Eternity\Contracts\Arrayable;
use Eternity\Laravel\Security\Crypt;

/**
 * Class Rbac
 * @package Eternity\Laravel\Rbac\Broadcast
 */
class Rbac implements Arrayable
{
    /**
     * @var \Eternity\Laravel\Rbac\Broadcast\Ability[]
     */
    protected $abilities = [];

    /**
     * @var array
     */
    protected $roles = [];

    /**
     * Rbac constructor.
     * @param array $roles
     * @param array $abilities
     */
    protected function __construct(array $roles = [], array $abilities = [])
    {
        $this->roles = $roles;
        $this->abilities = $abilities;
    }

    /**
     * Creates without roles and abilities
     * @return static
     */
    public static function create(): self
    {
        return new static();
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'roles'     => $this->roles,
            'abilities' => $this->abilities(),
        ];
    }

    /**
     * @return array
     */
    public function abilities(): array
    {
        $abilities = [];
        foreach ($this->abilities as $ability) {
            $abilities[$ability->getName()] = $ability->getDescription();
        }

        return $abilities;
    }

    /**
     * @param string|array $abilities
     * @return bool
     */
    public function isAuthorizedFor($abilities): bool
    {
        if (is_string($abilities)) {
            return $this->can($abilities);
        }

        if (is_array($abilities)) {
            return $this->intersect($abilities);
        }

        throw new \InvalidArgumentException('Abilities format is invalid. Only string or array is allowed');
    }

    /**
     * @param string $ability
     * @return bool
     */
    public function can(string $ability): bool
    {
        return array_key_exists($ability, $this->abilities());
    }

    /**
     * @param array $abilities
     * @return bool
     */
    public function intersect(array $abilities): bool
    {
        return (bool)count(array_intersect($abilities, array_keys($this->abilities())));
    }

    /**
     * Check if user has roles or one of roles.
     * @param string|array $roles
     * @return bool
     */
    public function hasRole($roles): bool
    {
        if (is_string($roles)) {
            return in_array($roles, $this->roles);
        }

        if (is_array($roles)) {
            return (bool)count(array_intersect($roles, $this->roles));
        }

        throw new \InvalidArgumentException('Roles format is invalid. Only string or array is allowed');
    }

    /**
     * @return false|string
     */
    public function serialize()
    {
        return json_encode($this->toArray());
    }

    /**
     * @return string
     */
    public function encrypt(): string
    {
        return (new Crypt($this->toArray()))->serialize();
    }

    /**
     * @param array $abilities
     * @return array
     */
    public static function constructAbilities(array $abilities): array
    {
        $result = [];
        foreach ($abilities as $abilityName => $abilityDescription) {
            $result[] = new Ability($abilityName, $abilityDescription);
        }

        return $result;
    }

    /**
     * @param string $serializedString
     * @return static
     */
    public static function deserialize(string $serializedString): self
    {
        $data = json_decode($serializedString, true);

        return new static($data['roles'], self::constructAbilities($data['abilities']));
    }

    /**
     * @param string $encryptedString
     * @return \Eternity\Laravel\Rbac\Broadcast\Rbac
     * @throws \JsonException
     */
    public static function decrypt(string $encryptedString): self
    {
        $data = Crypt::fromInput($encryptedString)->storage()->toArray();

        return new static($data['roles'], self::constructAbilities($data['abilities']));
    }
}