<?php

namespace Eternity\Laravel\Rbac\Database\Scope;

use Eternity\Laravel\Rbac\Database\Models;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope as EloquentScope;

/**
 * Class TenantScope
 * @package Eternity\Laravel\Rbac\Database\Scope
 */
class TenantScope implements EloquentScope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $query, Model $model): void
    {
        Models::scope()->applyToModelQuery($query, $model->getTable());
    }
}
