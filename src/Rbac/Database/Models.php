<?php

namespace Eternity\Laravel\Rbac\Database;

use Closure;
use Eternity\Laravel\Rbac\Contracts\Scope as ScopeContract;
use Eternity\Laravel\Rbac\Database\Scope\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\Builder;

/**
 * Class Models
 * @package Eternity\Laravel\Rbac\Database
 */
class Models
{
    public const ABL = 'AbilityModel';
    public const RLS = 'RolesModel';
    public const USR = 'UserModel';

    /**
     * Map of rbac's models.
     *
     * @var array
     */
    protected static $models = [
        self::ABL => Ability::class,
        self::RLS => Role::class,
    ];

    /**
     * Map of ownership for models.
     *
     * @var array
     */
    protected static $ownership = [];

    /**
     * Map of rbac's tables.
     *
     * @var array
     */
    protected static $tables = [];

    /**
     * The model scoping instance.
     *
     * @var \Eternity\Laravel\Rbac\Database\Scope\Scope
     */
    protected static $scope;

    /**
     * Set the model to be used for abilities.
     *
     * @param  string  $model
     * @return void
     */
    public static function setAbilitiesModel($model): void
    {
        static::$models[Models::ABL] = $model;

        static::updateMorphMap([$model]);
    }

    /**
     * Set the model to be used for roles.
     *
     * @param  string  $model
     * @return void
     */
    public static function setRolesModel($model): void
    {
        static::$models[Models::RLS] = $model;

        static::updateMorphMap([$model]);
    }

    /**
     * Set the model to be used for users.
     *
     * @param  string  $model
     * @return void
     */
    public static function setUsersModel($model): void
    {
        static::$models[Models::USR] = $model;

        static::$tables['users'] = static::user()->getTable();
    }

    /**
     * Set custom table names.
     *
     * @param  array  $map
     * @return void
     */
    public static function setTables(array $map): void
    {
        static::$tables = array_merge(static::$tables, $map);

        static::updateMorphMap();
    }

    /**
     * Get a custom table name mapping for the given table.
     *
     * @param  string  $table
     * @return string
     */
    public static function table($table): string
    {
        return static::$tables[$table] ?? $table;
    }

    /**
     * Get or set the model scoping instance.
     *
     * @param  \Eternity\Laravel\Rbac\Contracts\Scope|null  $scope
     * @return mixed
     */
    public static function scope(ScopeContract $scope = null)
    {
        if ($scope !== null) {
            return static::$scope = $scope;
        }

        if (static::$scope === null) {
            static::$scope = new Scope;
        }

        return static::$scope;
    }

    /**
     * Get the classname mapping for the given model.
     *
     * @param  string  $model
     * @return string
     */
    public static function classname($model): string
    {
        return static::$models[$model] ?? $model;
    }

    /**
     * Update Eloquent's morph map with the rbac models and tables.
     *
     * @param  array|null  $classNames
     * @return void
     */
    public static function updateMorphMap($classNames = null): void
    {
        if ($classNames === null) {
            $classNames = [
                static::classname(Models::ABL),
                static::classname(Models::RLS),
            ];
        }

        Relation::morphMap($classNames);
    }

    /**
     * Register an attribute/callback to determine if a model is owned by a given authority.
     *
     * @param  string|\Closure  $model
     * @param  string|\Closure|null  $attribute
     * @return void
     */
    public static function ownedVia($model, $attribute = null): void
    {
        if ($attribute === null) {
            static::$ownership['*'] = $model;
        }

        static::$ownership[$model] = $attribute;
    }

    /**
     * Determines whether the given model is owned by the given authority.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $authority
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return bool
     */
    public static function isOwnedBy(Model $authority, Model $model): bool
    {
        $type = get_class($model);

        if (isset(static::$ownership[$type])) {
            $attribute = static::$ownership[$type];
        } elseif (isset(static::$ownership['*'])) {
            $attribute = static::$ownership['*'];
        } else {
            $attribute = strtolower(static::basename($authority)).'_id';
        }

        return static::isOwnedVia($attribute, $authority, $model);
    }

    /**
     * Determines ownership via the given attribute.
     *
     * @param  string|\Closure  $attribute
     * @param  \Illuminate\Database\Eloquent\Model  $authority
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return bool
     */
    protected static function isOwnedVia($attribute, Model $authority, Model $model): bool
    {
        if ($attribute instanceof Closure) {
            return $attribute($model, $authority);
        }

        return $authority->getKey() === $model->{$attribute};
    }

    /**
     * Get an instance of the ability model.
     *
     * @param  array  $attributes
     * @return \Eternity\Laravel\Rbac\Database\Ability
     */
    public static function ability(array $attributes = []): Ability
    {
        return static::make(Models::ABL, $attributes);
    }

    /**
     * Get an instance of the role model.
     *
     * @param  array  $attributes
     * @return \Eternity\Laravel\Rbac\Database\Role
     */
    public static function role(array $attributes = []): Role
    {
        return static::make(Models::RLS, $attributes);
    }

    /**
     * Get an instance of the user model.
     *
     * @param  array  $attributes
     * @return \Illuminate\Database\Eloquent\Model
     */
    public static function user(array $attributes = []): Model
    {
        return static::make(Models::USR, $attributes);
    }

    /**
     * Get a new query builder instance.
     *
     * @param  string  $table
     * @return \Illuminate\Database\Query\Builder
     */
    public static function query($table): Builder
    {
        $query = new Builder(
            $connection = static::user()->getConnection(),
            $connection->getQueryGrammar(),
            $connection->getPostProcessor()
        );

        return $query->from(static::table($table));
    }

    /**
     * Reset all settings to their original state.
     *
     * @return void
     */
    public static function reset(): void
    {
        static::$models = static::$tables = static::$ownership = [];
    }

    /**
     * Get an instance of the given model.
     *
     * @param  string  $model
     * @param  array  $attributes
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected static function make($model, array $attributes = []): Model
    {
        $model = static::classname($model);

        return new $model($attributes);
    }

    /**
     * Get the basename of the given class.
     *
     * @param  string|object  $class
     * @return string
     */
    protected static function basename($class): string
    {
        if ( ! is_string($class)) {
            $class = get_class($class);
        }

        $segments = explode('\\', $class);

        return end($segments);
    }
}
