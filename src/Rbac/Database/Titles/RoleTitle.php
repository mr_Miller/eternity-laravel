<?php

namespace Eternity\Laravel\Rbac\Database\Titles;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RoleTitle
 * @package Eternity\Laravel\Rbac\Database\Titles
 */
class RoleTitle extends Title
{
    public function __construct(Model $role)
    {
        $this->title = $this->humanize($role->name);
    }
}
