<?php

namespace Eternity\Laravel\Rbac\Database;

use Eternity\Laravel\Rbac\Database\Scope\TenantScope;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Role
 * @package Eternity\Laravel\Rbac\Database
 */
class Role extends Model
{
    use Concerns\IsRole;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'title', 'level'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'int',
        'level' => 'int',
    ];

    /**
     * Constructor.
     *
     * @param array  $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->table = Models::table('roles');

        parent::__construct($attributes);
    }

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted(): void
    {
        static::addGlobalScope(new TenantScope);
    }
}
