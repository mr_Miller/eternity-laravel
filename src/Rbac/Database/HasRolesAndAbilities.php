<?php

namespace Eternity\Laravel\Rbac\Database;

use Eternity\Laravel\Rbac\Database\Concerns\HasAbilities;
use Eternity\Laravel\Rbac\Database\Concerns\HasRoles;

/**
 * Trait HasRolesAndAbilities
 * @package Eternity\Laravel\Rbac\Database
 */
trait HasRolesAndAbilities
{
    use HasRoles, HasAbilities;
}
