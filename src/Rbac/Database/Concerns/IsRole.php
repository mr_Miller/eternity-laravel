<?php

namespace Eternity\Laravel\Rbac\Database\Concerns;

use Eternity\Laravel\Rbac\Database\Models;
use Eternity\Laravel\Rbac\Database\Queries\Roles as RolesQuery;
use Eternity\Laravel\Rbac\Database\Titles\RoleTitle;
use Eternity\Laravel\Rbac\Helpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

/**
 * Trait IsRole
 * @package Eternity\Laravel\Rbac\Database\Concerns
 */
trait IsRole
{
    use HasAbilities, Authorizable;

    /**
     * Boot the is role trait.
     *
     * @return void
     */
    public static function bootIsRole(): void
    {
        static::creating(function ($role) {
            Models::scope()->applyToModel($role);

            if ($role->title === null) {
                $role->title = RoleTitle::from($role)->toString();
            }
        });

        static::deleted(function ($role) {
            $role->abilities()->detach();
        });
    }

    /**
     * The users relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users(): BelongsToMany
    {
        $relation = $this->morphedByMany(
            Models::classname(Models::USR),
            'entity',
            Models::table('assigned_roles')
        )->withPivot('scope');

        return Models::scope()->applyToRelation($relation);
    }

    /**
     * Assign the role to the given model(s).
     *
     * @param  string|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection  $model
     * @param  array|null  $keys
     * @return $this
     */
    public function assignTo($model, array $keys = null): self
    {
        [$model, $keys] = Helpers::extractModelAndKeys($model, $keys);

        $query = $this->newBaseQueryBuilder()->from(Models::table('assigned_roles'));

        $query->insert($this->createAssignRecords($model, $keys));

        return $this;
    }

    /**
     * Find the given roles, creating the names that don't exist yet.
     *
     * @param  iterable  $roles
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findOrCreateRoles($roles): \Illuminate\Database\Eloquent\Collection
    {
        $roles = Helpers::groupModelsAndIdentifiersByType($roles);

        $roles['integers'] = $this->find($roles['integers']);

        $roles['strings'] = $this->findOrCreateRolesByName($roles['strings']);

        return $this->newCollection(Arr::collapse($roles));
    }

    /**
     * Find roles by name, creating the ones that don't exist.
     *
     * @param  iterable  $names
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function findOrCreateRolesByName($names)
    {
        if (empty($names)) {
            return [];
        }

        $existing = static::whereIn('name', $names)->get()->keyBy('name');

        return (new Collection($names))
                ->diff($existing->pluck('name'))
                ->map(function ($name) {
                    return static::create(compact('name'));
                })
                ->merge($existing);
    }

    /**
     * Get the IDs of the given roles.
     *
     * @param  iterable  $roles
     * @return array
     */
    public function getRoleKeys($roles): array
    {
        $roles = Helpers::groupModelsAndIdentifiersByType($roles);

        $roles['strings'] = $this->getKeysByName($roles['strings']);

        $roles['models'] = Arr::pluck($roles['models'], $this->getKeyName());

        return Arr::collapse($roles);
    }

    /**
     * Get the names of the given roles.
     *
     * @param  iterable  $roles
     * @return array
     */
    public function getRoleNames($roles): array
    {
        $roles = Helpers::groupModelsAndIdentifiersByType($roles);

        $roles['integers'] = $this->getNamesByKey($roles['integers']);

        $roles['models'] = Arr::pluck($roles['models'], 'name');

        return Arr::collapse($roles);
    }

    /**
     * Get the keys of the roles with the given names.
     *
     * @param  array $names
     * @return array
     */
    public function getKeysByName($names): array
    {
        if (empty($names)) {
            return [];
        }

        return $this->whereIn('name', $names)
                    ->select($this->getKeyName())->get()
                    ->pluck($this->getKeyName())->all();
    }

    /**
     * Get the names of the roles with the given IDs.
     *
     * @param  array $keys
     * @return array
     */
    public function getNamesByKey($keys): array
    {
        if (empty($keys)) {
            return [];
        }

        return $this->whereIn($this->getKeyName(), $keys)
                    ->select('name')->get()
                    ->pluck('name')->all();
    }

    /**
     * Retract the role from the given model(s).
     *
     * @param  string|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection  $model
     * @param  array|null  $keys
     * @return $this
     */
    public function retractFrom($model, array $keys = null): self
    {
        [$model, $keys] = Helpers::extractModelAndKeys($model, $keys);

        $query = $this->newBaseQueryBuilder()
            ->from(Models::table('assigned_roles'))
            ->where('role_id', $this->getKey())
            ->where('entity_type', $model->getMorphClass())
            ->whereIn('entity_id', $keys);

        Models::scope()->applyToRelationQuery($query, $query->from);

        $query->delete();

        return $this;
    }

    /**
     * Create the pivot table records for assigning the role to given models.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  array  $keys
     * @return array
     */
    protected function createAssignRecords(Model $model, array $keys): array
    {
        $type = $model->getMorphClass();

        return array_map(function ($key) use ($type) {
            return Models::scope()->getAttachAttributes() + [
                'role_id'     => $this->getKey(),
                'entity_type' => $type,
                'entity_id'   => $key,
            ];
        }, $keys);
    }

    /**
     * Constrain the given query to roles that were assigned to the given authorities.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection  $model
     * @param  array  $keys
     * @return void
     */
    public function scopeWhereAssignedTo($query, $model, array $keys = null): void
    {
        (new RolesQuery)->constrainWhereAssignedTo($query, $model, $keys);
    }
}
