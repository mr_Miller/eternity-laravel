<?php

namespace Eternity\Laravel\Rbac\Database\Concerns;

use Eternity\Laravel\Rbac\Constraints\Constrainer;
use Eternity\Laravel\Rbac\Constraints\Group;
use Eternity\Laravel\Rbac\Database\Ability;
use Eternity\Laravel\Rbac\Database\Models;
use Eternity\Laravel\Rbac\Database\Queries\AbilitiesForModel;
use Eternity\Laravel\Rbac\Database\Titles\AbilityTitle;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Trait IsAbility
 * @package Eternity\Laravel\Rbac\Database\Concerns
 * @property array $options
 * @property array $attributes
 */
trait IsAbility
{
    /**
     * Boot the is ability trait.
     *
     * @return void
     */
    public static function bootIsAbility(): void
    {
        static::creating(function ($ability) {
            Models::scope()->applyToModel($ability);

            if ($ability->title === null) {
                $ability->title = AbilityTitle::from($ability)->toString();
            }
        });
    }

    /**
     * Get the options attribute.
     *
     * @return array
     */
    public function getOptionsAttribute(): array
    {
        if (empty($this->attributes['options'])) {
            return [];
        }

        return json_decode($this->attributes['options'], true, 32, JSON_THROW_ON_ERROR);
    }

    /**
     * Set the "options" attribute.
     *
     * @param  array  $options
     * @return void
     */
    public function setOptionsAttribute(array $options): void
    {
        $this->attributes['options'] = json_encode($options, JSON_THROW_ON_ERROR, 32);
    }

    /**
     * CHecks if the ability has constraints.
     *
     * @return bool
     */
    public function hasConstraints(): bool
    {
        return ! empty($this->options['constraints']);
    }

    /**
     * Get the ability's constraints.
     *
     * @return \Eternity\Laravel\Rbac\Constraints\Constrainer
     */
    public function getConstraints()
    {
        if (empty($this->options['constraints'])) {
            return new Group();
        }

        $data = $this->options['constraints'];

        return $data['class']::fromData($data['params']);
    }

    /**
     * Set the ability's constraints.
     *
     * @param  \Eternity\Laravel\Rbac\Constraints\Constrainer  $constrainer
     * @return $this
     */
    public function setConstraints(Constrainer $constrainer): self
    {
        $this->options = array_merge($this->options, [
            'constraints' => $constrainer->data(),
        ]);

        return $this;
    }

    /**
     * Create a new ability for a specific model.
     *
     * @param  \Illuminate\Database\Eloquent\Model|string  $model
     * @param  string|array  $attributes
     * @return \Eternity\Laravel\Rbac\Database\Ability
     */
    public static function createForModel($model, $attributes): Ability
    {
        $model = static::makeForModel($model, $attributes);

        $model->save();

        return $model;
    }

    /**
     * Make a new ability for a specific model.
     *
     * @param  \Illuminate\Database\Eloquent\Model|string  $model
     * @param  string|array  $attributes
     * @return \Eternity\Laravel\Rbac\Database\Ability
     */
    public static function makeForModel($model, $attributes): Ability
    {
        if (is_string($attributes)) {
            $attributes = ['name' => $attributes];
        }

        if ($model === '*') {
            return (new static)->forceFill($attributes + [
                'entity_type' => '*',
            ]);
        }

        if (is_string($model)) {
            $model = new $model;
        }

        return (new static)->forceFill($attributes + [
            'entity_type' => $model->getMorphClass(),
            'entity_id'   => $model->exists ? $model->getKey() : null,
        ]);
    }

    /**
     * The roles relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles(): BelongsToMany
    {
        $relation = $this->morphedByMany(
            Models::classname(Models::RLS),
            'entity',
            Models::table('permissions')
        )->withPivot('forbidden', 'scope');

        return Models::scope()->applyToRelation($relation);
    }

    /**
     * The users relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users(): BelongsToMany
    {
        $relation = $this->morphedByMany(
            Models::classname(Models::USR),
            'entity',
            Models::table('permissions')
        )->withPivot('forbidden', 'scope');

        return Models::scope()->applyToRelation($relation);
    }

    /**
     * Get the identifier for this ability.
     *
     * @return string
     */
    final public function getIdentifierAttribute(): string
    {
        $slug = $this->attributes['name'];

        if ($this->attributes['entity_type']) {
            $slug .= '-'.$this->attributes['entity_type'];
        }

        if ($this->attributes['entity_id']) {
            $slug .= '-'.$this->attributes['entity_id'];
        }

        if ($this->attributes['only_owned']) {
            $slug .= '-owned';
        }

        return strtolower($slug);
    }

    /**
     * Constrain a query to having the given name.
     *
     * @param  \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder  $query
     * @return void
     */
    public function scopeByName($query, $name, $strict = false): void
    {
        $names = (array) $name;

        if (! $strict && $name !== '*') {
            $names[] = '*';
        }

        $query->whereIn("{$this->table}.name", $names);
    }

    /**
     * Constrain a query to simple abilities.
     *
     * @param  \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder  $query
     * @return void
     */
    public function scopeSimpleAbility($query): void
    {
        $query->whereNull("{$this->table}.entity_type");
    }

    /**
     * Constrain a query to an ability for a specific model.
     *
     * @param  \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Model|string  $model
     * @param  bool  $strict
     * @return void
     */
    public function scopeForModel($query, $model, $strict = false): void
    {
        (new AbilitiesForModel)->constrain($query, $model, $strict);
    }
}
