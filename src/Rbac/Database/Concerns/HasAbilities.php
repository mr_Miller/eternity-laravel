<?php

namespace Eternity\Laravel\Rbac\Database\Concerns;

use Eternity\Laravel\Rbac\Conductors\ForbidsAbilities;
use Eternity\Laravel\Rbac\Conductors\GivesAbilities;
use Eternity\Laravel\Rbac\Conductors\RemovesAbilities;
use Eternity\Laravel\Rbac\Conductors\UnforbidsAbilities;
use Eternity\Laravel\Rbac\Contracts\Clipboard;
use Eternity\Laravel\Rbac\Database\Models;
use Eternity\Laravel\Rbac\Helpers;
use Illuminate\Container\Container;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Trait HasAbilities
 * @package Eternity\Laravel\Rbac\Database\Concerns
 */
trait HasAbilities
{
    /**
     * Boot the HasAbilities trait.
     *
     * @return void
     */
    public static function bootHasAbilities(): void
    {
        static::deleted(function ($model) {
            if (! Helpers::isSoftDeleting($model)) {
                $model->abilities()->detach();
            }
        });
    }

    /**
     * The abilities relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function abilities(): BelongsToMany
    {
        $relation = $this->morphToMany(
            Models::classname(Models::ABL),
            'entity',
            Models::table('permissions')
        )->withPivot('forbidden', 'scope');

        return Models::scope()->applyToRelation($relation);
    }

    /**
     * Get all of the model's allowed abilities.
     * @return \Illuminate\Database\Eloquent\Collection
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function getAbilities(): Collection
    {
        return Container::getInstance()
            ->make(Clipboard::class)
            ->getAbilities($this);
    }

    /**
     * Get all of the model's allowed abilities.
     * @return \Illuminate\Database\Eloquent\Collection
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function getForbiddenAbilities(): Collection
    {
        return Container::getInstance()
            ->make(Clipboard::class)
            ->getAbilities($this, false);
    }

    /**
     * Give an ability to the model.
     *
     * @param  mixed  $ability
     * @param  mixed|null  $model
     * @return \Eternity\Laravel\Rbac\Conductors\GivesAbilities|$this
     */
    public function allow($ability = null, $model = null)
    {
        if ($ability === null) {
            return new GivesAbilities($this);
        }

        (new GivesAbilities($this))->to($ability, $model);

        return $this;
    }

    /**
     * Remove an ability from the model.
     *
     * @param  mixed  $ability
     * @param  mixed|null  $model
     * @return \Eternity\Laravel\Rbac\Conductors\RemovesAbilities|$this
     */
    public function disallow($ability = null, $model = null)
    {
        if ($ability === null) {
            return new RemovesAbilities($this);
        }

        (new RemovesAbilities($this))->to($ability, $model);

        return $this;
    }

    /**
     * Forbid an ability to the model.
     *
     * @param  mixed  $ability
     * @param  mixed|null  $model
     * @return \Eternity\Laravel\Rbac\Conductors\ForbidsAbilities|$this
     */
    public function forbid($ability = null, $model = null)
    {
        if ($ability === null) {
            return new ForbidsAbilities($this);
        }

        (new ForbidsAbilities($this))->to($ability, $model);

        return $this;
    }

    /**
     * Remove ability forbiddal from the model.
     *
     * @param  mixed  $ability
     * @param  mixed|null  $model
     * @return \Eternity\Laravel\Rbac\Conductors\UnforbidsAbilities|$this
     */
    public function unforbid($ability = null, $model = null)
    {
        if ($ability === null) {
            return new UnforbidsAbilities($this);
        }

        (new UnforbidsAbilities($this))->to($ability, $model);

        return $this;
    }
}
