<?php

namespace Eternity\Laravel\Rbac\Database\Concerns;

use Eternity\Laravel\Rbac\Contracts\Clipboard;
use Illuminate\Container\Container;

/**
 * Trait Authorizable
 * @package Eternity\Laravel\Rbac\Database\Concerns
 */
trait Authorizable
{
    /**
     * Determine if the authority has a given ability.
     *
     * @param  string  $ability
     * @param  \Illuminate\Database\Eloquent\Model|null  $model
     *
     * @return bool
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function can($ability, $model = null): bool
    {
        return Container::getInstance()
            ->make(Clipboard::class)
            ->check($this, $ability, $model);
    }

    /**
     * Determine if the authority does not have a given ability.
     *
     * @param  string  $ability
     * @param  \Illuminate\Database\Eloquent\Model|null  $model
     *
     * @return bool
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function cannot($ability, $model = null): bool
    {
        return ! $this->can($ability, $model);
    }
}
