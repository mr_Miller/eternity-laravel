<?php
return [
    'options' => [
        'headers' => [
            'Accept'       => 'application/json',
            'Content-Type' => 'application/json',
        ],
        'verify'  => false
    ]
];