<?php
return [
   /*
   |--------------------------------------------------------------------------
   | Microservice Info
   |--------------------------------------------------------------------------
   |
   | Information about current microservice
   |
   */
   'name' => null, // used in Event-Bus event transmitting
    'credentials' => [
        'username' => env('INTERNAL_AUTH_USERNAME'),
        'password' => env('INTERNAL_AUTH_PASSWORD'),
    ],
];
