<?php

namespace Eternity\Laravel\Request;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Abstract form request
 *
 * Allows to validate route parameters
 *
 * Class AbstractRequest
 * @package Eternity\Laravel\Request
 */
abstract class AbstractRequest extends FormRequest
{
    /**
     * Get all of the input and files for the request.
     *
     * Modified all() method that allows to validate ROUTE parameters in request
     *
     * @param array|mixed|null $keys
     * @return array
     */
    public function all($keys = null)
    {
        return array_merge(parent::all(), $this->route()->parameters());
    }
}