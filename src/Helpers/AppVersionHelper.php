<?php

namespace Eternity\Laravel\Helpers;

use Eternity\Definitions\HeadersDefinition;

/**
 * Class AppVersionHelper
 * @package Eternity\Laravel\Helpers
 */
class AppVersionHelper
{
    public const ANIMAL_ID = 'animal_id';
    public const IOS = 'ios';
    public const ANDROID = 'android';

    /**
     * Get the parsed application version
     */
    public static function getParsedAppVersion(): ?string
    {
        if (preg_match('^(\d+\.)?(\d+\.)?(\*|\d+)^', self::getAppVersion(), $matches)) {
            return $matches[0];
        }

        return null;
    }

    /**
     * Get a raw application version
     */
    public static function getAppVersion(): ?string
    {
        return request()->header(HeadersDefinition::APP_VERSION);
    }

    /**
     * Get an application OS type
     */
    public static function getAppOsType(): ?string
    {
        $type = request()->header(HeadersDefinition::OS_TYPE);
        if ($type === null) {
            return null;
        }

        return mb_strtolower($type);
    }

    /**
     * Is the application version less than current
     * @param string|null $iosVer
     * @param string|null $androidVer
     * @param string|null $animalIdVer
     * @return bool
     */
    public static function isVersionLessThan(
        string $iosVer = null,
        string $androidVer = null,
        string $animalIdVer = null
    ): bool {
        switch (self::getAppOsType()) {
            case self::IOS:
                return $iosVer && version_compare(self::getParsedAppVersion(), $iosVer) <= 0;
            case self::ANDROID:
                return $androidVer && version_compare(self::getParsedAppVersion(), $androidVer) <= 0;
            case self::ANIMAL_ID:
                return $animalIdVer && version_compare(self::getParsedAppVersion(), $animalIdVer) <= 0;
            default:
                return false;
        }
    }
}