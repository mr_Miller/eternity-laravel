<?php

namespace Eternity\Laravel\Eloquent\Models;

use Eternity\Contracts\PublicEntity;
use Eternity\Helpers\Date;
use Eternity\Traits\PublicName;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AbstractModel
 * @package Eternity\Laravel\Eloquent\Models
 *
 * This is the abstract base class for all models in the application.
 * It extends the core Laravel model class and implements the PublicEntity interface.
 */
abstract class AbstractModel extends Model implements PublicEntity
{
    use PublicName;

    /**
     * @var string The storage format of the model's date columns.
     */
    protected $dateFormat = Date::POSTGRES_TIME_FORMAT;

    /**
     * Returns model table name
     * @return string
     */
    public static function table(): string
    {
        return (new static)->getTable();
    }

    /**
     * Returns entity public Id
     * @return string
     */
    public function publicId(): string
    {
        return $this->getAttribute('id');
    }

    /**
     * Returns entity public type
     * @return string
     */
    public function publicType(): string
    {
        return self::entityName();
    }
}