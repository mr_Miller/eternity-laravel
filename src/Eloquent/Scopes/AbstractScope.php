<?php

namespace Eternity\Laravel\Eloquent\Scopes;

use Eternity\Laravel\Contracts\PaginatorInterface;
use Eternity\Laravel\Pagination\LengthAwarePaginatorAdapter;
use Eternity\Resource\Objects\Order;
use Eternity\Resource\Objects\Page;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class AbstractScope
 * @package Eternity\Laravel\Eloquent\Scopes
 */
abstract class AbstractScope extends Builder
{
    /**
     * @param \Illuminate\Database\Eloquent\Model|null $model
     * @return static
     */
    abstract public static function init(Model $model = null);

    /**
     * Applies order from metadata to sql request
     *
     * @param \Eternity\Resource\Objects\Order $order
     * @return $this
     */
    public function applyOrder(Order $order): self
    {
        return $this->orderBy($order->getBy(), $order->getDirection());
    }

    /**
     * Returns paginator
     *
     * @param \Eternity\Resource\Objects\Page $page
     * @return \Eternity\Laravel\Contracts\PaginatorInterface
     */
    public function getPaginator(Page $page): PaginatorInterface
    {
        $paginator = $this->paginate(
            $page->getSize(),
            '*',
            '',
            $page->getCurrent()
        );

        return new LengthAwarePaginatorAdapter(new Collection($paginator->items()), $paginator);
    }
}