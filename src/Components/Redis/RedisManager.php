<?php

namespace Eternity\Laravel\Components\Redis;

/**
 * Class RedisManager
 * @package App\Application\Components\Redis
 */
class RedisManager extends \Illuminate\Redis\RedisManager
{
    /**
     * Set prefix for current connection
     *
     * Needed for setting tasks for various microservices
     *
     * @param string $prefix
     */
    public function setPrefix(string $prefix): void
    {
        $this->config['options']['prefix'] = $prefix;
    }
}
