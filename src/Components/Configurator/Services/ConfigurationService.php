<?php

namespace Eternity\Laravel\Components\Configurator\Services;

use Eternity\Definitions\HeadersDefinition;

class ConfigurationService
{
    public const CONNECTION_NAME = 'configurator';

    /**
     * Get app uuid by app key from Radis
     * @param string $key
     * @return string
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function getAppUuidByKey(string $key): ?string
    {
        $redis = app()->make('redis');
        $redis->setPrefix('');
        return $redis->connection(self::CONNECTION_NAME)->get($key);
    }

    /**
     * @param string $uuid
     * @param string $version
     * @return string
     */
    public function getCacheKeyForApp(string $uuid, string $version): string
    {
        return "app-config:$uuid:$version";
    }

    /**
     * @param string $cacheKey
     * @return array
     * @throws \JsonException
     */
    public function getConfigFromCache(string $cacheKey): array
    {
        $redis = app()->make('redis');
        $redis->setPrefix('');

        $config = $redis->connection(self::CONNECTION_NAME)->get($cacheKey);

        return $config ? json_decode($config, true, 512, JSON_THROW_ON_ERROR) : [];
    }

    /**
     * Get the app configuration by package
     * @param string $packageName
     * @return array
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \JsonException
     */
    public function getConfigurationByPackage(string $packageName): array
    {
        $uuid = $this->getAppUuidByKey(implode(':', ['package', $packageName]));

        if ($uuid === null) {
            return [];
        }

        $configuration = $this->getConfigFromCache(
            $this->getCacheKeyForApp($uuid, config('app.config_version'))
        );

        if (empty($configuration)) {
            throw new \Exception(
                'Configuration not loaded. Check `' . $packageName .'` package name'
            );
        }

        return $configuration;
    }

    /**
     * Get the app configuration by bundle_id
     * @param string $bundleId
     * @return array
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \JsonException
     */
    public function getConfigurationByBundleId(string $bundleId): array
    {
        $uuid = $this->getAppUuidByKey(implode(':', ['bid', $bundleId]));

        if ($uuid === null) {
            return [];
        }

        $configuration = $this->getConfigFromCache(
            $this->getCacheKeyForApp($uuid, config('app.config_version'))
        );

        if (empty($configuration)) {
            throw new \Exception(
                'Configuration not loaded. Check `' . $bundleId .'` package name'
            );
        }

        return $configuration;
    }

    /**
     * @param string|null $service
     * @param string|null $version
     * @return string
     */
    public function getCacheKeyForMicroService(string $service = null, string $version = null): string
    {
        return "service-config:$service:$version";
    }

    /**
     * @param string $cacheKey
     * @param array $configuration
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \JsonException
     */
    public function saveConfigToCache(string $cacheKey, array $configuration): void
    {
        $redis = app()->make('redis');
        $redis->setPrefix('');
        $redis->connection(self::CONNECTION_NAME)
            ->set($cacheKey, json_encode($configuration, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE));
    }

    /**
     * @param string $filename
     * @return array
     */
    public function getLocalEnv(string $filename): array
    {
        $rows = file($filename);
        $env = [];

        foreach ($rows as $row) {
            $row = trim($row);

            if (empty($row) || in_array($row[0], [';', '#'])) {
                continue;
            }

            [$key, $value] = explode('=', $row, 2);
            $env[trim($key)] = trim($value);
        }

        return $env;
    }

    /**
     * Make .env file
     * @param string $envPath
     * @param array $configuration
     */
    public function makeEnvFile(string $envPath, array $configuration): void
    {
        $localConfiguration = $this->getLocalEnv($envPath);
        $file = fopen($envPath, 'wb');
        $configuration = array_merge(
            $localConfiguration,
            $configuration
        );

        foreach ($configuration as $key => $value) {
            fwrite($file, "$key=$value\r\n");
        }

        fclose($file);
    }

    /**
     * Get the application configuration by uuid
     * @param string|null $uuid
     * @return array|null
     * @throws \JsonException
     */
    public function getAppConfigByUuid(?string $uuid): ?array
    {
        if (empty($uuid)) {
            return null;
        }

        return $this->getConfigFromCache(
            $this->getCacheKeyForApp($uuid, config('app.config_version'))
        );
    }

    /**
     * Get the application configuration by uuid from header
     * @return array|null
     * @throws \JsonException
     */
    public function getAppConfiguration(): ?array
    {
        return $this->getAppConfigByUuid(request()->header(HeadersDefinition::APP_ID));
    }

    /**
     * Load configuration for mobile application from Redis
     * @return void
     * @throws \JsonException
     */
    public function loadMobileAppConfiguration(): void
    {
        $appUuid = request()->header(HeadersDefinition::APP_ID);

        if ($appUuid === null) {
            return;
        }

        $config = $this->getConfigFromCache($this->getCacheKeyForApp($appUuid, config('app.config_version')));

        if (empty($config)) {
            return;
        }

        foreach ($config as $key => $value) {
            app('config')->set($key, $value);
        }
    }
}
