## Logger
Eternity logger with configuration file for laravel

### Usage
Creating a Channel Handler ```eternity```.

> Type `general` denotes `general` section in handlers configuration.

Config example:
```php
return [
    'channels' => [
        ...
        'eternity' => [
            /*
             * Eternity logger configuration
             */
            'driver'    => 'custom',
            'via'       => \Eternity\Laravel\Components\Logger\Factories\CreateEternityLogger::class,
            'section'   => env('LOGGER_SECTION', 'local'),
            'debug'     => env('APP_DEBUG'),
            'handlers'  => [
                /*
                 * Sections
                 */
                'local'   => [
                    'rotating-file' => [
                        'level'           => 'debug',
                        'filename'        => storage_path() . '/logs/eternity.log',
                        'type'            => 'rotating-file',
                        'filename_format' => '{filename}-{date}',
                        'formatter'       => 'pipe',
                    ],
                ],
                'general'  => [ // This name must be provided to logger as type
                    /*
                     * handlers of each section
                     */
                    'handler1' => [
                        // String to identify the source of the log message. Default value is 'gw'
                        'type'      => 'syslog',  // Type is required
                        'ident'     => 'gw',
                        'level'     => 'info',
                        // Allowed formatters: pipe, json
                        'formatter' => 'pipe',
                        'facility'  => 'user',
                        'pipe'      => '%timestamp%|%component%|%severity%|%msg-code%|%msg%|%trace-id%|%entity-type%|%entity-id%|%stack-trace%',
                    ],
                    'telegram-bot'  => [
                        'type'      => 'telegram',
                        'api_key'   => env('TELEGRAM_LOG_API_KEY'),
                        'channel'   => env('TELEGRAM_LOG_CHAT_ID'), // Telegram Channel or Group ID
                        'level'     => env('TELEGRAM_ERROR_LEVEL', 'critical'),
                        'formatter' => 'telegram',  // No trace if send error via telegram
                    ],
                ],
            ],
            'formatters' => [
                'pipe'     => '%timestamp%|%component%|%severity%|%msg-code%|%msg%|%trace-id%|%aws-req-id%|%auth-uid%|%client-ip%|%app-id%|%app-version%|%os-type%|%os-version%|%device-name%|%language%|%route%|%entity-type%|%entity-id%|%user-agent%|%stack-trace%',
                'telegram' => '%timestamp%|%client-ip%|%component%|%severity%|%msg-code%|%msg%|%trace-id%|%auth-uid%|%app-id%|%app-version%|%os-type%|%os-version%|%route%|%entity-type%|%entity-id%',
            ],
            'component' => [
                // String to identify the source of the log message. Default value is 'gw'
                // (Microservice name)
                'name' => env('APP_NAME', \Eternity\Microservices::GATEWAY),
            ]
        ],
        ...
    ],
];
```