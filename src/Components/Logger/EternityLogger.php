<?php

namespace Eternity\Laravel\Components\Logger;

use Eternity\Laravel\Components\Logger\Processors\AuthUidProcessor;
use Eternity\Laravel\Components\Logger\Processors\AwsRequestIdProcessor;
use Eternity\Laravel\Components\Logger\Processors\ClientIpProcessor;
use Eternity\Laravel\Components\Logger\Processors\HeadersProcessor;
use Eternity\Laravel\Components\Logger\Processors\RequestBodyProcessor;
use Eternity\Laravel\Components\Logger\Processors\RouteProcessor;
use Eternity\Laravel\Components\Logger\Processors\UserAgentProcessor;
use Eternity\Logger\Interfaces\LogConfig;
use Eternity\Logger\Logger;
use Eternity\Trace\TraceId;
use Illuminate\Http\Request;

/**
 * Class EternityLogger
 * @package Eternity\Laravel\Components\Logger
 */
class EternityLogger extends Logger
{
    /**
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * @param string $type
     * @param \Eternity\Trace\TraceId $traceId
     * @param \Eternity\Logger\Interfaces\LogConfig $config
     * @param \Illuminate\Http\Request|null $request
     */
    public function __construct(string $type, TraceId $traceId, LogConfig $config, Request $request = null)
    {
        parent::__construct($type, $traceId, $config);
        if ($request === null) {
            $request = request();
        }
        $this->request = $request;
    }

    /**
     * @return array|\Monolog\Processor\ProcessorInterface[]
     * @throws \Exception
     */
    public function processors(): array
    {
        return array_merge(parent::processors(), [
            new HeadersProcessor($this->request),
            new ClientIpProcessor($this->request),
            new AuthUidProcessor(),
            new RouteProcessor($this->request),
            new UserAgentProcessor($this->request),
            new AwsRequestIdProcessor($this->request),
            new RequestBodyProcessor($this->request),
        ]);
    }
}