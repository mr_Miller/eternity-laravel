<?php

namespace Eternity\Laravel\Components\Logger\Processors;

use Illuminate\Http\Request;
use Monolog\Processor\ProcessorInterface;

/**
 * Class RouteProcessor
 * @package Eternity\Laravel\Components\Logger\Processors
 */
class RouteProcessor implements ProcessorInterface
{
    public const METHOD = 'method';
    public const ROUTE = 'route';

    /**
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * @param \Illuminate\Http\Request|null $request
     */
    public function __construct(Request $request = null)
    {
        if ($request === null) {
            $request = \request();
        }
        $this->request = $request;
    }

    /**
     * @return array The processed record
     */
    public function __invoke(array $record): array
    {
        $record[self::METHOD] = $this->request->getRealMethod();
        $record[self::ROUTE] = $this->request->getRequestUri();

        return $record;
    }
}