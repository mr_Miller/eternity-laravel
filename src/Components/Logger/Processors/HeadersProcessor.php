<?php

namespace Eternity\Laravel\Components\Logger\Processors;

use Eternity\Definitions\HeadersDefinition;
use Illuminate\Http\Request;
use Monolog\Processor\ProcessorInterface;

/**
 * Class HeadersProcessor
 * @package Eternity\Laravel\Components\Logger\Processors
 */
class HeadersProcessor implements ProcessorInterface
{
    public const APP_ID = 'app-id';
    public const APP_VERSION = 'app-version';
    public const DEVICE_NAME = 'device-name';
    public const OS_VERSION = 'os-version';
    public const OS_TYPE = 'os-type';
    public const LANGUAGE = 'language';

    /**
     * @var array
     */
    protected static $keys = [
        self::APP_ID      => HeadersDefinition::APP_ID,
        self::APP_VERSION => HeadersDefinition::APP_VERSION,
        self::DEVICE_NAME => HeadersDefinition::DEVICE_NAME,
        self::LANGUAGE    => HeadersDefinition::LANG_CODE,
        self::OS_TYPE     => HeadersDefinition::OS_TYPE,
        self::OS_VERSION  => HeadersDefinition::OS_VERSION,
    ];

    /**
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * @param \Illuminate\Http\Request|null $request
     */
    public function __construct(Request $request = null)
    {
        if ($request === null) {
            $request = \request();
        }
        $this->request = $request;
    }

    /**
     * @return array The processed record
     */
    public function __invoke(array $record): array
    {
        foreach (self::getKeys() as $key => $headerKey) {
            $record[$key] = $this->request instanceof Request ? $this->request->header($headerKey, '') : '';
        }

        return $record;
    }

    /**
     * @return array
     */
    public static function getKeys(): array
    {
        return self::$keys;
    }
}