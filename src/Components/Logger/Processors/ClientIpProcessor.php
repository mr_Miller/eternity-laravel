<?php

namespace Eternity\Laravel\Components\Logger\Processors;

use Illuminate\Http\Request;
use Monolog\Processor\ProcessorInterface;

/**
 * Class ClientIpProcessor
 * @package Eternity\Laravel\Components\Logger\Processors
 */
class ClientIpProcessor implements ProcessorInterface
{
    public const CLIENT_IP = 'client-ip';

    /**
     * @var \Illuminate\Http\Request|null
     */
    private $request;

    /**
     * @param \Illuminate\Http\Request|null $request
     */
    public function __construct(Request $request = null)
    {
        if ($request === null) {
            $request = \request();
        }
        $this->request = $request;
    }

    /**
     * @return array The processed record
     */
    public function __invoke(array $record): array
    {
        if ($this->request instanceof Request) {
            $record[self::CLIENT_IP] = $this->request->getClientIp();
        }

        return $record;
    }
}