<?php

namespace Eternity\Laravel\Components\Logger\Processors;

use Illuminate\Http\Request;
use Monolog\Processor\ProcessorInterface;

/**
 * Class UserAgentProcessor
 * @package Eternity\Laravel\Components\Logger\Processors
 */
class UserAgentProcessor implements ProcessorInterface
{
    public const USER_AGENT = 'user-agent';

    /**
     * @var \Illuminate\Http\Request|null
     */
    private $request;

    /**
     * @param \Illuminate\Http\Request|null $request
     */
    public function __construct(Request $request = null)
    {
        if ($request === null) {
            $request = \request();
        }
        $this->request = $request;
    }

    /**
     * @return array The processed record
     */
    public function __invoke(array $record): array
    {
        $record[self::USER_AGENT] = $this->request->userAgent();

        return $record;
    }
}