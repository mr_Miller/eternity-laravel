<?php

namespace Eternity\Laravel\Components\Logger\Processors;

use Monolog\Processor\ProcessorInterface;

/**
 * Class AuthUidProcessor
 * @package Eternity\Laravel\Components\Logger\Processors
 */
class AuthUidProcessor implements ProcessorInterface
{
    public const AUTH_UID = 'auth-uid';

    /**
     * @return array The processed record
     */
    public function __invoke(array $record): array
    {
        try {
            $record[self::AUTH_UID] = auth()->check() ? auth()->id() : '';
        } catch (\Throwable $e) {
            $record[self::AUTH_UID] = '';
        }

        return $record;
    }
}