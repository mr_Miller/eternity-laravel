<?php

namespace Eternity\Laravel\Components\Logger\Processors;

use Illuminate\Http\Request;
use Monolog\Processor\ProcessorInterface;

/**
 * Class RequestBodyProcessor
 * @package Eternity\Laravel\Components\Logger\Processors
 */
class RequestBodyProcessor implements ProcessorInterface
{
    public const REQUEST_BODY = 'req-body';

    /**
     * @var array
     */
    private $dontFlash = [
        'password',
        'password_hash',
        'password_confirmation',
    ];

    /**
     * @var \Illuminate\Http\Request|null
     */
    private $request;

    /**
     * @param \Illuminate\Http\Request|null $request
     */
    public function __construct(Request $request = null)
    {
        if ($request === null) {
            $request = \request();
        }
        $this->request = $request;
    }

    /**
     * @return array The processed record
     * @throws \JsonException
     */
    public function __invoke(array $record): array
    {
        if ($this->request instanceof Request) {
            $data = $this->request->all();
            foreach ($this->dontFlash as $key) {
                if (isset($data[$key])) {
                    $data[$key] = '*****';
                }
            }
            $record[self::REQUEST_BODY] = json_encode($data, JSON_THROW_ON_ERROR, 128);
        }

        return $record;
    }
}