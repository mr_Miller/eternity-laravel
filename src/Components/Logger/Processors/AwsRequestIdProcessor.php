<?php

namespace Eternity\Laravel\Components\Logger\Processors;

use Eternity\Definitions\HeadersDefinition;
use Illuminate\Http\Request;
use Monolog\Processor\ProcessorInterface;

/**
 * Class AwsRequestIdProcessor
 * @package Eternity\Laravel\Components\Logger\Processors
 */
class AwsRequestIdProcessor implements ProcessorInterface
{
    public const REQUEST_ID = 'aws-req-id';

    /**
     * @var \Illuminate\Http\Request|null
     */
    private $request;

    /**
     * @param \Illuminate\Http\Request|null $request
     */
    public function __construct(Request $request = null)
    {
        if ($request === null) {
            $request = \request();
        }
        $this->request = $request;
    }

    /**
     * @return array The processed record
     */
    public function __invoke(array $record): array
    {
        if ($this->request instanceof Request) {
            $record[self::REQUEST_ID] = $this->request->header(HeadersDefinition::AWS_REQUEST_ID, '');
        }

        return $record;
    }
}