<?php

namespace Eternity\Laravel\Components\Logger\Factories;

use Eternity\Laravel\Components\Logger\EternityLogger;
use Eternity\Logger\Config\PhpConfig;
use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;

/**
 * Class CreateEternityLogger
 * @package Eternity\Laravel\Components\Logger\Factories
 */
class CreateEternityLogger
{
    /**
     * @param array $config
     * @return \Psr\Log\LoggerInterface
     * @throws \Eternity\Logger\Exceptions\LoggerException
     */
    public function __invoke(array $config): LoggerInterface
    {
        $logConfig = new PhpConfig(app()->mode(), $config);

        $logger = new EternityLogger($config['section'], app()->get('traceId'), $logConfig);
        $logger->setExceptionHandler(function (\Throwable $e, array $record) {
            // Use default laravel logger if logging fails
            Log::channel('single')->error($e);
        });

        return $logger;
    }
}