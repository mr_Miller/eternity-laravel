<?php

namespace Eternity\Laravel\Components\Trace\Factories;

use Eternity\Definitions\ApplicationMode;
use Eternity\Definitions\HeadersDefinition;
use Eternity\Exceptions\ErrorCodes;
use Eternity\Messages\Messages;
use Eternity\Trace\Exceptions\TraceIdException;
use Eternity\Trace\TraceId;

/**
 * Use this factory in provider
 *
 * Class TraceIdFactory
 * @package Eternity\Laravel\Components\Trace\Factories
 */
class TraceIdFactory
{
    /**
     * @var array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Http\Request|string
     */
    private $request;

    /**
     * @var string
     */
    private $prefix;

    /**
     * @var string
     */
    private $mode;

    /**
     * TraceIdFactory constructor.
     * @param string $prefix
     * @param string $mode
     */
    public function __construct(string $prefix, string $mode)
    {
        $this->request = request();
        $this->prefix = $prefix;
        $this->mode = $mode;
    }

    /**
     * Check if it's http request
     * @return bool
     */
    public function isNotConsole(): bool
    {
        return !app()->runningInConsole();
    }

    /**
     * This method must be used across all microservices except Core.
     * @return \Eternity\Trace\TraceId
     * @throws \Eternity\Trace\Exceptions\TraceIdException
     */
    public function create(): TraceId
    {
        $traceId = new TraceId();
        if ($this->isNotConsole()) {
            // For http requests Trace ID must be provided in header
            $traceIdValue = $this->getTraceIdHeader();
            if ($traceIdValue === null) {
                throw new TraceIdException(
                    ErrorCodes::TRACE_ID_HEADER_MISSING,
                    'Trace Id Error',
                    Messages::message(ErrorCodes::TRACE_ID_HEADER_MISSING)
                );
            }
            $traceId->init($traceIdValue);
        } elseif ($this->shouldBeGeneratedInConsole()) {
            $traceId->generate($this->prefix);
        }

        return $traceId;
    }

    /**
     * Returns trace ID from header
     * @return string
     */
    private function getTraceIdHeader(): ?string
    {
        return $this->request->headers->get(HeadersDefinition::TRACE_ID);
    }

    /**
     * @return bool
     */
    private function shouldBeGeneratedInConsole()
    {
        return $this->mode === ApplicationMode::MODE_CRON
            || $this->mode === ApplicationMode::MODE_COMMAND
            || $this->mode === ApplicationMode::MODE_WORKER;
    }

    /**
     * This method must be used ONLY in Core microservice Gateway
     * Create for core microservice
     * @return \Eternity\Trace\TraceId
     * @throws \Exception
     */
    public function createCore(): TraceId
    {
        $traceId = new TraceId();
        if ($this->isNotConsole()) {
            // For http requests Trace ID generates automatically
            $traceId->generate($this->prefix);
        } elseif ($this->shouldBeGeneratedInConsole()) {
            $traceId->generate($this->prefix);
        }

        // For other request types: console, queue worker - Trace ID must be generated manually

        return $traceId;
    }
}