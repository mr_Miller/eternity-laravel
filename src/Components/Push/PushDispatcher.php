<?php

namespace Eternity\Laravel\Components\Push;

use Eternity\Events\Microservices\Notification\Push;
use Eternity\Events\Microservices\Notification\PushMultiple;
use Eternity\Laravel\Components\Event\EventBus;

/**
 * Multiple Push Balancer
 *
 * Class responsible to sending massive number of PUSH-notification with balanced load
 *
 * Gather PUSH-notification until it reaches the limit than release them by sending to the Event-Bus
 * Event-Bus handles notifications itself further
 *
 * Class PushDispatcher
 * @package Eternity\Laravel\Components\Push
 */
class PushDispatcher
{
    /**
     * Limit of notifications for one Multiple Push Event
     */
    public const MAX_NOTIFICATIONS = 500;

    /**
     * @var \Eternity\Laravel\Components\Event\EventBus
     */
    private $eventBus;

    /**
     * @var int Notification counter
     */
    private $counter;

    /**
     * @var \Eternity\Events\Microservices\Notification\PushMultiple
     */
    private $event;

    /**
     * PushDispatcher constructor.
     * @param \Eternity\Laravel\Components\Event\EventBus $eventBus
     */
    public function __construct(EventBus $eventBus)
    {
        $this->eventBus = $eventBus;
        $this->reset();
    }

    /**
     * Reset
     *
     *  - counter
     *  - event class
     */
    private function reset(): void
    {
        $this->counter = 0;
        $this->event = new PushMultiple();
    }

    /**
     * Increases the counter
     */
    private function increase(): void
    {
        $this->counter++;
    }

    /**
     * Adds push
     *
     * @param \Eternity\Events\Microservices\Notification\Push $push
     */
    public function add(Push $push): void
    {
        $this->event->add($push);
        $this->increase();

        if ($this->isLimitReached()) {
            $this->release();
        }
    }

    /**
     * Checks if limit is reached
     * @return bool
     */
    public function isLimitReached(): bool
    {
       return $this->counter >= self::MAX_NOTIFICATIONS;
    }

    /**
     * Release event
     *
     * Release all gathered notifications
     */
    public function release(): void
    {
        $this->eventBus->publish($this->event);
        $this->reset();
    }

    /**
     * Checks if event has notifications
     * @return bool
     */
    public function hasNotifications(): bool
    {
        return $this->counter > 0;
    }

    /**
     * Destructor
     */
    public function __destruct()
    {
        if ($this->hasNotifications()) {
            $this->release();
        }
    }
}