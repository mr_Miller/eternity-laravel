<?php

namespace Eternity\Laravel\Components\Localization;

use Eternity\Definitions\HeadersDefinition;
use Eternity\Laravel\Components\Localization\Factories\RegionFactory;
use Illuminate\Http\Request;

/**
 * Localization service
 *
 * Automatically sets language from request headers according to current region configuration
 *
 * Class LocalizationService
 * @package Eternity\Laravel\Components\Localization
 */
class Localization
{
    /**
     * @var string Current language CODE
     */
    private $languageCode;

    /**
     * @var \Eternity\Laravel\Components\Localization\Region Region configuration
     */
    private $region;

    /**
     * Localization constructor.
     * @param \Eternity\Laravel\Components\Localization\Region|null $region
     * @param \Illuminate\Http\Request|null $request
     * @throws \Eternity\Laravel\Components\Localization\Exceptions\LanguageException
     * @throws \Eternity\Laravel\Components\Localization\Exceptions\RegionException
     */
    public function __construct(Region $region = null, Request $request = null)
    {
        if ($request === null) {
            $request = request();
        }
        if ($region === null) {
            $region = RegionFactory::create();
        }
        $this->region = $region;
        $languageCode = $this->getRequestLanguage($request);
        if ($languageCode === null || !$this->region->isSupported($languageCode)) {
            // If language is not provided
            // OR language is not supported in current region
            $languageCode = $this->defaultLanguage();
        }
        $this->languageCode = $languageCode;
    }

    /**
     * Retrieve language code from request header
     * @param \Illuminate\Http\Request $request
     * @return string
     */
    private function getRequestLanguage(Request $request): ?string
    {
        return $request->headers->get(HeadersDefinition::LANG_CODE);
    }

    /**
     * Returns current region object
     * @return \Eternity\Laravel\Components\Localization\Region
     */
    public function region(): Region
    {
        return $this->region;
    }

    /**
     * Returns default language CODE
     * @return string
     */
    public function defaultLanguage(): string
    {
        return $this->region->getDefaultLanguage();
    }

    /**
     * Returns current language CODE
     * @return string
     */
    public function language(): string
    {
        return $this->languageCode;
    }

}