## Localization service

Add `LOCALIZATION_REGION` to your environment `.env` file.

Example using in `.env`:
```
LOCALIZATION_REGION=ukraine
```

Copy configuration with:
```
php artisan vendor:publish --tag=localization
```

### Usage
Register `Eternity\Laravel\Components\Localization\Localization` in service provider:
```php
/**
 * Registers services in Di container
 */
public function register()
{
    $this->app->singleton(Localization::class, Localization::class);
}
```