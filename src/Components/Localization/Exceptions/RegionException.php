<?php

namespace Eternity\Laravel\Components\Localization\Exceptions;

use Eternity\Exceptions\ErrorCodes;
use Eternity\Exceptions\EternityException;

/**
 * Class RegionException
 * @package App\Infrastructure\Services\Localization\Exceptions
 */
class RegionException extends EternityException
{
    /**
     * RegionException constructor.
     * @param int $code
     * @param string|null $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(
        int $code = ErrorCodes::REGION_ERROR,
        string $title = null,
        string $detail = null,
        \Throwable $previous = null
    ) {
        parent::__construct($title, $detail, $previous);
        $this->code = $code;
    }
}
