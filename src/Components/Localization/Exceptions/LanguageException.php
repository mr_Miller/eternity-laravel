<?php

namespace Eternity\Laravel\Components\Localization\Exceptions;

use Eternity\Exceptions\EternityException;

/**
 * Class LanguageException
 * @package App\Infrastructure\Services\Localization\Exceptions
 */
class LanguageException extends EternityException
{
    /**
     * LanguageException constructor.
     * @param int $code
     * @param string|null $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(
        int $code,
        string $title = null,
        string $detail = null,
        \Throwable $previous = null
    ) {
        parent::__construct($title, $detail, $previous);
        $this->code = $code;
    }
}
