<?php

namespace Eternity\Laravel\Components\Localization;

use Eternity\Definitions\Language\LanguageDefinition;
use Eternity\Exceptions\ErrorCodes;
use Eternity\Laravel\Components\Localization\Definitions\RegionDefinition;
use Eternity\Laravel\Components\Localization\Exceptions\LanguageException;
use Eternity\Laravel\Components\Localization\Exceptions\RegionException;

/**
 * Class Region
 * @package App\Infrastructure\Services\Localization
 */
class Region
{
    /**
     * @var string Default language code
     */
    protected $defaultLanguage;

    /**
     * @var array List of allowed languages in current region
     */
    protected $languages;

    /**
     * @var string Region name
     */
    private $name;

    /**
     * Region constructor.
     * @param string $name
     * @param string $defaultLanguage
     * @param array $languages
     * @throws \Eternity\Laravel\Components\Localization\Exceptions\LanguageException
     * @throws \Eternity\Laravel\Components\Localization\Exceptions\RegionException
     */
    public function __construct(string $name, string $defaultLanguage, array $languages)
    {
        $this->defaultLanguage = $defaultLanguage;
        $this->languages = $languages;
        $this->name = $name;
        $this->validate();
    }

    /**
     * Returns current region default language code
     * @return string
     */
    public function getDefaultLanguage(): string
    {
        return $this->defaultLanguage;
    }

    /**
     * Returns allowed languages for current region
     * @return array
     */
    public function getLanguages(): array
    {
        return $this->languages;
    }

    /**
     * Returns current's region name
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Validate data
     * @throws \Eternity\Laravel\Components\Localization\Exceptions\LanguageException
     * @throws \Eternity\Laravel\Components\Localization\Exceptions\RegionException
     */
    private function validate(): void
    {
        $this->checkLanguage($this->defaultLanguage);
        $this->checkLanguages($this->languages);
        $this->checkRegion($this->name);
    }

    /**
     * Checks if language is allowed in current region
     * @param string $languageCode
     * @return bool
     */
    public function isSupported(string $languageCode): bool
    {
        return in_array($languageCode, $this->languages);
    }

    /**
     * Checks language is defined
     * @param string $language
     * @return void
     * @throws \Eternity\Laravel\Components\Localization\Exceptions\LanguageException
     */
    private function checkLanguage(string $language): void
    {
        if (!in_array($language, LanguageDefinition::getConstList())) {
            throw new LanguageException(
                ErrorCodes::LANGUAGE_NOT_EXISTS_ERROR,
                'Language error',
                'Language [' . $language . '] is not exist'
            );
        }
    }

    /**
     * Checks languages array
     * @param array $languages
     * @throws \Eternity\Laravel\Components\Localization\Exceptions\LanguageException
     */
    private function checkLanguages(array $languages): void
    {
        foreach ($languages as $language) {
            $this->checkLanguage($language);
        }
    }

    /**
     * Check region
     * @param string $name
     * @throws \Eternity\Laravel\Components\Localization\Exceptions\RegionException
     */
    private function checkRegion(string $name): void
    {
        if (!in_array($name, RegionDefinition::getConstList())) {
            throw new RegionException(
                ErrorCodes::REGION_NOT_EXISTS_ERROR,
                'Region error',
                'Region is not exist'
            );
        }
    }
}
