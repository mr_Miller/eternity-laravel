<?php

namespace Eternity\Laravel\Components\Localization\Definitions;

use Eternity\Components\Definition\AbstractBaseDefinition;

/**
 * Class RegionDefinition
 * @package Eternity\Laravel\Components\Localization\Definition
 */
final class RegionDefinition extends AbstractBaseDefinition
{
    public const UKRAINE = 'ukraine';

    /**
     * Method builds the whole definition structure
     *
     * Every array item must have next structure:
     *  - id
     *  - title
     *  - translation
     *
     * For example:
     *  [
     *      [
     *          'id' => 5,
     *          'title' => 'String description',
     *          'translation_key' => 'Key for user readable translation',
     *      ],
     *      ...
     *  ]
     *
     * @return array
     */
    public static function getDefinitionsList(): array
    {
        return [
            [
                'id' => self::UKRAINE,
                'title' => 'Ukrainian region'
            ]
        ];
    }

}