<?php

namespace Eternity\Laravel\Components\Localization\Definitions;

use Eternity\Definitions\Country\CountryDefinition;
use Eternity\Exceptions\ErrorCodes;
use Eternity\Laravel\Components\Localization\Exceptions\RegionException;
use Eternity\Laravel\Components\Localization\Phone;
use Illuminate\Support\Arr;

/**
 * Country phone map
 *
 * Definition is responsible for linking Country definition with phones standardization as prefix, regex
 *
 * Class RegionPhoneDefinition
 * @package Eternity\Laravel\Components\Localization\Definitions
 */
class CountryPhoneDefinition
{
    /**
     * List of countries with phone prefixes and Regex statements
     */
    private const COUNTRY_PHONES = [
        CountryDefinition::UKRAINE => [
            'regex' => '/^\+38\d{10}$/', // Example: +380987928159
            'prefix' => '+38',
        ],
    ];

    /**
     * Phone
     * @param string $countryId In ISO format
     * @return \Eternity\Laravel\Components\Localization\Phone
     */
    public static function phone(string $countryId): Phone
    {
        $phone = Arr::get(static::COUNTRY_PHONES, $countryId, function () use ($countryId){
            throw new RegionException(
                ErrorCodes::COUNTRY_PHONE_IS_NOT_SET_ERROR,
                'Region error',
                "Phone is not set for country (ID $countryId)"
            );
        });

        return new Phone($phone['prefix'], $phone['regex']);
    }
}