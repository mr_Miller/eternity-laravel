<?php

namespace Eternity\Laravel\Components\Localization\Definitions;

use Eternity\Definitions\Language\LanguageDefinition;
use Eternity\Exceptions\ErrorCodes;
use Eternity\Laravel\Components\Localization\Exceptions\RegionException;
use Illuminate\Support\Arr;

/**
 * This section must be strictly reviewed
 * Class RegionLanguageDefinition
 * @package Eternity\Laravel\Components\Localization\Definition
 */
final class RegionLanguageDefinition
{
    /**
     * Each region has it's single default language
     *
     * ATTENTION: Changing default region will cause error on selecting translated entities, because entity translation
     * possibly can be missing
     */
    private const REGION_DEFAULT_LANGUAGE = [
        RegionDefinition::UKRAINE => LanguageDefinition::ENG
    ];

    /**
     * Each region has list of allowed languages
     */
    private const REGION_LANGUAGES_LIST = [
        RegionDefinition::UKRAINE => [
            LanguageDefinition::ENG,
            LanguageDefinition::RUS,
            LanguageDefinition::UKR,
        ]
    ];

    /**
     * Returns region default language
     * @param string $region
     * @return string
     */
    public static function getRegionDefault(string $region): string
    {
        return Arr::get(self::REGION_DEFAULT_LANGUAGE, $region, function () use ($region) {
            throw new RegionException(
                ErrorCodes::REGION_DEFAULT_LANGUAGE_NOT_SET_ERROR,
                'Region error',
                'Default language for region ' . $region . ' is not set'
            );
        });
    }

    /**
     * Returns region languages list
     * @param string $region
     * @return array
     */
    public static function getRegionLanguagesList(string $region): array
    {
        return Arr::get(self::REGION_LANGUAGES_LIST, $region, function () use ($region) {
            throw new RegionException(
                ErrorCodes::REGION_LANGUAGES_NOT_SET_ERROR,
                'Region error',
                'Region languages list is not set'
            );
        });
    }
}