<?php

namespace Eternity\Laravel\Components\Localization\Definitions;

use Eternity\Definitions\Country\CountryDefinition;
use Eternity\Exceptions\ErrorCodes;
use Eternity\Laravel\Components\Localization\Exceptions\RegionException;
use Illuminate\Support\Arr;

/**
 * Class RegionCountryDefinition
 * @package Eternity\Laravel\Components\Localization\Definitions
 */
final class RegionCountryDefinition
{
    /**
     * List of country that are related to region
     */
    private const REGION_COUNTRIES = [
        RegionDefinition::UKRAINE => [
            CountryDefinition::UKRAINE,
        ],
    ];

    /**
     * Return list of countries
     *
     * @param string $regionName
     * @return array
     */
    public static function countries(string $regionName): array
    {
        return Arr::get(static::REGION_COUNTRIES, $regionName, function () {
            throw new RegionException(
                ErrorCodes::REGION_COUNTRIES_NOT_SET_ERROR,
                'Region error',
                'Country list is not set for Region'
            );
        });
    }
}