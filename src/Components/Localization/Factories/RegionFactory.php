<?php

namespace Eternity\Laravel\Components\Localization\Factories;

use Eternity\Exceptions\ErrorCodes;
use Eternity\Laravel\Components\Localization\Definitions\RegionLanguageDefinition;
use Eternity\Laravel\Components\Localization\Exceptions\RegionException;
use Eternity\Laravel\Components\Localization\Region;

/**
 * Class RegionFactory
 * @package App\Infrastructure\Services\Localization
 */
class RegionFactory
{
    /**
     * Create Region object
     * @param string|null $region
     * @return \Eternity\Laravel\Components\Localization\Region
     * @throws \Eternity\Laravel\Components\Localization\Exceptions\LanguageException
     * @throws \Eternity\Laravel\Components\Localization\Exceptions\RegionException
     */
    public static function create(string $region = null): Region
    {
        if (is_null($region)) {
            $region = config('localization.region', null);
            if (is_null($region)) {
                throw new RegionException(
                    ErrorCodes::REGION_NOT_SET_ERROR,
                    'Region error',
                    'Region is not set in config'
                );
            }
        }

        return new Region(
            $region,
            RegionLanguageDefinition::getRegionDefault($region),
            RegionLanguageDefinition::getRegionLanguagesList($region)
        );
    }
}
