<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Current region for application
    |--------------------------------------------------------------------------
    */
    'region' => env('LOCALIZATION_REGION', null),
];