<?php

namespace Eternity\Laravel\Components\Localization;

use Eternity\Contracts\Arrayable;

/**
 * Class Phone
 * @package Eternity\Laravel\Components\Localization
 */
class Phone implements Arrayable
{
    /**
     * @var string
     */
    private $prefix;

    /**
     * @var string
     */
    private $regex;

    /**
     * Phone constructor.
     * @param string $prefix
     * @param string $regex
     */
    public function __construct(string $prefix, string $regex)
    {
        $this->prefix = $prefix;
        $this->regex = $regex;
    }

    /**
     * @return string
     */
    public function getPrefix(): string
    {
        return $this->prefix;
    }

    /**
     * @return string
     */
    public function getRegex(): string
    {
        return $this->regex;
    }

    /**
     * Transforms object into array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'prefix' => $this->prefix,
            'regex'  => $this->regex,
        ];
    }

}