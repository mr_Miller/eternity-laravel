## Secrets manager
Eternity secrets manager with configuration file for laravel.

### Install
__1.__ Publish secrets-manager config
`php artisan vendor:publish --tag=eternity.secrets-manager`

__2.__ Add `SecretsManagerAdaptersProvider`
provider to `config/app.php` __providers__ for secrets manager adapters setup.

Example:

```php
'providers' => [
    /*
     * Package Service Providers...
     */
    Eternity\Laravel\Components\SecretsManager\Providers\SecretsManagerAdaptersProvider::class,
];
```
