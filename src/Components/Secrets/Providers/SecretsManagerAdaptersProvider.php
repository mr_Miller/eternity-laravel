<?php

namespace Eternity\Laravel\Components\Secrets\Providers;

use Aws\Credentials\Credentials;
use Aws\SecretsManager\SecretsManagerClient;
use Eternity\Components\Secrets\Aws\AwsAdapter;
use Illuminate\Support\ServiceProvider;

/**
 * Class SecretsManagerAdaptersProvider
 * @package Eternity\Laravel\Components\Secrets\Providers
 */
class SecretsManagerAdaptersProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SecretsManagerClient::class, function () {
            $credentials = new Credentials(
                config('secrets-manager.credentials.aws.key'),
                config('secrets-manager.credentials.aws.secret')
            );

            return new SecretsManagerClient([
                'credentials' => $credentials,
                'region'      => config('secrets-manager.region'),
                'version'     => config('secrets-manager.version'),
            ]);
        });

        $this->app->singleton(AwsAdapter::class, function () {
            return new AwsAdapter($this->app->make(SecretsManagerClient::class));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../Components/SecretsManager/Configs/secrets-manager.php' => config_path('secrets-manager.php'),
        ], 'eternity.secrets-manager');
    }
}
