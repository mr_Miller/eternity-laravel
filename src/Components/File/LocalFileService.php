<?php

namespace Eternity\Laravel\Components\File;

use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;

/**
 * Class LocalFileService
 * @package Eternity\Laravel\Components\File
 */
class LocalFileService extends FileDecorator
{
    /**
     * LocalFileService constructor.
     * @param \League\Flysystem\Adapter\Local $local
     * @param array $config
     */
    public function __construct(Local $local, array $config = [])
    {
        $filesystem = new Filesystem($local, $config);
        parent::__construct($filesystem);
    }
}