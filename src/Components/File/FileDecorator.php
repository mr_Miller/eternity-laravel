<?php

namespace Eternity\Laravel\Components\File;

use Eternity\Laravel\Components\File\Exceptions\DirectoryNotCreatedException;
use Eternity\Laravel\Components\File\Exceptions\DirectoryNotDeletedException;
use Eternity\Laravel\Components\File\Exceptions\FileContentException;
use Eternity\Laravel\Components\File\Exceptions\FileNotDeletedException;
use Eternity\Laravel\Components\File\Exceptions\FileReadException;
use Eternity\Laravel\Components\File\Exceptions\FileUploadException;
use Illuminate\Http\UploadedFile;
use League\Flysystem\FilesystemInterface;
use Ramsey\Uuid\Uuid;

/**
 * Filesystem decorator
 *
 * Class FilesystemDecorator
 * @package Eternity\Laravel\Components\File
 */
class FileDecorator
{
    /**
     * @var \League\Flysystem\FilesystemInterface
     */
    private $filesystem;

    /**
     * FileDecorator constructor.
     * @param \League\Flysystem\FilesystemInterface $filesystem
     */
    public function __construct(FilesystemInterface $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    /**
     * Generate hash
     * @param string $extension
     * @return string
     * @throws \Exception
     */
    public static function generateName(string $extension): string
    {
        return Uuid::uuid4()->toString() . '.' . $extension;
    }

    /**
     * Put uploaded file
     *
     * @param string $name
     * @param string $path
     * @param \Illuminate\Http\UploadedFile $file
     * @throws \Eternity\Laravel\Components\File\Exceptions\FileContentException
     * @throws \Eternity\Laravel\Components\File\Exceptions\FileUploadException
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function putUploadFile(string $name, string $path, UploadedFile $file): void
    {
        $content = $file->get();
        if ($content === false) {
            throw new FileContentException();
        }
        $this->put($name, $path, $content);
    }

    /**
     * Put
     *
     * @param string $name
     * @param string $path
     * @param string $content
     * @throws \Eternity\Laravel\Components\File\Exceptions\FileUploadException
     */
    public function put(string $name, string $path, string $content): void
    {
        if ($this->filesystem->put($path . '/' . $name, $content) === false) {
            throw new FileUploadException();
        }
    }

    /**
     * Get file
     *
     * @param string $name
     * @param string $path
     * @return string File content
     * @throws \Eternity\Laravel\Components\File\Exceptions\FileReadException
     * @throws \League\Flysystem\FileNotFoundException
     */
    public function get(string $name, string $path): string
    {
        $content = $this->filesystem->read($path . '/' . $name);
        if ($content === false) {
            throw new FileReadException();
        }

        return $content;
    }

    /**
     * Remove file
     *
     * Completely deletes file from storage
     *
     * @param string $name
     * @param string $path
     * @throws \Eternity\Laravel\Components\File\Exceptions\FileNotDeletedException
     * @throws \League\Flysystem\FileNotFoundException
     */
    public function remove(string $name, string $path): void
    {
        if ($this->filesystem->delete($path . '/' . $name) === false) {
            throw new FileNotDeletedException();
        }
    }

    /**
     * Add directory
     *
     * @param string $name
     * @param string $path
     * @throws \Eternity\Laravel\Components\File\Exceptions\DirectoryNotCreatedException
     */
    public function makeDirectory(string $name, string $path): void
    {
        if ($this->filesystem->createDir($path . '/' . $name) === false) {
            throw new DirectoryNotCreatedException();
        }
    }

    /**
     * Remove directory
     *
     * @param string $path
     * @throws \Eternity\Laravel\Components\File\Exceptions\DirectoryNotDeletedException
     */
    public function removeDirectory(string $path): void
    {
        if ($this->filesystem->deleteDir($path) === false) {
            throw new DirectoryNotDeletedException();
        }
    }
}