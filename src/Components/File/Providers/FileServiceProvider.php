<?php

namespace Eternity\Laravel\Components\File\Providers;

use Aws\S3\S3Client;
use Eternity\Laravel\Components\File\LocalFileService;
use Eternity\Laravel\Components\File\S3FileService;
use Illuminate\Support\ServiceProvider;
use League\Flysystem\Adapter\Local;
use League\Flysystem\AwsS3v3\AwsS3Adapter;

/**
 * Class FileServiceProvider
 * @package Eternity\Laravel\Components\File\Providers
 */
class FileServiceProvider extends ServiceProvider
{
    /**
     * Register services
     */
    public function register()
    {
        $this->app->singleton(S3FileService::class, function () {
            $s3Client = new S3Client([
                'credentials' => [
                    'key' => config('filesystems.disks.s3.key'),
                    'secret' => config('filesystems.disks.s3.secret'),
                ],
                'region' => config('filesystems.disks.s3.region'),
                'version' => 'latest',
            ]);
            $s3Adapter = new AwsS3Adapter($s3Client, config('filesystems.disks.s3.bucket'));
            return new S3FileService($s3Adapter);
        });

        $this->app->singleton(LocalFileService::class, function () {
            $localAdapter = new Local(config('filesystems.disks.local.root'));
            return new LocalFileService($localAdapter);
        });
    }
}