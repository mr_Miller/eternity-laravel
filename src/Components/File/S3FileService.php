<?php

namespace Eternity\Laravel\Components\File;

use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;

/**
 * Class S3FileService
 * @package Eternity\Laravel\Components\File
 */
class S3FileService extends FileDecorator
{
    /**
     * S3FileService constructor.
     * @param \League\Flysystem\AwsS3v3\AwsS3Adapter $awsS3Adapter
     * @param array $config
     */
    public function __construct(AwsS3Adapter $awsS3Adapter, array $config = [])
    {
        $filesystem = new Filesystem($awsS3Adapter, $config);
        parent::__construct($filesystem);
    }
}