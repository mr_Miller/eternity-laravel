<?php

namespace Eternity\Laravel\Components\File\Exceptions;

use Eternity\Exceptions\ErrorCodes;
use Eternity\Exceptions\ServerException;
use Throwable;

/**
 * Class FileContentException
 * @package Eternity\Laravel\Components\File\Exceptions
 */
class FileContentException extends ServerException
{
    /**
     * FileContentException constructor.
     * @param string|null $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(
        string $title = 'File content error',
        string $detail = 'Can\'t upload file with empty content',
        Throwable $previous = null
    ) {
        parent::__construct($title, $detail, $previous);
        $this->type = 'FileContentException';
        $this->code = ErrorCodes::FILE_CONTENT_ERROR;
    }
}