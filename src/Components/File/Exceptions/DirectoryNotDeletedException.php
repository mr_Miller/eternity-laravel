<?php

namespace Eternity\Laravel\Components\File\Exceptions;

use Eternity\Exceptions\ErrorCodes;
use Eternity\Exceptions\ServerException;
use Throwable;

/**
 * Class DirectoryNotDeletedException
 * @package Eternity\Laravel\Components\File\Exceptions
 */
class DirectoryNotDeletedException extends ServerException
{
    /**
     * FileNotDeletedException constructor.
     * @param string|null $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(string $title = 'Not deleted', string $detail = 'Directory not deleted', Throwable $previous = null)
    {
        parent::__construct($title, $detail, $previous);
        $this->type = 'DirectoryNotDeletedException';
        $this->code = ErrorCodes::DIRECTORY_NOT_DELETED_ERROR;
    }
}