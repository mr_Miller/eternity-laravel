<?php

namespace Eternity\Laravel\Components\File\Exceptions;

use Eternity\Exceptions\ErrorCodes;
use Eternity\Exceptions\ServerException;
use Throwable;

/**
 * Class FileReadException
 * @package Eternity\Laravel\Components\File\Exceptions
 */
class FileReadException extends ServerException
{
    /**
     * FileNotFound constructor.
     * @param string|null $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(string $title = 'Not found', string $detail = 'File can\'t be read', Throwable $previous = null)
    {
        parent::__construct($title, $detail, $previous);
        $this->type = 'FileReadException';
        $this->code = ErrorCodes::FILE_READ_ERROR;
    }
}