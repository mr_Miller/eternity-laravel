<?php

namespace Eternity\Laravel\Components\File\Exceptions;

use Eternity\Exceptions\ErrorCodes;
use Eternity\Exceptions\ServerException;
use Throwable;

/**
 * Class FileNotDeletedException
 * @package Eternity\Laravel\Components\File\Exceptions
 */
class FileNotDeletedException extends ServerException
{
    /**
     * FileNotDeletedException constructor.
     * @param string|null $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(string $title = 'Not deleted', string $detail = 'File not deleted', Throwable $previous = null)
    {
        parent::__construct($title, $detail, $previous);
        $this->type = 'FileNotDeletedException';
        $this->code = ErrorCodes::FILE_NOT_DELETED_ERROR;
    }
}