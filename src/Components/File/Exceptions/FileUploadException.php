<?php

namespace Eternity\Laravel\Components\File\Exceptions;

use Eternity\Exceptions\ErrorCodes;
use Eternity\Exceptions\ServerException;
use Throwable;

/**
 * Class FileUploadException
 * @package Eternity\Laravel\Components\File\Exceptions
 */
class FileUploadException extends ServerException
{
    /**
     * FileUploadException constructor.
     * @param string|null $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(string $title = 'File upload error', string $detail = 'Can\'t upload file to storage', Throwable $previous = null)
    {
        parent::__construct($title, $detail, $previous);
        $this->type = 'FileUploadException';
        $this->code = ErrorCodes::FILE_UPLOAD_ERROR;
    }
}