<?php

namespace Eternity\Laravel\Components\File\Exceptions;

use Eternity\Exceptions\ErrorCodes;
use Eternity\Exceptions\ServerException;
use Throwable;

/**
 * Class FileNotDeletedException
 * @package Eternity\Laravel\Components\File\Exceptions
 */
class DirectoryNotCreatedException extends ServerException
{
    /**
     * FileNotDeletedException constructor.
     * @param string|null $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(string $title = 'Not created', string $detail = 'Directory not created', Throwable $previous = null)
    {
        parent::__construct($title, $detail, $previous);
        $this->type = 'DirectoryNotCreatedException';
        $this->code = ErrorCodes::DIRECTORY_NOT_CREATED_ERROR;
    }
}