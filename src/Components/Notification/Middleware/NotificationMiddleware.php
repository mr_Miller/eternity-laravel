<?php

namespace Eternity\Laravel\Components\Notification\Middleware;

use Closure;
use Eternity\Components\Connector\Notification\Firebase\MessageDispatcher;

/**
 * Class NotificationMiddleware
 * @package Eternity\Laravel\Middleware
 */
class NotificationMiddleware
{
    /**
     * Handle
     * @param $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    /**
     * Terminate
     *
     * Runs code before terminate application
     * @param $request
     * @param $response
     */
    public function terminate($request, $response)
    {
        $messageDispatcher = app(MessageDispatcher::class);
        $messageDispatcher->dispatch();
    }
}