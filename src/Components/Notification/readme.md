### Notification Middleware
Sends deferred push messages to notification service after response is sent.

### Requirements
1. `Eternity\Laravel\Providers\EternityProvider` must be published into project
2. Notification microservice config must be set in `config\services` **internal** section.
Example:
```php
'internal' => [
    'notification' => [
        'url' => env('NOTIFICATION_URL'),
    ]
],
```  

### Install
__1.__ Add 
`Eternity\Laravel\Components\Notification\Middleware\NotificationMiddleware` 
to global middleware list in `App\Interfaces\Http\Kernel`. Example:
```php
protected $middleware = [
    ...
    Eternity\Laravel\Components\Notification\Middleware\NotificationMiddleware::class
];
```
__2.__ Add `Eternity\Laravel\Components\Notification\Providers\NotificationProvider`
provider to `config/app.php` __providers__.
Example:
```php
'providers' => [
    /*
     * Package Service Providers...
     */
    Eternity\Laravel\Components\Notification\Providers\NotificationProvider::class,
];
```