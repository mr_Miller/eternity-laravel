<?php

namespace Eternity\Laravel\Components\Notification\Providers;

use Eternity\Components\Connector\Notification\Firebase\MessageDispatcher;
use Eternity\Components\Connector\Notification\NotificationConnector;
use Eternity\Laravel\Components\Notification\Factories\NotificationFactory;
use Illuminate\Support\ServiceProvider;

/**
 * Class NotificationProvider
 * @package Eternity\Laravel\Components\Notification\Providers
 */
class NotificationProvider extends ServiceProvider
{
    /**
     * Register
     */
    public function register()
    {
        $this->app->singleton(NotificationConnector::class, function () {
            return NotificationFactory::create();
        });
        $this->app->singleton(MessageDispatcher::class, MessageDispatcher::class);
    }
}