<?php

namespace Eternity\Laravel\Components\Notification\Factories;

use Eternity\Components\Connector\Notification\NotificationConnector;
use Eternity\Definitions\HeadersDefinition;
use Eternity\Exceptions\ConfigException;
use Eternity\Http\Client;
use Eternity\Laravel\Components\Localization\Localization;
use Eternity\Microservices;

/**
 * Notification connector factory
 * Class NotificationFactory
 * @package Eternity\Laravel\Components\Notification\Factories
 */
abstract class NotificationFactory
{
    /**
     * @return \Eternity\Components\Connector\Notification\NotificationConnector
     * @throws \Eternity\Exceptions\ConfigException
     */
    public static function create(): NotificationConnector
    {
        $config = config('services.internal');
        if (!isset($config[Microservices::NOTIFICATION])) {
            throw new ConfigException('Config error', "Notification config is not set in 'services.internal'");
        }
        $notificationConfig = $config[Microservices::NOTIFICATION];
        if (!isset($notificationConfig['url'])) {
            throw new ConfigException('Config error', 'Notification service url is not set');
        }
        $localization = app(Localization::class);
        // Language
        $headers[HeadersDefinition::LANG_CODE] = $localization->language();
        // Authorize
        if (isset($notificationConfig['credentials']['username']) && isset($notificationConfig['credentials']['password'])) {
            $headers[HeadersDefinition::AUTHORIZATION] = self::authorize(
                $notificationConfig['credentials']['username'],
                $notificationConfig['credentials']['password']
            );
        }

        return new NotificationConnector(
            app(Client::class),
            $notificationConfig['url'],
            app('traceId'),
            $headers
        );
    }

    /**
     * Authorize internal
     * @param string $username
     * @param string $password
     * @return string
     */
    private static function authorize(string $username, string $password): string
    {
        return 'Basic ' . base64_encode($username . ':' . $password);
    }
}