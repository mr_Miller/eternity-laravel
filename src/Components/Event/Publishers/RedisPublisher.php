<?php

namespace Eternity\Laravel\Components\Event\Publishers;

use Eternity\Events\Contracts\Event;
use Eternity\Events\Events;
use Eternity\Laravel\Components\Event\Contracts\Publisher;
use Eternity\Laravel\Components\Event\Jobs\HandlerJob;
use Eternity\Laravel\Components\Redis\RedisManager;
use Eternity\Laravel\Facades\TraceId;
use Eternity\Microservices;

/**
 * Event Bus Redis publisher
 *
 * To use this external 'Event-Bus' publisher Redis connection and queue must be configured locally
 *
 * Class BusRedisPublisher
 * @package Eternity\Laravel\Components\Event\Publishers
 */
class RedisPublisher implements Publisher
{
    public const KEY_PREFIX = '_database_';

    /**
     * @var string
     */
    private $connection;

    /**
     * @var string
     */
    private $queue;

    /**
     * @var \Eternity\Laravel\Components\Redis\RedisManager
     */
    private $redisManager;

    /**
     * BusRedisPublisher constructor.
     * @param \Eternity\Laravel\Components\Redis\RedisManager $redisManager
     * @param string $connection
     * @param string $queue
     */
    public function __construct(RedisManager $redisManager, string $connection = 'event', string $queue = 'events')
    {
        $this->connection = $connection;
        $this->queue = $queue;
        $this->redisManager = $redisManager;
    }

    /**
     * Publish to bus
     *
     * @param \Eternity\Events\Contracts\Event $event
     * @throws \Eternity\Events\Exceptions\EventException
     */
    public function publish(Event $event): void
    {
        $this->redisManager->setPrefix($this->getPrefix());
        HandlerJob::dispatch(Events::getType(get_class($event)), $event->serialize(), TraceId::value())
            ->onConnection($this->connection)
            ->onQueue($this->queue);
    }

    /**
     * Returns prefix
     *
     * @return string
     */
    protected function getPrefix(): string
    {
        return Microservices::EVENT . self::KEY_PREFIX;
    }
}