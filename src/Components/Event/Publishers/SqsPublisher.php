<?php

namespace Eternity\Laravel\Components\Event\Publishers;

use Eternity\Events\Contracts\Event;
use Eternity\Laravel\Components\Event\Contracts\Publisher;
use Eternity\Laravel\Components\Event\Jobs\BaseEventJob;
use Eternity\Laravel\Facades\TraceId;
use Illuminate\Queue\QueueManager;
use Ramsey\Uuid\Uuid;

/**
 * Class SqsPublisher
 * @package Eternity\Laravel\Components\Event\Publishers
 */
class SqsPublisher implements Publisher
{
    public const BUS_SUFFIX = 'bus';

    /** @var string */
    private $microserviceName;

    /** @var \Illuminate\Queue\QueueManager */
    private $queue;

    /**
     * BusRedisPublisher constructor.
     * @param string $microserviceName
     */
    public function __construct(string $microserviceName)
    {
        $this->microserviceName = $microserviceName;
        $this->queue = app(QueueManager::class);
    }

    /**
     * Publish to bus
     *
     * @param \Eternity\Events\Contracts\Event $event
     * @throws \JsonException
     */
    public function publish(Event $event): void
    {
        $payload = [
            'uuid'    => Uuid::uuid4(),
            'job'     => BaseEventJob::class,
            'event'   => get_class($event),
            'traceId' => TraceId::value(),
            'data'    => $event->toArray(),
        ];
        $this->queue->connection($this->getConnectionName())->pushRaw(json_encode($payload, JSON_THROW_ON_ERROR));
    }

    /**
     * @return string
     */
    private function getConnectionName(): string
    {
        return $this->microserviceName . '-' . self::BUS_SUFFIX;
    }
}
