<?php

namespace Eternity\Laravel\Components\Event\Services;

use Eternity\Events\Consumer;
use Eternity\Laravel\Components\Event\Contracts\Broker;
use Eternity\Logger\Messages\CustomLogMessage;
use Illuminate\Support\Facades\Log;

/**
 * Bus service
 *
 * Service responsible for Event handling and delivering them to consumers.
 * Only for Event microservice use
 *
 * Class BusService
 * @package App\Infrastructure\Services\Bus
 */
class BusService
{
    /**
     * @var \Eternity\Laravel\Components\Event\Contracts\Broker
     */
    private $publisher;

    /**
     * @var \Eternity\Events\Consumer
     */
    private $consumer;

    /**
     * BusService constructor.
     * @param \Eternity\Laravel\Components\Event\Contracts\Broker $publisher
     * @param \Eternity\Events\Consumer $consumer
     */
    public function __construct(Broker $publisher, Consumer $consumer)
    {
        $this->publisher = $publisher;
        $this->consumer = $consumer;
    }

    /**
     * Handle event
     *
     * Handles Event-Bus events and sends them across the consumers
     *
     * @param string $type
     * @param string $payload
     * @throws \Eternity\Events\Exceptions\EventException
     */
    public function handle(string $type, string $payload): void
    {
        $consumers = $this->consumer->get($type);

        foreach ($consumers as $consumer) {
            try {
                $this->publisher->send($type, $payload, $consumer);
            } catch (\Throwable $e) {
                Log::error(
                    new CustomLogMessage("Event '$type' has not been delivered to consumer '$consumer'. Payload: $payload"),
                    ['exception' => $e]
                );
            }
        }
    }
}
