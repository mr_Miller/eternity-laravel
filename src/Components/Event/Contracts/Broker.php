<?php

namespace Eternity\Laravel\Components\Event\Contracts;

/**
 * Interface Publisher
 * @package App\Infrastructure\Services\Bus
 */
interface Broker
{
    /**
     * Publish to microservice
     *
     * @param string $type
     * @param string $payload
     * @param string $consumer
     */
    public function send(string $type, string $payload, string $consumer): void;
}
