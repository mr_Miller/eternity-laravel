<?php

namespace Eternity\Laravel\Components\Event\Contracts;

use Eternity\Events\Contracts\Event;

/**
 * Interface BusPublisher
 * @package Eternity\Laravel\Components\Event\Contracts
 */
interface Publisher
{
    /**
     * Publish to bus
     * @param \Eternity\Events\Contracts\Event $event
     */
    public function publish(Event $event): void;
}