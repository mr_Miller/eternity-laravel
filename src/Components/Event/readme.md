### Event-Bus 
We use number __3__ redis database for Event Bus in Event Service, and number __2__ for events 
across other microservices. Choose __3__ database for consumer microservices.

#### Installation
##### 1. Define Redis connections for Event-Bus in `config/database.php`
You need two connections to work with Event-Bus:
1. `event-bus` - for publishing events into Event-Bus (Publisher)
2. `event-consumer` - consuming events from Event-Bus (Consumer)

It's possible to define only one connection for both actions, but this documentation assumes you use separate ones. 
To perform it in redis section create new connection to Event-Bus Redis.
> Pay attention that these connections use same redis but different databases 2 and 3 respectively.

Example:
```php
'redis' => [
    // For publishing to Event-Bus
    'event-bus' => [
        'host' => env('REDIS_HOST', '127.0.0.1'),
        'password' => env('REDIS_PASSWORD', null),
        'port' => env('REDIS_PORT', 6379),
        'database' => env('REDIS_DB', 2),
    ],
    // For consuming from Event-Bus
    'event-consumer' => [
        'host' => env('REDIS_HOST', '127.0.0.1'),
        'password' => env('REDIS_PASSWORD', null),
        'port' => env('REDIS_PORT', 6379),
        'database' => env('REDIS_DB', 3),
    ],
],
```

##### 2. Define queue connections in `config/queue.php` 
This connection is need to consume events from Event-Bus. Event-Bus will send events to this queue.
Example:
```php
'connections' => [
    // For publishing to Event-Bus
    'event-bus' => [
        'driver' => 'redis',
        'connection' => 'event-bus', // Use publisher connection name from step (1)
        'queue' => 'events', // Queue name
        'retry_after' => 90,
        'block_for' => null,
    ],
    // For consuming from Event-Bus
    'event-consumer' => [
        'driver' => 'redis',
        'connection' => 'event-consumer', // Use consumer connection name from step (1)
        'queue' => 'events', // Queue name
        'retry_after' => 90,
        'block_for' => null,
    ],
],
```
- `event-bus` - push event queue name
- `event-consumer` - consume Event queue name

##### 3. Define `EventBus` service in service provider as singleton for publishing Events into Event-Bus.
Use __Publisher__ queue connection name from step (2) as second parameter for `RedisPublisher`.
Example: 
```php
use Illuminate\Support\ServiceProvider;
use Eternity\Laravel\Components\Event\EventBus;
use \Eternity\Laravel\Components\Event\Publishers\RedisPublisher;

class AppServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(EventBus::class, function () {
            $publisher = new RedisPublisher($this->app->make('redis'), 'event-bus', 'events');

            return new EventBus($publisher);
        });
    }
}

``` 

##### 4. Check microservice "name" property is set in `config/microservice.php`
 
##### 5. Run worker to handle consumed events.
Run command in console:
```
php artisan --mode=worker queue:work event-consumer --queue=events
```

### Usage
##### 1. Define events in Eternity package
Documented [here](src/Events).

##### 1. For Publishing events 
Initiate needed event class, fill it with date and publish via `EventBus`.
```php
use Eternity\Events\Microservices\AnimalId\FirstTagActivated;
use Eternity\Laravel\Components\Event\EventBus;

class TestController
{
    /**
     * Test action
     * @return string
     */
    public function test()
    {
        $eventBus = app(EventBus::class);
        $event = new FirstTagActivated();
        $event->fill(['tag' => 'some tag', 'promo' => 'promo-code', 'uid' => 1]);
        $eventBus->publish($event);
    }
}
```

##### 2. For consuming events
Create event listener and register it for need Event as described in 
[laravel documentation](https://laravel.com/docs/7.x/events#registering-events-and-listeners) (Laravel ver. 7 on the moment of developing this documentation).

