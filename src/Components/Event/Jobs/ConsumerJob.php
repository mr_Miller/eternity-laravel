<?php

namespace Eternity\Laravel\Components\Event\Jobs;

use Eternity\Events\Factory\EventFactory;
use Eternity\Laravel\Facades\TraceId;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class ConsumerJob
 * @package Eternity\Laravel\Components\Event\Jobs
 */
class ConsumerJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    private $traceId;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $serialized;

    /**
     * EventJob constructor.
     * @param string $type
     * @param string $serialized
     * @param string $traceId
     */
    public function __construct(string $type, string $serialized, string $traceId)
    {
        $this->traceId = $traceId;
        $this->type = $type;
        $this->serialized = $serialized;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Eternity\Events\Exceptions\EventException
     */
    public function handle()
    {
        // Initialize Trace ID
        TraceId::init($this->traceId);
        // Throw event from Event-Bus locally to application
        event(EventFactory::create($this->type, $this->serialized));
    }
}