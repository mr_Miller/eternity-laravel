<?php

namespace Eternity\Laravel\Components\Event\Jobs;

use Eternity\Laravel\Components\Event\Services\BusService;
use Eternity\Laravel\Facades\TraceId;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Event handler bus job
 *
 * Job that handles published events.
 * This job uses configured redis connection from Laravel DI to deliver events from Event-Bus to consumer microservices.
 *
 * Class HandleJob
 * @package Eternity\Laravel\Components\Event\Jobs
 */
class HandlerJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $payload;

    /**
     * @var string
     */
    private $traceId;

    /**
     * Create a new job instance.
     *
     * @param string $type
     * @param string $payload
     * @param $traceId
     */
    public function __construct(string $type, string $payload, string $traceId)
    {
        $this->type = $type;
        $this->payload = $payload;
        $this->traceId = $traceId;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getPayload(): string
    {
        return $this->payload;
    }

    /**
     * @return string
     */
    public function getTraceId(): string
    {
        return $this->traceId;
    }

    /**
     * Execute the job.
     *
     * @param \Eternity\Laravel\Components\Event\Services\BusService $busService
     * @return void
     * @throws \Eternity\Events\Exceptions\EventException
     */
    public function handle(BusService $busService)
    {
        // Initialize Trace ID
        TraceId::init($this->traceId);
        // Handle events and send them across the subscriber (consumers)
        $busService->handle($this->type, $this->payload);
    }
}
