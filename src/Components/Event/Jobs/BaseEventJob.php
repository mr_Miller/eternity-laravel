<?php

namespace Eternity\Laravel\Components\Event\Jobs;

use Eternity\Laravel\Facades\TraceId;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\Jobs\SqsJob as Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

/**
 * Class BaseEventJob
 * @package Eternity\Laravel\Components\Event\Jobs
 */
class BaseEventJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 60;

    /**
     * Execute the job.
     *
     * @param \Illuminate\Queue\Jobs\SqsJob $job
     * @return void
     * @throws \Eternity\Events\Exceptions\EventException
     * @throws \Eternity\Exceptions\Entity\EntityNotFoundException
     * @throws \Eternity\Exceptions\Entity\EntityNotSavedException
     * @throws \Eternity\Exceptions\EternityException
     * @throws \Exception
     */
    public function fire(Job $job)
    {
        $payload = $job->payload();
        TraceId::init($payload['traceId']);
        Log::debug('START `' . $payload['event'] . '`: ' . $job->getRawBody());

        if (!$job->isDeleted()) {
            $event = app($payload['event']);
            $event->fromArray($payload['data']);
            event($event);
            $job->delete();
        }

        Log::debug('END `' . $payload['event'] . '`');
    }
}