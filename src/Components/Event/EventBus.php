<?php

namespace Eternity\Laravel\Components\Event;

use Eternity\Events\Consumer;
use Eternity\Events\Contracts\Event;
use Eternity\Laravel\Components\Event\Publishers\SqsPublisher;

/**
 * Event bus on Redis
 *
 * Use this Bus to publish event to Global Event Bus
 *
 * Class RedisBus
 * @package Eternity\Laravel\Components\Event
 */
class EventBus
{
    /**
     * Publish event to event bus
     *
     * @param \Eternity\Events\Contracts\Event $event
     * @throws \Eternity\Events\Exceptions\EventException
     * @throws \JsonException
     */
    public function publish(Event $event): void
    {
        $consumer = new Consumer();
        $microServers = $consumer->get(get_class($event));
        foreach ($microServers as $microServer) {
            $publisher = new SqsPublisher($microServer);
            $publisher->publish($event);
        }
    }
}