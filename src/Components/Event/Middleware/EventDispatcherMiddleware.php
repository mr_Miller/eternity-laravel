<?php

namespace Eternity\Laravel\Components\Event\Middleware;

use Closure;
use Eternity\Laravel\Components\Event\Dispatcher\EventDispatcher;

/**
 * Class EventDispatcherMiddleware
 * @package Eternity\Laravel\Components\Event\Middleware
 */
class EventDispatcherMiddleware
{
    /**
     * @var \Eternity\Laravel\Components\Event\EventBus
     */
    private $eventDispatcher;

    /**
     * EventDispatcherMiddleware constructor.
     * @param \Eternity\Laravel\Components\Event\Dispatcher\EventDispatcher $eventDispatcher
     */
    public function __construct(EventDispatcher $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /** @var \Illuminate\Http\Response $response */
        $response = $next($request);

        if ($response->isSuccessful()) {
            $this->eventDispatcher->dispatch();
        } else {
            $this->eventDispatcher->clear();
        }

        return $response;
    }
}