<?php

namespace Eternity\Laravel\Components\Event\Dispatcher;

use Eternity\Events\Contracts\Event;
use Eternity\Laravel\Components\Event\Dispatcher\States\State;

/**
 * Event Dispatcher
 *
 * This class allows to dispatch delayed events.
 * Use this dispatcher if events must be sent to Event Bus only if no errors were occurred during the execution
 * Requires configured Event Bus service
 *
 * Class EventDispatcher
 * @package Eternity\Laravel\Components\Event\Dispatcher
 */
class EventDispatcher implements DispatcherInterface
{
    /**
     * @var \Eternity\Laravel\Components\Event\Dispatcher\States\State
     */
    private $state;

    /**
     * EventDispatcher constructor.
     * @param \Eternity\Laravel\Components\Event\Dispatcher\States\State $state
     */
    public function __construct(State $state)
    {
        $this->changeState($state);
    }

    /**
     * @param \Eternity\Laravel\Components\Event\Dispatcher\States\State $state
     */
    public function changeState(State $state): void
    {
        $this->state = $state;
        $this->state->setContext($this);
    }

    /**
     * Add event to dispatcher queue
     * @param \Eternity\Events\Contracts\Event $event
     * @return \Eternity\Laravel\Components\Event\Dispatcher\EventDispatcher
     */
    public function add(Event $event): DispatcherInterface
    {
        return $this->state->add($event);
    }

    /**
     * Add events to dispatcher queue
     * @param Event[] $events
     */
    public function addMultiple(array $events): void
    {
        $this->state->addMultiple($events);
    }

    /**
     * Dispatch all events from the queue
     */
    public function dispatch(): void
    {
        $this->state->dispatch();
    }

    /**
     * Clear all queued events
     */
    public function clear(): void
    {
        $this->state->clear();
    }
}