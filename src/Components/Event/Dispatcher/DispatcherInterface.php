<?php

namespace Eternity\Laravel\Components\Event\Dispatcher;

use Eternity\Events\Contracts\Event;

/**
 * Interface DispatcherInterface
 * @package Eternity\Laravel\Components\Event\Dispatcher
 */
interface DispatcherInterface
{
    /**
     * Add event to dispatcher queue
     * @param \Eternity\Events\Contracts\Event $event
     * @return \Eternity\Laravel\Components\Event\Dispatcher\DispatcherInterface
     */
    public function add(Event $event): DispatcherInterface;

    /**
     * Add events to dispatcher queue
     * @param Event[] $events
     */
    public function addMultiple(array $events): void;

    /**
     * Dispatch all events from the queue
     */
    public function dispatch(): void;

    /**
     * Clear all queued events
     */
    public function clear(): void;
}