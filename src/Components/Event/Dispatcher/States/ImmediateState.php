<?php

namespace Eternity\Laravel\Components\Event\Dispatcher\States;

use Eternity\Events\Contracts\Event;
use Eternity\Laravel\Components\Event\Dispatcher\DispatcherInterface;

/**
 *
 * Immediate Dispatcher state
 *
 * Defers events immediately
 *
 * Class ImmediateState
 * @package Eternity\Laravel\Components\Event\Dispatcher\States
 */
class ImmediateState extends AbstractState
{
    /**
     * Add event to dispatcher queue
     * @param \Eternity\Events\Contracts\Event $event
     * @return \Eternity\Laravel\Components\Event\Dispatcher\DispatcherInterface
     */
    public function add(Event $event): DispatcherInterface
    {
        $this->eventBus->publish($event);

        return $this;
    }

    /**
     * Add events to dispatcher queue
     * @param Event[] $events
     */
    public function addMultiple(array $events): void
    {
        foreach ($events as $event) {
            $this->add($event);
        }
    }

    /**
     * Dispatch all events from the queue
     */
    public function dispatch(): void
    {
    }

    /**
     * Clear all queued events
     */
    public function clear(): void
    {
    }
}