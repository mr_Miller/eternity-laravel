<?php

namespace Eternity\Laravel\Components\Event\Dispatcher\States;

use Eternity\Laravel\Components\Event\Dispatcher\DispatcherInterface;
use Eternity\Laravel\Components\Event\Dispatcher\EventDispatcher;

/**
 * Interface State
 * @package Eternity\Laravel\Components\Event\Dispatcher\States
 */
interface State extends DispatcherInterface
{
    /**
     * @return \Eternity\Laravel\Components\Event\Dispatcher\EventDispatcher
     */
    public function context(): EventDispatcher;

    /**
     * @param \Eternity\Laravel\Components\Event\Dispatcher\EventDispatcher $dispatcher
     */
    public function setContext(EventDispatcher $dispatcher): void;
}