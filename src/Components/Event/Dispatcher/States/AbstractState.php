<?php

namespace Eternity\Laravel\Components\Event\Dispatcher\States;

use Eternity\Laravel\Components\Event\Dispatcher\EventDispatcher;
use Eternity\Laravel\Components\Event\EventBus;

/**
 * Class AbstractState
 * @package Eternity\Laravel\Components\Event\Dispatcher\States
 */
abstract class AbstractState implements State
{
    /**
     * @var \Eternity\Laravel\Components\Event\Dispatcher\EventDispatcher
     */
    private $context;

    /**
     * @var \Eternity\Laravel\Components\Event\EventBus
     */
    protected $eventBus;

    /**
     * AbstractState constructor.
     * @param \Eternity\Laravel\Components\Event\EventBus $eventBus
     */
    public function __construct(EventBus $eventBus)
    {
        $this->eventBus = $eventBus;
    }

    /**
     * @return \Eternity\Laravel\Components\Event\Dispatcher\EventDispatcher
     */
    public function context(): EventDispatcher
    {
        return $this->context;
    }

    /**
     * @param \Eternity\Laravel\Components\Event\Dispatcher\EventDispatcher $dispatcher
     */
    public function setContext(EventDispatcher $dispatcher): void
    {
        $this->context = $dispatcher;
    }
}