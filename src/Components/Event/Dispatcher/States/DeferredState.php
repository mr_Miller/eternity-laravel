<?php

namespace Eternity\Laravel\Components\Event\Dispatcher\States;

use Eternity\Events\Contracts\Event;
use Eternity\Laravel\Components\Event\Dispatcher\DispatcherInterface;

/**
 * Deferred Dispatcher state
 *
 * Defers event to a queue.
 *
 * Class DeferredState
 * @package Eternity\Laravel\Components\Event\Dispatcher\States
 */
class DeferredState extends AbstractState
{
    /**
     * @var Event[] $events Events queue
     */
    private $events = [];

    /**
     * Add event to dispatcher queue
     * @param \Eternity\Events\Contracts\Event $event
     * @return \Eternity\Laravel\Components\Event\Dispatcher\DispatcherInterface
     */
    public function add(Event $event): DispatcherInterface
    {
        $this->events[] = $event;

        return $this;
    }

    /**
     * Add events to dispatcher queue
     * @param Event[] $events
     */
    public function addMultiple(array $events): void
    {
        foreach ($events as $event) {
            $this->add($event);
        }
    }

    /**
     * Dispatch all events from the queue
     */
    public function dispatch(): void
    {
        foreach ($this->events as $key => $event) {
            $this->eventBus->publish($event);
            unset($this->events[$key]);
        }
        $this->context()->changeState(new ImmediateState($this->eventBus));
    }

    /**
     * Clear all queued events
     */
    public function clear(): void
    {
        $this->events = [];
        $this->context()->changeState(new ImmediateState($this->eventBus));
    }

}