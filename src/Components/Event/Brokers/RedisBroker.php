<?php

namespace Eternity\Laravel\Components\Event\Brokers;

use Eternity\Laravel\Components\Event\Contracts\Broker;
use Eternity\Laravel\Components\Event\Jobs\ConsumerJob;
use Eternity\Laravel\Components\Redis\RedisManager;
use Eternity\Laravel\Facades\TraceId;

/**
 * Redis publisher
 *
 * Manages remote microservices Redis queue connections
 *
 * Class Publisher
 * @package App\Infrastructure\Services\Bus
 */
class RedisBroker implements Broker
{
    public const KEY_PREFIX = '_database_';

    /**
     * @var string
     */
    private $queue;

    /**
     * @var \Eternity\Laravel\Components\Redis\RedisManager
     */
    private $redisManager;

    /**
     * RedisPublisher constructor.
     * @param \Eternity\Laravel\Components\Redis\RedisManager $redisManager
     * @param string $queue
     */
    public function __construct(RedisManager $redisManager, string $queue = 'events')
    {
        $this->redisManager = $redisManager;
        $this->queue = $queue;
    }

    /**
     * Send event to microservice
     *
     * @param string $type
     * @param string $payload
     * @param string $consumer
     */
    public function send(string $type, string $payload, string $consumer): void
    {
        // Set redis queue prefix of consumer microservice
        $this->redisManager->setPrefix($this->getPrefix($consumer));

        // Dispatch consumer that will be handled on consumer microservice
        ConsumerJob::dispatch($type, $payload, TraceId::value())
            ->onConnection($this->getConnectionName($consumer))
            ->onQueue($this->queue());
    }

    /**
     * @param string $consumer
     * @return string
     */
    protected function getConnectionName(string $consumer): string
    {
        return $consumer;
    }

    /**
     * @return string
     */
    public function queue(): string
    {
        return $this->queue;
    }

    /**
     * Returns prefix
     *
     * @param string $consumer
     * @return string
     */
    protected function getPrefix(string $consumer): string
    {
        return $consumer . self::KEY_PREFIX;
    }
}
