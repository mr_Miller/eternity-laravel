<?php

namespace Eternity\Laravel\Console;

use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Input\InputDefinition;

/**
 * Class Input
 * @package Eternity\Laravel\Console
 */
class Input extends ArgvInput
{
    /**
     * @var string|null
     */
    private $mode;

    /**
     * Input constructor.
     * @param array|null $argv
     * @param \Symfony\Component\Console\Input\InputDefinition|null $definition
     */
    public function __construct(array $argv = null, InputDefinition $definition = null)
    {
        // todo Think about this part of code
        // todo Quick fix to resolve errors

        if (null === $argv) {
            $argv = $_SERVER['argv'];
        }

        // strip the application name
        array_shift($argv);
        $this->setTokens($argv);

        // Detect mode
        if ($this->hasParameterOption('--mode')) {
            $this->mode = $this->getParameterOption('--mode');
            array_shift($argv);
            $this->setTokens($argv);
        }

        // Definitions
        if (null === $definition) {
            $this->definition = new InputDefinition();
        } else {
            $this->bind($definition);
            $this->validate();
        }
    }

    /**
     * @return string|null
     */
    public function mode(): ?string
    {
        return $this->mode;
    }
}