### Input
This class is extended base symfony 
`Symfony\Component\Console\Input\ArgvInput`

#### Business Idea
Handle all console command aimed to application and determine proper Application Mode.
List of available application modes are here: `Eternity\Definitions\ApplicationMode`.

#### Usage 
1. In `artisan` file (the application root directory) replace `Symfony\Component\Console\Input\ArgvInput`  with 
`Eternity\Laravel\Console\Input`.
2. Add parameter `--mode=` right after `artisan` and use any Laravel or custom console command. Example: `php artisan --mode=worker queue:work`.
As the result application set the mode to `worker`. 

The whole file must be like:
```php
#!/usr/bin/env php
<?php

define('LARAVEL_START', microtime(true));

require __DIR__.'/vendor/autoload.php';

$app = require_once __DIR__.'/bootstrap/app.php';

$kernel = $app->make(\App\Interfaces\Console\Kernel::class);

// Here we take the console input
$input = new Eternity\Laravel\Console\Input;
// Set application mode due to --mode parameter in console command
// Parameter must be placed right after 'artisan' parameter
// Example: php artisan --mode=worker {command}
// Here we set application mode up to the --mode parameter
// If parameter is missing the default mode will be set.
$app->initMode($input->mode());

$status = $kernel->handle(
    $input,
    new Symfony\Component\Console\Output\ConsoleOutput
);

$kernel->terminate($input, $status);

exit($status);

```