<?php

namespace Eternity\Laravel\Console\Commands\Configurator;

use Eternity\Laravel\Components\Configurator\Services\ConfigurationService;
use Illuminate\Console\Command;
use Illuminate\Contracts\Container\BindingResolutionException;

class EnvToRedisCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'configurator:copy-env-to-redis
                            {--ver=}
                            {--service=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Copy local .env micro service configuration to Redis';

    /**
     * @param ConfigurationService $configuratorService
     * @throws BindingResolutionException
     * @throws \JsonException
     */
    public function handle(ConfigurationService $configuratorService): void
    {
        $serviceName = $this->option('service') ?? config('app.name');
        $version = $this->option('ver') ?? config('app.config_version');

        $configuration = $configuratorService->getLocalEnv($this->laravel->environmentFilePath());
        $cacheKey = $configuratorService->getCacheKeyForMicroService($serviceName, $version);
        $configuratorService->saveConfigToCache($cacheKey, $configuration);
        $this->info($cacheKey . ' has been completed');
    }
}

