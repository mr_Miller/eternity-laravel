<?php

namespace Eternity\Laravel\Console\Commands\Configurator;

use Eternity\Laravel\Components\Configurator\Services\ConfigurationService;
use Illuminate\Console\Command;

class MakeEnv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'configurator:make-env';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make .env file from configuration from Radis';

    /**
     * @param ConfigurationService $configuratorService
     * @throws \JsonException
     */
    public function handle(ConfigurationService $configuratorService): void
    {
        $cacheKey = $configuratorService->getCacheKeyForMicroService(
            config('app.name'),
            config('app.config_version')
        );

        $configuration = $configuratorService->getConfigFromCache($cacheKey);
        if (empty($configuration)) {
            $this->error($cacheKey . ' is absent into Redis');
            return;
        }

        $configuratorService->makeEnvFile($this->laravel->environmentFilePath(), $configuration);
        $this->info($cacheKey . ' has been saved to .env');
    }
}
