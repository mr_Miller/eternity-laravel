<?php

namespace Eternity\Laravel\Security;

use Illuminate\Support\Collection;

/**
 * Class Crypt
 * @package Eternity\Security
 */
class Crypt
{
    private const DELIMITER = ':';

    /**
     * @var \Illuminate\Support\Collection
     */
    private $storage;

    /**
     * Broadcast constructor.
     *
     * @param array $items
     */
    public function __construct(array $items = [])
    {
        $this->storage = new Collection($items);
    }

    /**
     * Get current storage
     * @return \Illuminate\Support\Collection
     */
    public function storage(): Collection
    {
        return $this->storage;
    }

    /**
     * Encode collection for broadcasting
     * @return string
     */
    public function serialize(): string
    {
        return $this->encode($this->storage->toJson(JSON_THROW_ON_ERROR));
    }

    /**
     * Encodes base64 string
     *
     * @param string $input
     *
     * @return string
     */
    private function encode(string $input): string
    {
        $key = uniqid();

        return base64_encode(base64_encode(self::protect($input, $key)) . self::DELIMITER . $key);
    }

    /**
     * Creates an object initialized from encoded input
     *
     * @param string $input
     *
     * @return \Eternity\Laravel\Security\Crypt
     * @throws \JsonException
     */
    public static function fromInput(string $input): self
    {
        [$protected, $key] = explode(self::DELIMITER, base64_decode($input));
        $data = json_decode(self::protect(base64_decode($protected), $key), true, 5, JSON_THROW_ON_ERROR);

        return new self($data);
    }

    /**
     * Replace bytes in string
     * @param string $input
     * @param string $key
     *
     * @return string
     */
    private static function protect(string $input, string $key): string
    {
        $str_len = strlen($input);
        $key_len = strlen($key);

        for ($i = 0; $i < $str_len; $i++) {
            $input[$i] = ($input[$i] ^ $key[$i % $key_len]);
        }

        return $input;
    }
}
