<?php

namespace Eternity\Laravel\Security\Keeper;

/**
 * Class Keeper
 * @package Eternity\Laravel\Security\Keeper
 */
class Keeper
{
    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $username;

    /**
     * Keeper constructor
     * @param string $username
     * @param string $password
     */
    public function __construct(string $username, string $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * Verifying credentials
     * @param string|null $username
     * @param string|null $password
     *
     * @return bool
     */
    public function verify(?string $username, ?string $password): bool
    {
        if ($username === null || $password === null) {
            return false;
        }

        if (password_verify($this->username, $username) && password_verify($this->password, $password)) {
            return true;
        }

        return false;
    }
}