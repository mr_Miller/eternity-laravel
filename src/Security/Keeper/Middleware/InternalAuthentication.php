<?php

namespace Eternity\Laravel\Security\Keeper\Middleware;

use Closure;
use Eternity\Exceptions\AuthenticationException;
use Eternity\Exceptions\ConfigException;
use Eternity\Laravel\Security\Keeper\Keeper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

/**
 * Class InternalAuthentication
 * @package Eternity\Laravel\Security\Keeper\Middleware
 */
class InternalAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     * @throws \Eternity\Exceptions\AuthenticationException
     * @throws \Eternity\Exceptions\ConfigException
     */
    public function handle(Request $request, Closure $next)
    {
        $credentials = Config::get('microservice.credentials');

        if (!isset($credentials['username']) || !isset($credentials['password'])) {
            throw new ConfigException('Microservice configuration', 'Credentials not defined for the microservice');
        }

        $keeper = new Keeper($credentials['username'], $credentials['password']);

        if ($keeper->verify($request->getUser(), $request->getPassword()) === false) {
            throw new AuthenticationException('Unauthorized', 'Invalid credentials for microservice');
        }

        return $next($request);
    }
}