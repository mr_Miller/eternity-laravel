<?php

namespace Eternity\Laravel\Security\Keeper\Console;

use Illuminate\Console\Command;
use Jackiedo\DotenvEditor\DotenvEditor;

/**
 * Class CleanCommand
 * @package Eternity\Laravel\Security\Keeper\Console
 */
class KeeperCommand extends Command
{
    /**
     * The name and signature of the console command.
     * @var string
     */
    protected $signature = 'keeper:credentials
                            {username : string, user for authentication}
                            {password : string, user password for authentication}';

    /**
     * The console command description.
     * @var string
     */
    protected $description = 'Generating hashes for microservice credentials';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle(): void
    {
        $username = $this->argument('username');
        $password = $this->argument('password');

        $tpl = <<<MSG
        Environment variables:
        INTERNAL_AUTH_USERNAME={$username}
        INTERNAL_AUTH_PASSWORD={$password}
        Were configured in .env file
        MSG;

        $options = [
            [
                'key' => 'INTERNAL_AUTH_USERNAME',
                'value' => $username,
            ],
            [
                'key' => 'INTERNAL_AUTH_PASSWORD',
                'value' => $password,
            ],
        ];

        $editor = new DotenvEditor($this->laravel, $this->laravel->get('config'));
        $editor->setKeys($options)->save();

        $this->comment($tpl);

        $this->info('Username: ' . password_hash($username, PASSWORD_DEFAULT));
        $this->info('Password: ' . password_hash($password, PASSWORD_DEFAULT));

        $this->comment('Set these values for the desired variables in Gateway .env config file');
    }
}
