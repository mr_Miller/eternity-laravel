<?php

namespace Eternity\Laravel\Resource;

use Eternity\Definitions\HeadersDefinition;
use Eternity\Laravel\Contracts\PaginatorInterface;
use Eternity\Laravel\Metadata\Factories\FilterMetadataFactory;
use Eternity\Laravel\Metadata\Factories\MetadataFactory;
use Eternity\Laravel\Metadata\Factories\PaginationMetadataFactory;
use Eternity\Metadata\Contracts\MetadataInterface;
use Eternity\Metadata\Metadata;
use Eternity\Resource\Factories\PaginationFactory;
use Eternity\Resource\Objects\Link;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;

/**
 * Prepare eternity structure Resource
 * {
 *  "payload": array,
 *  "metadata": object,
 *  "links": array,
 *  "message": string
 * }
 * Class Resource
 *
 * @package App\Application\Components\Resource
 */
class Resource extends JsonResource
{
    public const PAYLOAD = 'payload';
    public const LINKS = 'links';
    public const MESSAGE = 'message';
    public const METADATA = 'metadata';
    protected const HEADER_EXTEND = 'x-eternity-expand';

    /**
     * @var array
     */
    protected $payload;

    /**
     * @var MetadataInterface|null
     */
    protected $metadata;

    /**
     * @var string Message
     */
    protected $message;

    /**
     * @var array List of expands
     */
    protected $expands = [];

    /**
     * @var array $routeLinks This links will be added to ROUTE response data
     */
    protected $routeLinks = [];

    /**
     * @var array $entityLinks This links will be add to each entity
     */
    protected $entityLinks = [];

    /**
     * @var array
     */
    protected $cachedExpands;

    /**
     * Resource constructor.
     *
     * @param $resource
     * @param \Eternity\Metadata\Contracts\MetadataInterface|null $metadata
     * @param string|null $message
     */
    public function __construct($resource, MetadataInterface $metadata = null, string $message = null)
    {
        $this->payload = $resource;
        $this->metadata = $metadata;
        $this->message = $message;
        parent::__construct($resource);
    }

    /**
     * Factory create method
     * Creates resource with PaginationMetadata
     *
     * @param \Eternity\Laravel\Contracts\PaginatorInterface $entityPaginator
     * @param \Eternity\Metadata\Metadata|null $metadata
     * @param string|null $message
     * @return $this
     * @throws \Eternity\Laravel\Metadata\Exceptions\ValidationException
     * @throws \Eternity\Resource\Exceptions\MetadataException
     */
    public static function fromPaginator(
        PaginatorInterface $entityPaginator,
        Metadata $metadata = null,
        string $message = null
    ): self {
        if ($metadata === null) {
            $metadata = MetadataFactory::createStatic();
        }
        $pagination = PaginationFactory::createStatic(
            $metadata->page(),
            $entityPaginator->getTotalItems(),
            $entityPaginator->getTotalPages()
        );

        return new static(
            $entityPaginator->getItems(),
            PaginationMetadataFactory::createStatic($metadata, $pagination),
            $message
        );
    }

    /**
     * Factory create method
     * Creates resource with AnalyticsMetadata
     *
     * @param $resource
     * @param \Eternity\Metadata\Metadata|null $metadata
     * @param string|null $message
     * @return $this
     * @throws \Eternity\Resource\Exceptions\MetadataException
     */
    public static function withFilter(
        $resource,
        Metadata $metadata = null,
        string $message = null
    ): self {
        return new static(
            $resource,
            $metadata ? $message : FilterMetadataFactory::createStatic(),
            $message
        );
    }

    /**
     * Adds link to route
     * @param \Eternity\Resource\Objects\Link $link
     * @return $this
     */
    public function addRouteLink(Link $link): self
    {
        $this->routeLinks[] = $link->toArray();

        return $this;
    }

    /**
     * Adds canonical route link
     * @param string $method
     * @param string $href
     * @return $this
     */
    public function addCanonical(string $method, string $href): self
    {
        $this->addRouteLink(new Link('canonical', $method, $href));

        return $this;
    }

    /**
     * Adds links to route
     * @param array $links
     * @return $this
     */
    public function addRouteLinks(array $links): self
    {
        foreach ($links as $link) {
            $this->addRouteLink($link);
        }

        return $this;
    }

    /**
     * Returns route links
     * @return array
     */
    public function getRouteLinks(): array
    {
        return $this->routeLinks;
    }

    /**
     * Adds link to entity
     * @param \Eternity\Resource\Objects\Link $link
     * @return $this
     */
    public function addEntityLink(Link $link): self
    {
        $this->entityLinks[] = $link->toArray();

        return $this;
    }

    /**
     * Adds links to entity
     * @param array $links
     * @return $this
     */
    public function addEntityLinks(array $links): self
    {
        foreach ($links as $link) {
            $this->addEntityLink($link);
        }

        return $this;
    }

    /**
     * Return entity links
     *
     * Compose both defined and dynamically added entity links
     *
     * @return array
     */
    public function getEntityLinks(): array
    {
        $defined = $this->links();
        $linksArray = [];
        foreach ($defined as $link) {
            $linksArray[] = $link->toArray();
        }

        return array_merge($linksArray, $this->entityLinks);
    }

    /**
     * Defines expands
     *
     * @param \Illuminate\Http\Request $request
     */
    private function defineExpands(Request $request): void
    {
        if ($request->hasHeader(HeadersDefinition::EXPAND)) {
            $this->expands = json_decode($request->header(HeadersDefinition::EXPAND));
        }
    }

    /**
     * Prepare array for load more resource
     *
     * @param string $key
     * @return bool
     */
    protected function isExpanded(string $key): bool
    {
        return in_array($key, $this->expands);
    }

    /**
     * Returns cache key for Expand
     *
     * @param string $id
     * @param string $type
     * @return string
     */
    private function getCacheKey(string $id, string $type): string
    {
        return "customCache:$type:$id";
    }

    /**
     * Returns cached expand
     *
     * @param string $id
     * @param string $type
     * @return mixed
     */
    private function getCached(string $id, string $type)
    {
        if (!$this->isCached($id, $type)) {
            return null;
        }

        return $this->cachedExpands[$this->getCacheKey($id, $type)];
    }

    /**
     * Check if expand is cached
     *
     * @param string $id
     * @param string $type
     * @return bool
     */
    private function isCached(string $id, string $type): bool
    {
        return isset($this->cachedExpands[$this->getCacheKey($id, $type)]);
    }

    /**
     * Cache expand
     *
     * @param string $id
     * @param string $type
     * @param string|array|int $value
     */
    private function cache(string $id, string $type, $value): void
    {
        $this->cachedExpands[$this->getCacheKey($id, $type)] = $value;
    }

    /**
     * Returns callable result from cache, or if it does not exist executes the callable and caches the result
     *
     * @param string $id
     * @param string $type
     * @param callable $callable
     * @return mixed
     */
    protected function remember(string $id, string $type, callable $callable)
    {
        if ($this->isCached($id, $type)) {
            return $this->getCached($id, $type);
        }

        $result = $callable();

        $this->cache($id, $type, $result);

        return $result;
    }

    /**
     * Extend parent resolver for build eternity depth payload
     *
     * @param null $request
     *
     * @return array
     */
    public function resolve($request = null): array
    {
        $data = parent::resolve($request);
        $data[self::LINKS] = $this->getEntityLinks();

        return $data;
    }

    /**
     * Extend parent toResponse for build eternity response
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function toResponse($request): JsonResponse
    {
        $data = [];
        $this->defineExpands($request);

        if (!($this->payload instanceof Collection)) {
            $collection = new Collection();
            $collection->add($this->payload);
            $this->payload = $collection;
        }

        foreach ($this->payload as $item) {
            $this->resource = $item;
            $data[] = $this->resolve($request);
        }

        $response = [
            self::PAYLOAD  => $data,
            self::METADATA => $this->metadata ? $this->metadata->toArray() : null,
            self::LINKS    => $this->getRouteLinks(),
            self::MESSAGE  => $this->message,
        ];

        return new JsonResponse($response);
    }

    /**
     * Returns links
     *
     * @return array
     */
    public function links(): array
    {
        return [];
    }
}
