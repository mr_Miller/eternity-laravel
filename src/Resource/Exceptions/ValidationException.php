<?php

namespace Eternity\Laravel\Resource\Exceptions;

use Eternity\Exceptions\UserException;

/**
 * Class ValidationException
 * @package Eternity\Resource\Exceptions
 */
class ValidationException extends UserException
{
    /**
     * ValidationException constructor.
     * @param int $code
     * @param string|null $title
     * @param string|null $detail
     * @param \Throwable|null $previous
     */
    public function __construct(
        int $code,
        string $title = 'Metadata validation error',
        string $detail = null,
        \Throwable $previous = null
    ) {
        parent::__construct($title, $detail, $previous);
        $this->type = 'ValidationException';
        $this->code = $code;
    }
}