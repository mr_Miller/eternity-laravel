<?php

namespace Eternity\Laravel\Resource\Factories;

use Eternity\Definitions\HeadersDefinition;
use Eternity\Laravel\Exceptions\Codes;
use Eternity\Laravel\Resource\Exceptions\ValidationException;
use Eternity\Resource\Metadata\Metadata;
use Eternity\Resource\Objects\Filter\FilterCollection;
use Eternity\Resource\Objects\Filter\Type;
use Eternity\Resource\Objects\Order;
use Eternity\Resource\Objects\Page;
use Illuminate\Http\Request;

/**
 * Metadata factory
 *
 * Designed for Laravel
 *
 * Class MetadataFactory
 * todo Delete when will be not used anywhere
 * @deprecated Use new metadata factory
 * @link \Eternity\Laravel\Metadata\Factories\MetadataFactory
 * @package Eternity\Laravel\Resource\Factories
 */
class MetadataFactory
{
    private const ASCENDING_DIRECTION = 'ASC';
    private const DESCENDING_DIRECTION = 'DESC';
    private const MAXIMUM_PAGE_SIZE = 30;
    private const DEFAULT_PAGE_SIZE = 10;
    private const ALLOWED_DIRECTIONS = [
        self::ASCENDING_DIRECTION,
        self::DESCENDING_DIRECTION,
    ];
    private const ALLOWED_FILTER_TYPES = [
        Type::EQUALS,
        Type::GREATER,
        Type::LESS,
        Type::IN,
    ];
    private const DEFAULT_CURRENT_PAGE = 1;

    /**
     * @var \Illuminate\Http\Request|null
     */
    private $request;

    /**
     * MetadataFactory constructor.
     * @param \Illuminate\Http\Request|null $request
     * @param bool $validate
     * @throws \Eternity\Laravel\Resource\Exceptions\ValidationException
     */
    public function __construct(Request $request = null, $validate = true)
    {
        if ($request === null) {
            $request = \Illuminate\Support\Facades\Request::instance();
        }
        $this->request = $request;
        if ($validate) {
            $this->validate();
        }
    }

    /**
     * Request metadata parameters validation
     * @throws \Eternity\Laravel\Resource\Exceptions\ValidationException
     */
    public function validate(): void
    {
        // Order
        $this->checkOrderDirection();
        // Page
        $this->checkCurrentPage();
        $this->checkPageSize();
        // Filter
        $this->checkFilters();
    }

    /**
     * Checks order direction
     * @return void
     * @throws \Eternity\Laravel\Resource\Exceptions\ValidationException
     */
    private function checkOrderDirection(): void
    {
        if ($this->request->header(HeadersDefinition::ORDER_DIRECTION)
            && !in_array($this->request->header(HeadersDefinition::ORDER_DIRECTION), self::ALLOWED_DIRECTIONS)
        ) {
            throw new ValidationException(
                Codes::METADATA_ORDER_INVALID,
                'Metadata validation error',
                'Invalid order direction'
            );
        }
    }

    /**
     * Checks current page
     * @throws \Eternity\Laravel\Resource\Exceptions\ValidationException
     */
    private function checkCurrentPage(): void
    {
        if ($this->request->header(HeadersDefinition::PAGE_CURRENT, null) === null) {
            throw new ValidationException(
                Codes::METADATA_CURRENT_PAGE_MISSING_ERROR,
                'Metadata validation error',
                'Current page is missing'
            );
        }
    }

    /**
     * Checks page size
     * @throws \Eternity\Laravel\Resource\Exceptions\ValidationException
     */
    private function checkPageSize(): void
    {
        $pageSize = $this->request->header(HeadersDefinition::PAGE_SIZE, null);
        if ($pageSize === null) {
            throw new ValidationException(
                Codes::METADATA_PAGE_MISSING_ERROR,
                'Metadata validation error',
                'Page size is missing'
            );
        }
        if ($pageSize > $this->getMaximumPageSize()) {
            throw new ValidationException(
                Codes::METADATA_PAGE_SIZE_ERROR,
                'Metadata validation error',
                'Page size exceeded limit of ' . $this->getMaximumPageSize()
            );
        }
    }

    /**
     * Check filter
     */
    private function checkFilters(): void
    {
        $encodedFilters = $this->request->header(HeadersDefinition::FILTER, null);
        if ($encodedFilters === null) {
            return;
        }
        $filters = json_decode($encodedFilters, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new ValidationException(0, 'Filter error', 'Incorrect json value');
        }
        if (!is_array($filters)) {
            throw new ValidationException(0, 'Filter error', 'Filter must be an array');
        }
        foreach ($filters as $filter) {
            if (isset($filter['type']) && !in_array($filter['type'], self::ALLOWED_FILTER_TYPES)) {
                throw new ValidationException(
                    Codes::METADATA_FILTER_TYPE_INCORRECT,
                    'Filter error',
                    'Incorrect filter type. Use these: ' . implode(',', self::ALLOWED_FILTER_TYPES)
                );
            }
        }
    }

    /**
     * Create metadata
     * @return \Eternity\Resource\Metadata\Metadata
     * @throws \Eternity\Resource\Exceptions\MetadataException
     */
    public function create(): Metadata
    {
        $order = new Order(
            $this->request->header(HeadersDefinition::ORDER_DIRECTION, $this->getDefaultOrderDirection()),
            $this->request->header(HeadersDefinition::ORDER_BY, $this->getDefaultOrderBy())
        );
        $filter = new FilterCollection(
            json_decode($this->request->header(HeadersDefinition::FILTER), true) ?: []
        );
        $page = new Page(
            $this->request->header(HeadersDefinition::PAGE_SIZE, $this->getDefaultPageSize()),
            $this->request->header(HeadersDefinition::PAGE_CURRENT, $this->getDefaultCurrentPage())
        );

        return new Metadata($filter, $order, $page);
    }

    /**
     * Return maximum page size
     *
     * Override function to change value
     * @return int
     */
    public function getMaximumPageSize(): int
    {
        return self::MAXIMUM_PAGE_SIZE;
    }

    /**
     * Return default current page number
     * @return int
     */
    public function getDefaultCurrentPage(): int
    {
        return self::DEFAULT_CURRENT_PAGE;
    }

    /**
     * Returns default page size
     *
     * Override function to change value
     *
     * @return int
     */
    public function getDefaultPageSize(): int
    {
        return self::DEFAULT_PAGE_SIZE;
    }

    /**
     * Returns default By
     * @return string
     */
    public function getDefaultOrderBy(): string
    {
        return 'id';
    }

    /**
     * Returns default Direction
     * @return string
     */
    public function getDefaultOrderDirection(): string
    {
        return self::DESCENDING_DIRECTION;
    }

    /**
     * Create metadata statically
     * @param \Illuminate\Http\Request|null $request
     * @param bool $validate
     * @return \Eternity\Resource\Metadata\Metadata
     * @throws \Eternity\Laravel\Resource\Exceptions\ValidationException
     * @throws \Eternity\Resource\Exceptions\MetadataException
     */
    public static function createStatic(Request $request = null, $validate = true): Metadata
    {
        $factory = new static($request, $validate);

        return $factory->create();
    }
}