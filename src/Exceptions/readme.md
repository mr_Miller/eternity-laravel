## Handler
Class reference
```
Eternity\Laravel\Exceptions\Handler
```
Automatic error handling. Allows to handle Eternity application exceptions and builds Error HTTP response according to [documentation](https://animal-id.atlassian.net/wiki/spaces/A/pages/3538945/API+Reference+v1.0#Errors)
in **Laravel** based project.

#### Usage
In your laravel project find Handler.php file. By default this file has path `app/Exceptions/Handler.php`. Inherit it from `Eternity\Laravel\Exceptions\Handler`
 in stead of inheriting it from `Illuminate\Foundation\Exceptions\Handler`.
