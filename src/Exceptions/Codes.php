<?php

namespace Eternity\Laravel\Exceptions;

use Eternity\Exceptions\ErrorCodes;

/**
 * Class Codes
 * @package Eternity\Laravel\Exceptions
 */
class Codes extends ErrorCodes
{
    // Metadata codes
    public const METADATA_PAGE_MISSING_ERROR = 50017;
    public const METADATA_PAGE_SIZE_ERROR = 50018;
    public const METADATA_CURRENT_PAGE_MISSING_ERROR = 50019;
    public const METADATA_ORDER_INVALID = 50020;
    public const METADATA_FILTER_TYPE_INCORRECT = 50021;

}