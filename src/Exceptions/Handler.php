<?php

namespace Eternity\Laravel\Exceptions;

use Eternity\Exceptions\EternityException;
use Eternity\Http\Exceptions\ResponseException;
use Eternity\Laravel\Factories\ErrorResponseFactory;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;

/**
 * Exception handler
 *
 * Replaces application handler.
 * Allows to output errors in correct format.
 *
 * Class Handler
 * @package App\Application\Exceptions\Eternity\Exceptions
 */
class Handler extends ExceptionHandler
{
    /**
     * Render an exception into a response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Throwable $e
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response
     */
    public function render($request, \Throwable $e)
    {
        $factory = new ErrorResponseFactory();
        switch ($e) {
            case ($this->isValidation($e)):
                /** @var ValidationException $e */
                return $factory->createFromValidation($e);
                break;
            case ($this->isResponseError($e)):
                /** @var ResponseException $e */
                return $factory->createFromResponse($e);
                break;
            case ($this->isEternity($e)):
                /** @var \Eternity\Exceptions\EternityException $e */
                return $factory->createFromEternity($e);
                break;
            default:
                return $factory->createFromUnknown($e);
        }
    }

    /**
     * Check if exception is Laravel Validation exception
     * @param \Throwable $e
     * @return bool
     */
    protected function isValidation(\Throwable $e): bool
    {
        return $e instanceof ValidationException;
    }

    /**
     * Check if exception extends Eternity Exception
     * @param \Throwable $e
     * @return bool
     */
    protected function isEternity(\Throwable $e): bool
    {
        return $e instanceof EternityException;
    }

    /**
     * Check if exception extends Response exception of http client
     * @param \Throwable $e
     * @return bool
     */
    protected function isResponseError(\Throwable $e): bool
    {
        return $e instanceof ResponseException;
    }
}
