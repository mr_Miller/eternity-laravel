<?php

namespace Eternity\Laravel;

use Eternity\Definitions\ApplicationMode;

/**
 * Class Application
 * @package Eternity\Laravel
 */
class Application extends \Illuminate\Foundation\Application
{
    /**
     * Available modes
     */
    private const MODES = [
        ApplicationMode::MODE_WORKER,
        ApplicationMode::MODE_CRON,
        ApplicationMode::MODE_COMMAND,
        ApplicationMode::MODE_API,
    ];

    /**
     * @var string
     */
    private $mode;

    /**
     * Returns application mode
     * @return string|null
     */
    public function mode(): ?string
    {
        // todo To think if it's necessary to throw exception here on null value
        return $this->mode;
    }

    /**
     * Initializes mode
     *
     * Can be initialized only once. Exception will be thrown on trying to initialize more than once.
     * @param string $mode
     */
    public function initMode(?string $mode = null): void
    {
        if ($this->mode !== null) {
            throw new \BadMethodCallException('Application mode is already set. Cannot redeclare.');
        }
        if ($mode === null) {
            $mode = $this->defaultMode();
        }
        $this->validateMode($mode);
        $this->mode = $mode;
    }

    /**
     * Validate mode
     * @param string $mode
     */
    private function validateMode(string $mode): void
    {
        if (!in_array($mode, self::MODES)) {
            throw new \InvalidArgumentException("Invalid mode value '$mode' ." .
                " Value must be from list: " . implode(', ', self::MODES) . '.');
        }
    }

    /**
     * Returns default application mode
     * @return string
     */
    private function defaultMode(): string
    {
        return ApplicationMode::MODE_COMMAND;
    }
}