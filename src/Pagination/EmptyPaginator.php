<?php

namespace Eternity\Laravel\Pagination;

use Eternity\Laravel\Contracts\PaginatorInterface;
use Eternity\Resource\Metadata\Metadata;
use Illuminate\Support\Collection;

/**
 * Class EmptyPaginator
 * @package Eternity\Laravel\Pagination
 */
class EmptyPaginator implements PaginatorInterface
{
    /**
     * @var \Illuminate\Support\Collection
     */
    private $collection;

    /**
     * @var int
     */
    private $totalItems;

    /**
     * @var int
     */
    private $totalPages;

    /**
     * @var int
     */
    private $pageSize;

    /**
     * @var int
     */
    private $currentPage;

    /**
     * EntityPaginator constructor.
     * @param \Eternity\Resource\Metadata\Metadata $metadata
     */
    public function __construct(Metadata $metadata)
    {
        $this->collection = new Collection();
        $this->totalItems = 0;
        $this->totalPages = 0;
        $this->pageSize = $metadata->getPage()->getSize();
        $this->currentPage = $metadata->getPage()->getCurrent();
    }

    /**
     * Returns number of items on page
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * Returns current page number
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    /**
     * Returns overall count of items in storage
     * @return int
     */
    public function getTotalItems(): int
    {
        return $this->totalItems;
    }

    /**
     * Returns overall count of pages
     * @return int
     */
    public function getTotalPages(): int
    {
        return $this->totalPages;
    }

    /**
     * Returns collection of items on current page
     * @return \Illuminate\Support\Collection
     */
    public function getItems(): Collection
    {
        return $this->collection;
    }
}