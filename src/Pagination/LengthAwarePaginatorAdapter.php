<?php

namespace Eternity\Laravel\Pagination;

use Eternity\Laravel\Contracts\PaginatorInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Adapter for laravel paginator
 *
 * Class EntityPaginator
 * @package Eternity\Laravel\Pagination
 */
class LengthAwarePaginatorAdapter implements PaginatorInterface
{
    /**
     * @var \Illuminate\Support\Collection
     */
    private $collection;

    /**
     * @var int
     */
    private $totalItems;

    /**
     * @var int
     */
    private $totalPages;

    /**
     * @var int
     */
    private $pageSize;

    /**
     * @var int
     */
    private $currentPage;

    /**
     * EntityPaginator constructor.
     * @param \Illuminate\Support\Collection $collection
     * @param \Illuminate\Contracts\Pagination\LengthAwarePaginator $lengthAwarePaginator
     */
    public function __construct(Collection $collection, LengthAwarePaginator $lengthAwarePaginator)
    {
        $this->collection = $collection;
        $this->totalItems = $lengthAwarePaginator->total();
        $this->totalPages = $lengthAwarePaginator->lastPage();
        $this->pageSize = $lengthAwarePaginator->perPage();
        $this->currentPage = $lengthAwarePaginator->currentPage();
    }

    /**
     * Returns number of items on page
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * Returns current page number
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    /**
     * Returns overall count of items in storage
     * @return int
     */
    public function getTotalItems(): int
    {
        return $this->totalItems;
    }

    /**
     * Returns overall count of pages
     * @return int
     */
    public function getTotalPages(): int
    {
        return $this->totalPages;
    }

    /**
     * Returns collection of items on current page
     * @return \Illuminate\Support\Collection
     */
    public function getItems(): Collection
    {
        return $this->collection;
    }

}