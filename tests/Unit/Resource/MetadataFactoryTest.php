<?php

namespace Eternity\Laravel\Tests\Unit\Resource;

use Eternity\Definitions\HeadersDefinition;
use Eternity\Laravel\Exceptions\Codes;
use Eternity\Laravel\Metadata\Exceptions\ValidationException;
use Eternity\Laravel\Metadata\Factories\MetadataFactory;
use Eternity\Resource\Exceptions\MetadataException;
use Eternity\Resource\Objects\Filter\Filter;
use Eternity\Resource\Objects\Filter\FilterCollection;
use Eternity\Resource\Objects\Filter\Type;

/**
 * @group MetadataFactory
 * Class MetadataFactoryTest
 * @package Eternity\Laravel\Tests\Unit\Resource
 */
class MetadataFactoryTest extends AbstractFactory
{
    /**
     * @param array $replace
     * @return array
     */
    protected function getFilterData(array $replace = []): array
    {
        $filters = [
            [
                'type'  => Type::EQUALS,
                'key'   => 'some_field1',
                'value' => 'some value',
            ],
            [
                'type'  => Type::IN,
                'key'   => 'some_field2',
                'value' => [1, 2, 3, 4],
            ],
            [
                'type'  => Type::GREATER,
                'key'   => 'some_field3',
                'value' => 'some value',
            ],
            [
                'type'  => Type::LESS,
                'key'   => 'some_field4',
                'value' => 'some value',
            ],
            [
                'type' => Type::LIKE,
                'key' => 'some_field5',
                'value' => 'some',
            ]
        ];

        return array_merge($filters, $replace);
    }

    public function testFilterCollectionGetSuccess(): void
    {
        $filterCollection = new FilterCollection($this->getFilterData());
        $this->assertInstanceOf(Filter::class, $filterCollection->get('some_field1'));
    }

    public function testFilterCollectionGetOnIncorrectKeyFail(): void
    {
        $filterCollection = new FilterCollection($this->getFilterData());
        $this->expectException(MetadataException::class);
        $this->assertInstanceOf(Filter::class, $filterCollection->get('some_field6'));
    }

    public function filterDataProvider(): array
    {
        return [
            [Type::EQUALS, 'some_field1', 'some value'],
            [Type::LESS, 'some_field2', 1234],
            [Type::GREATER, 'some_field3', 234],
            [Type::IN, 'some_field4', [1, 2, 3, 4]],
            [Type::LIKE, 'some_field5', 'some'],
        ];
    }

    /**
     * @dataProvider filterDataProvider
     */
    public function testFilterGettersSuccess($type, $key, $value): void
    {
        $filter = new Filter($type, $key, $value);
        $this->assertEquals($type, $filter->type());
        $this->assertEquals($key, $filter->key());
        $this->assertEquals($value, $filter->value());
    }

    public function testCreateMetadataWithFilterSuccess(): void
    {
        $request = $this->getRequest();
        $request->headers->set(HeadersDefinition::FILTER, json_encode($this->getFilterData()));
        $metadata = MetadataFactory::createStatic($request);
        $this->assertInstanceOf(FilterCollection::class, $metadata->filter());
        $this->assertEquals($this->getFilterData()[0], $metadata->filter()->get('some_field1')->toArray());
    }

    public function testCreateMetadataWithNotArrayFilterFail(): void
    {
        $request = $this->getRequest();
        $request->headers->set(HeadersDefinition::FILTER, json_encode('privet ya ne korektnyj filter'));
        try {
            MetadataFactory::createStatic($request);
            $this->fail('Validation exception has not been thrown');
        } catch (ValidationException $e) {
            $this->assertEquals('Filter error', $e->getTitle(),);
            $this->assertEquals('Filter must be an array', $e->getDetail());
        }
    }

    public function testCreateMetadataFilterBadTypeFail(): void
    {
        $request = $this->getRequest();
        $filterData = [
            [
                'type'  => 'wrong type',
                'key'   => 'some key',
                'value' => 'some value',
            ],
        ];
        $request->headers->set(HeadersDefinition::FILTER, json_encode($filterData));
        try {
            MetadataFactory::createStatic($request);
            $this->fail('Validation exception has not been thrown');
        } catch (ValidationException $e) {
            $this->assertEquals('Filter error', $e->getTitle(),);
            $this->assertEquals('Incorrect filter type. Use these: =,>,<,IN,LIKE', $e->getDetail());
            $this->assertEquals(Codes::METADATA_FILTER_TYPE_INCORRECT, $e->getCode());
        }
    }

}