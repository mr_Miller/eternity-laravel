<?php

namespace Eternity\Laravel\Tests\Unit\Resource;

use Eternity\Laravel\Metadata\Factories\FilterMetadataFactory;
use Eternity\Metadata\Contracts\MetadataInterface;
use Eternity\Metadata\FilterMetadata;

/**
 * @group FilterMetadataFactoryTest
 * Class FilterMetadataFactoryTest
 * @package Eternity\Laravel\Tests\Unit\Resource
 */
class FilterMetadataFactoryTest extends AbstractFactory
{
    public function testCreateSuccess(): void
    {
        $metadata = FilterMetadataFactory::createStatic($this->getRequest());
        $this->assertInstanceOf(FilterMetadata::class, $metadata);
        $this->assertInstanceOf(MetadataInterface::class, $metadata);
    }
}