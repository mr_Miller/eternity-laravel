<?php

namespace Eternity\Laravel\Tests\Unit\Resource;

use Eternity\Laravel\Metadata\Factories\MetadataFactory;
use Eternity\Laravel\Metadata\Factories\PaginationMetadataFactory;
use Eternity\Metadata\PaginationMetadata;
use Eternity\Resource\Factories\PaginationFactory;

/**
 * Class PaginationMetadataFactoryTest
 * @package Eternity\Laravel\Tests\Unit\Resource
 */
class PaginationMetadataFactoryTest extends AbstractFactory
{
    public function testCreateSuccess(): void
    {
        $metadata = MetadataFactory::createStatic($this->getRequest());
        $paginationMetadata = PaginationMetadataFactory::createStatic(
            $metadata,
            PaginationFactory::createStatic(
                $metadata->page(),
                1,
                1
            )
        );
        $this->assertInstanceOf(PaginationMetadata::class, $paginationMetadata);
    }
}