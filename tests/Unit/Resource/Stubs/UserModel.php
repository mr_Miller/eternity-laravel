<?php

namespace Eternity\Laravel\Tests\Unit\Resource\Stubs;

class UserModel
{
    public $id = 2;

    public $name = 'test2';

    public $email = 'email2';
}