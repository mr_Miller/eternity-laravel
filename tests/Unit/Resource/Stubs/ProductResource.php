<?php

namespace Eternity\Laravel\Tests\Unit\Resource\Stubs;

use Eternity\Laravel\Resource\Resource;
use Eternity\Resource\Objects\Link;

class ProductResource extends Resource
{
    public $depth = 1;

    protected $name = 'product';

    public function links(): array
    {
        return [
            new Link('canonical', 'GET', '/v1/product'),
            new Link('create', 'POST', '/v1/product/1'),
            new Link('update', 'PUT', '/v1/product/1'),
            new Link('delete', 'DELETE', '/v1/product/1'),
        ];
    }

    public function toArray($request)
    {
        $output = [
            'id'   => $this->resource->id,
            'name' => $this->resource->title,
        ];

        if ($this->isExpanded('category')) {
            $output['category'] = (new CategoryResource((new RepositoryMock())->getCategory()))->resolve($request);
        }

        if ($this->isExpanded('user')) {
            $output['user'] = (new UserResource((new RepositoryMock())->getUser()))->resolve($request);
        }

        return $output;
    }
}