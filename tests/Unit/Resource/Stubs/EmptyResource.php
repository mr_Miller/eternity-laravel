<?php

namespace Eternity\Laravel\Tests\Unit\Resource\Stubs;

use Eternity\Laravel\Resource\Resource;

class EmptyResource extends Resource
{
    public function links(): array
    {
        return [];
    }
}