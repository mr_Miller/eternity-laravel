<?php

namespace Eternity\Laravel\Tests\Unit\Resource\Stubs;

/**
 * Made by Vitalik
 * Class ParentModelOneOne
 * @package Eternity\Laravel\Tests\Unit\Resource\Stubs
 */
class RoleModel
{
    public $id = 11;

    public $name = 'test11';

    public $email = 'email11';

}