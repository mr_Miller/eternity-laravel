<?php

namespace Eternity\Laravel\Tests\Unit\Resource\Stubs;

class RepositoryMock
{
    public function getCategory()
    {
        return new CategoryModel();
    }

    public function getUser()
    {
        return new UserModel();
    }

    public function getRole()
    {
        return new RoleModel();
    }
}