<?php

namespace Eternity\Laravel\Tests\Unit\Resource\Stubs;

use Eternity\Laravel\Resource\Resource;
use Eternity\Resource\Objects\Link;

class UserResource extends Resource
{
    protected $name = 'parent-two';

    public function links(): array
    {
        return [
            new Link('canonical', 'GET', '/v1/category'),
            new Link('create', 'POST', '/v1/category/1'),
            new Link('update', 'PUT', '/v1/category/1'),
            new Link('delete', 'DELETE', '/v1/category/1'),
        ];
    }

    public function toArray($request)
    {
        return [
            'id'    => $this->resource->id,
            'name'  => $this->resource->name,
            'email' => $this->resource->email,
        ];
    }
}