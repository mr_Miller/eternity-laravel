<?php

namespace Eternity\Laravel\Tests\Unit\Resource\Stubs;

/**
 * Class ChildModel
 * @package Eternity\Laravel\Tests\Unit\Resource\Stubs
 */
class TestModel
{
    public $id = 1;

    public $title = 'title';
}