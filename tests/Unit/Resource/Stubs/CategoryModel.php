<?php

namespace Eternity\Laravel\Tests\Unit\Resource\Stubs;

class CategoryModel
{
    public $id = 1;

    public $name = 'test1';

    public $email = 'email1';
}