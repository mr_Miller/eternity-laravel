<?php

namespace Eternity\Laravel\Tests\Unit\Resource;

use Eternity\Definitions\HeadersDefinition;
use Eternity\Laravel\Metadata\Exceptions\ValidationException;
use Eternity\Laravel\Metadata\Factories\MetadataFactory;
use Eternity\Laravel\Metadata\Factories\PaginationMetadataFactory;
use Eternity\Laravel\Tests\Unit\Resource\Stubs\EmptyResource;
use Eternity\Laravel\Tests\Unit\Resource\Stubs\ProductResource;
use Eternity\Laravel\Tests\Unit\Resource\Stubs\TestModel;
use Eternity\Resource\Factories\PaginationFactory;
use Eternity\Resource\Objects\Link;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;

/**
 * @group Resource
 * Class ResourceTest
 * @package Eternity\Laravel\Tests\Unit\Resource
 */
class ResourceTest extends TestCase
{
    /**
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     *
     */
    protected function setUp(): void
    {
        parent::setUp();
        $request = new Request();
        $request->setMethod('post');
        $request->headers->set('content-type', 'application/json');
        $request->headers->set(HeadersDefinition::ORDER_DIRECTION, 'ASC');
        $request->headers->set(HeadersDefinition::ORDER_BY, 'Some_field');
        $request->headers->set(HeadersDefinition::PAGE_CURRENT, 1);
        $request->headers->set(HeadersDefinition::PAGE_SIZE, 15);
        $request->headers->set(HeadersDefinition::EXPAND, json_encode(['category', 'role', 'user']));

        $this->request = $request;
    }

    /**
     * @return \Illuminate\Http\Request
     */
    protected function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @return \Eternity\Laravel\Tests\Unit\Resource\Stubs\ProductResource
     */
    protected function buildResource()
    {
        $collect = new Collection();
        $collect->add(new TestModel());
        $collect->add(new TestModel());

        return new ProductResource($collect);
    }

    /**
     * @return \Eternity\Laravel\Tests\Unit\Resource\Stubs\EmptyResource
     */
    protected function buildEmptyResource()
    {
        return new EmptyResource(new Collection(), null);
    }

    public function testResponse()
    {
        $resource = $this->buildResource();
        $response = $resource->toResponse($this->getRequest());
        self::assertArrayHasKey('payload', $response->getOriginalContent());
        self::assertArrayHasKey('message', $response->getOriginalContent());
        self::assertArrayHasKey('metadata', $response->getOriginalContent());
    }

    public function testResponseWithLink()
    {
        $resource = $this->buildResource();
        $resource->addRouteLink(new Link('canonical', 'GET', '/v1/product'));
        $data = $resource->toResponse($this->getRequest());
        self::assertArrayHasKey('links', json_decode($data->content(), true));
    }

    public function testResponseWithLinks()
    {
        $resource = $this->buildResource();
        $resource->addRouteLinks([
            new Link('canonical', 'GET', '/v1/product'),
            new Link('update', 'POST', '/v1/product/1'),
        ]);
        $response = $resource->toResponse($this->getRequest());
        $data = json_decode($response->content(), true);
        self::assertArrayHasKey('links', $data);
        self::assertCount(2, $data['links']);
    }

    public function testEmptyResponse()
    {
        $resource = $this->buildEmptyResource();
        $response = $resource->toResponse(new Request());
        $data = json_decode($response->content(), true);
        self::assertEmpty($data['payload']);
        self::assertEmpty($data['message']);
        self::assertEmpty($data['metadata']);
    }

    /**
     * @throws \Eternity\Resource\Exceptions\MetadataException
     */
    public function testThrowsValidationExceptionOnEmptyPageSizeFail()
    {
        $request = $this->getRequest();
        $request->headers->set(HeadersDefinition::PAGE_SIZE, null);
        $this->expectException(ValidationException::class);
        MetadataFactory::createStatic($request);
    }

    /**
     * @throws \Eternity\Laravel\Metadata\Exceptions\ValidationException
     * @throws \Eternity\Resource\Exceptions\MetadataException
     */
    public function testThrowsValidationExceptionOnEmptyCurrentPageFail()
    {
        $request = $this->getRequest();
        $request->headers->set(HeadersDefinition::PAGE_CURRENT, null);
        $this->expectException(ValidationException::class);
        MetadataFactory::createStatic($request);
    }

    /**
     * @throws \Eternity\Laravel\Metadata\Exceptions\ValidationException
     * @throws \Eternity\Resource\Exceptions\MetadataException
     */
    public function testThrowsValidationExceptionOnInvalidOrderDirectionFail()
    {
        $request = $this->getRequest();
        $request->headers->set(HeadersDefinition::ORDER_DIRECTION, 'invalid');
        $this->expectException(ValidationException::class);
        MetadataFactory::createStatic($request);
    }

    /**
     * @throws \Eternity\Laravel\Metadata\Exceptions\ValidationException
     * @throws \Eternity\Resource\Exceptions\MetadataException
     */
    public function testResponseWithCustomMetadataStructureSuccess()
    {
        $itemsTotal = 15;
        $pagesTotal = 1;
        $collect = new Collection();
        $collect->add(new TestModel());
        $metadata = MetadataFactory::createStatic($this->getRequest());
        $pagination = PaginationFactory::createStatic($metadata->page(), $itemsTotal, $pagesTotal);
        $resource = new ProductResource($collect, PaginationMetadataFactory::createStatic($metadata, $pagination));
        $response = $resource->toResponse($this->getRequest());
        $data = json_decode($response->content(), true);
        // Testing structure
        self::assertEquals($itemsTotal, $data['metadata']['pagination']['items_total']);
        self::assertEquals($pagesTotal, $data['metadata']['pagination']['pages_total']);
        self::assertEquals($this->getRequest()->header(HeadersDefinition::PAGE_SIZE), $data['metadata']['page']['size']);
        self::assertEquals($this->getRequest()->header(HeadersDefinition::PAGE_CURRENT), $data['metadata']['page']['current']);
        self::assertEquals($this->getRequest()->header(HeadersDefinition::ORDER_DIRECTION), $data['metadata']['order']['direction']);
        self::assertEquals($this->getRequest()->header(HeadersDefinition::ORDER_BY), $data['metadata']['order']['by']);
        self::assertEquals(1, $data['metadata']['pagination']['pages']['first']);
        self::assertEquals(1, $data['metadata']['pagination']['pages']['last']);
        self::assertNull($data['metadata']['pagination']['pages']['next']);
        self::assertNull($data['metadata']['pagination']['pages']['prev']);
    }

}