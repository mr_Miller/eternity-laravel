<?php

namespace Eternity\Laravel\Tests\Unit\Resource;

use Eternity\Definitions\HeadersDefinition;
use Eternity\Laravel\Tests\Feature\TestCase;
use Eternity\Resource\Objects\Filter\Type;
use Illuminate\Http\Request;

/**
 * Class AbstractFactory
 * @package Eternity\Laravel\Tests\Unit\Resource
 */
abstract class AbstractFactory extends TestCase
{
    /**
     * @return \Illuminate\Http\Request
     */
    protected function getRequest(): Request
    {
        $filters = [
            [
                'type'  => Type::EQUALS,
                'key'   => 'some',
                'value' => 'some',
            ],
        ];

        $request = new Request();
        $request->setMethod('post');
        $request->headers->set('content-type', 'application/json');
        $request->headers->set(HeadersDefinition::ORDER_DIRECTION, 'ASC');
        $request->headers->set(HeadersDefinition::ORDER_BY, 'Some_field');
        $request->headers->set(HeadersDefinition::PAGE_CURRENT, 1);
        $request->headers->set(HeadersDefinition::PAGE_SIZE, 15);
        $request->headers->set(HeadersDefinition::EXPAND, json_encode(['category', 'role', 'user']));
        $request->headers->set(HeadersDefinition::FILTER, json_encode($filters));

        return $request;
    }
}