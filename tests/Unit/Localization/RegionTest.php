<?php

namespace Eternity\Laravel\Tests\Unit\Localization;

use Eternity\Definitions\HeadersDefinition;
use Eternity\Definitions\Language\LanguageDefinition;
use Eternity\Laravel\Components\Localization\Definitions\RegionDefinition;
use Eternity\Laravel\Components\Localization\Definitions\RegionLanguageDefinition;
use Eternity\Laravel\Components\Localization\Exceptions\RegionException;
use Eternity\Laravel\Components\Localization\Factories\RegionFactory;
use Eternity\Laravel\Components\Localization\Localization;
use Eternity\Laravel\Components\Localization\Region;
use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;

/**
 * Class RegionTest
 * @package Eternity\Tests\Unit\Localization
 */
class RegionTest extends TestCase
{
    /**
     * @return \Illuminate\Http\Request
     */
    private function getRequest(): Request
    {
        $request = new Request();
        $request->headers->set(HeadersDefinition::LANG_CODE, LanguageDefinition::ENG);

        return $request;
    }

    public function testLocalization()
    {
        $testRegion = RegionDefinition::UKRAINE;
        $region = RegionFactory::create($testRegion);
        $request = $this->getRequest();
        $localization = new Localization($region, $request);
        $this->assertEquals($region, $localization->region());
        $this->assertEquals(RegionLanguageDefinition::getRegionDefault($testRegion), $localization->defaultLanguage());
        $this->assertEquals(LanguageDefinition::ENG, $localization->language());
    }

    public function testRegionExpectsExceptionOnUnknownRegion()
    {
        $testRegion = 'unknown';
        $this->expectException(RegionException::class);
        $region = new Region(
            $testRegion,
            RegionLanguageDefinition::getRegionDefault('ukraine'),
            RegionLanguageDefinition::getRegionLanguagesList('ukraine')
        );
    }

    public function testGetRegionLanguagesListThrowsExceptionOnUnknownRegion()
    {
        $this->expectException(RegionException::class);
        RegionLanguageDefinition::getRegionLanguagesList('unknown');
    }

    public function testGetRegionDefaultThrowsExceptionOnUnknownRegion()
    {
        $this->expectException(RegionException::class);
        RegionLanguageDefinition::getRegionDefault('unknown');
    }
}