<?php

namespace Eternity\Laravel\Tests\Unit\Components\Push;

use Eternity\Events\Microservices\Notification\Push;
use Eternity\Laravel\Components\Event\EventBus;
use Eternity\Laravel\Components\Push\PushDispatcher;
use Eternity\Laravel\Tests\Feature\TestCase;

/**
 * Class PushDispatcherTest
 * @group PushDispatcherTest
 * @package Eternity\Laravel\Tests\Unit\Components\Push
 */
class PushDispatcherTest extends TestCase
{
    public function testReleaseOnLimitReachedAutomaticallySuccess(): void
    {
        $mock = \Mockery::mock(EventBus::class);
        $mock->shouldReceive('publish')->once();

        $dispatcher = new PushDispatcher($mock);

        // Add 500 push notifications
        for ($i = 0; $i < 500; $i++) {
            $dispatcher->add(Push::create(1, 1));
        }
        $this->assertFalse($dispatcher->hasNotifications());
    }

    public function testReleaseAutomaticallyOnDestructSuccess(): void
    {
        $mock = \Mockery::mock(EventBus::class);
        $mock->shouldReceive('publish')->once();

        $dispatcher = new PushDispatcher($mock);
        $dispatcher->add(Push::create(1, 1));
        unset($dispatcher);
    }

    public function testReleaseCustomSuccess(): void
    {
        $mock = \Mockery::mock(EventBus::class);
        $mock->shouldReceive('publish')->once();

        $dispatcher = new PushDispatcher($mock);
        $dispatcher->add(Push::create(1, 1));

        $dispatcher->release();

        $this->assertFalse($dispatcher->hasNotifications());
    }
}