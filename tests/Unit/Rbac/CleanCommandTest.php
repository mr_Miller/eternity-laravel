<?php

namespace Eternity\Laravel\Tests\Unit\Rbac;

use Eternity\Laravel\Rbac\Console\CleanCommand;
use Eternity\Laravel\Rbac\Database\Ability;
use Prophecy\PhpUnit\ProphecyTrait;

class CleanCommandTest extends BaseTestCase
{
    use Concerns\TestsConsoleCommands, ProphecyTrait;

    /**
     * @test
     */
    public function the_orphaned_flag()
    {
        $rbac = $this->inspector($user = User::create())->dontCache();

        $rbac->allow($user)->to(['access-dashboard', 'ban-users', 'throw-dishes']);
        $rbac->disallow($user)->to(['access-dashboard', 'ban-users']);

        self::assertEquals(3, Ability::query()->count());

        $this->clean(['--unassigned' => true], '<info>Deleted 2 unassigned abilities.</info>');

        self::assertEquals(1, Ability::query()->count());
        self::assertTrue($rbac->can('throw-dishes'));
    }

    /**
     * @test
     */
    public function the_orphaned_flag_with_no_orphaned_abilities()
    {
        $rbac = $this->inspector($user = User::create())->dontCache();

        $rbac->allow($user)->to(['access-dashboard', 'ban-users', 'throw-dishes']);

        self::assertEquals(3, Ability::query()->count());

        $this->clean(['--unassigned' => true], '<info>No unassigned abilities.</info>');

        self::assertEquals(3, Ability::query()->count());
    }

    /**
     * @test
     */
    public function the_missing_flag()
    {
        $rbac = $this->inspector($user1 = User::create())->dontCache();

        $account1 = Account::create();
        $account2 = Account::create();
        $user2 = User::create();

        $rbac->allow($user1)->to('create', Account::class);
        $rbac->allow($user1)->to('create', User::class);
        $rbac->allow($user1)->to('update', $user1);
        $rbac->allow($user1)->to('update', $user2);
        $rbac->allow($user1)->to('update', $account1);
        $rbac->allow($user1)->to('update', $account2);

        $account1->delete();
        $user2->delete();

        self::assertEquals(6, Ability::query()->count());

        $this->clean(['--orphaned' => true], '<info>Deleted 2 orphaned abilities.</info>');

        self::assertEquals(4, Ability::query()->count());
        self::assertTrue($rbac->can('create', Account::class));
        self::assertTrue($rbac->can('create', User::class));
        self::assertTrue($rbac->can('update', $user1));
        self::assertTrue($rbac->can('update', $account2));
    }

    /**
     * @test
     */
    public function no_flags()
    {
        $rbac = $this->inspector($user1 = User::create())->dontCache();

        $account1 = Account::create();
        $account2 = Account::create();
        $user2 = User::create();

        $rbac->allow($user1)->to('update', $user1);
        $rbac->allow($user1)->to('update', $user2);
        $rbac->allow($user1)->to('update', $account1);
        $rbac->allow($user1)->to('update', $account2);

        $rbac->disallow($user1)->to('update', $user1);
        $account1->delete();

        self::assertEquals(4, Ability::query()->count());

        $this->clean([], [
            '<info>Deleted 1 unassigned ability.</info>',
            '<info>Deleted 1 orphaned ability.</info>'
        ]);

        self::assertEquals(2, Ability::query()->count());
    }

    /**
     * Run the clean command, and see the given message in the output.
     *
     * @param  array  $parameters
     * @param  string|array  $message
     * @return \Prophecy\Prophecy\ObjectProphecy
     */
    protected function clean(array $parameters = [], $message)
    {
        return $this->runCommand(
            new CleanCommand, $parameters, $this->predictOutputMessage($message)
        );
    }
}
