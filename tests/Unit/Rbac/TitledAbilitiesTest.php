<?php

namespace Eternity\Laravel\Tests\Unit\Rbac;

use Eternity\Laravel\Rbac\Database\Ability;

class TitledAbilitiesTest extends BaseTestCase
{
    /**
     * @test
     */
    public function allowing_simple_ability()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->allow($user)->to('access-dashboard', null, [
            'title' => 'Dashboard administration',
        ]);

        $this->seeTitledAbility('Dashboard administration');
    }

    /**
     * @test
     */
    public function allowing_model_class_ability()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->allow($user)->to('create', User::class, [
            'title' => 'Create users',
        ]);

        $this->seeTitledAbility('Create users');
    }

    /**
     * @test
     */
    public function allowing_model_ability()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->allow($user)->to('delete', $user, [
            'title' => 'Delete user #1',
        ]);

        $this->seeTitledAbility('Delete user #1');
    }

    /**
     * @test
     */
    public function allowing_everything()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->allow($user)->everything([
            'title' => 'Omnipotent',
        ]);

        $this->seeTitledAbility('Omnipotent');
    }

    /**
     * @test
     */
    public function allowing_to_manage_a_model_class()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->allow($user)->toManage(User::class, [
            'title' => 'Manage users',
        ]);

        $this->seeTitledAbility('Manage users');
    }

    /**
     * @test
     */
    public function allowing_to_manage_a_model()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->allow($user)->toManage($user, [
            'title' => 'Manage user #1',
        ]);

        $this->seeTitledAbility('Manage user #1');
    }

    /**
     * @test
     */
    public function allowing_an_ability_on_everything()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->allow($user)->to('create')->everything([
            'title' => 'Create anything',
        ]);

        $this->seeTitledAbility('Create anything');
    }

    /**
     * @test
     */
    public function allowing_to_own_a_model_class()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->allow($user)->toOwn(Account::class, [
            'title' => 'Manage onwed account',
        ]);

        $this->seeTitledAbility('Manage onwed account');
    }

    /**
     * @test
     */
    public function allowing_to_own_a_model()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->allow($user)->toOwn($user, [
            'title' => 'Manage user #1 when owned',
        ]);

        $this->seeTitledAbility('Manage user #1 when owned');
    }

    /**
     * @test
     */
    public function allowing_to_own_everything()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->allow($user)->toOwnEverything([
            'title' => 'Manage anything onwed',
        ]);

        $this->seeTitledAbility('Manage anything onwed');
    }

    /**
     * @test
     */
    public function forbidding_simple_ability()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->forbid($user)->to('access-dashboard', null, [
            'title' => 'Dashboard administration',
        ]);

        $this->seeTitledAbility('Dashboard administration');
    }

    /**
     * @test
     */
    public function forbidding_model_class_ability()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->forbid($user)->to('create', User::class, [
            'title' => 'Create users',
        ]);

        $this->seeTitledAbility('Create users');
    }

    /**
     * @test
     */
    public function forbidding_model_ability()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->forbid($user)->to('delete', $user, [
            'title' => 'Delete user #1',
        ]);

        $this->seeTitledAbility('Delete user #1');
    }

    /**
     * @test
     */
    public function forbidding_everything()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->forbid($user)->everything([
            'title' => 'Omnipotent',
        ]);

        $this->seeTitledAbility('Omnipotent');
    }

    /**
     * @test
     */
    public function forbidding_to_manage_a_model_class()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->forbid($user)->toManage(User::class, [
            'title' => 'Manage users',
        ]);

        $this->seeTitledAbility('Manage users');
    }

    /**
     * @test
     */
    public function forbidding_to_manage_a_model()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->forbid($user)->toManage($user, [
            'title' => 'Manage user #1',
        ]);

        $this->seeTitledAbility('Manage user #1');
    }

    /**
     * @test
     */
    public function forbidding_an_ability_on_everything()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->forbid($user)->to('create')->everything([
            'title' => 'Create anything',
        ]);

        $this->seeTitledAbility('Create anything');
    }

    /**
     * @test
     */
    public function forbidding_to_own_a_model_class()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->forbid($user)->toOwn(Account::class, [
            'title' => 'Manage onwed account',
        ]);

        $this->seeTitledAbility('Manage onwed account');
    }

    /**
     * @test
     */
    public function forbidding_to_own_a_model()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->forbid($user)->toOwn($user, [
            'title' => 'Manage user #1 when owned',
        ]);

        $this->seeTitledAbility('Manage user #1 when owned');
    }

    /**
     * @test
     */
    public function forbidding_to_own_everything()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->forbid($user)->toOwnEverything([
            'title' => 'Manage anything onwed',
        ]);

        $this->seeTitledAbility('Manage anything onwed');
    }

    /**
     * Assert that there's an ability with the given title in the DB.
     *
     * @param  string  $title
     * @return void
     */
    protected function seeTitledAbility($title)
    {
        self::assertTrue(Ability::where(compact('title'))->exists());
    }
}
