<?php

namespace Eternity\Laravel\Tests\Unit\Rbac;

use Eternity\Laravel\Rbac\Database\Ability;
use Eternity\Laravel\Rbac\Database\Role;
use Exception;

class RbacSimpleTest extends BaseTestCase
{
    use Concerns\TestsClipboards;

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_give_and_remove_abilities($provider)
    {
        [$rbac, $user] = $provider();

        $editSite = Ability::create(['name' => 'edit-site']);
        $banUsers = Ability::create(['name' => 'ban-users']);
        $accessDashboard = Ability::create(['name' => 'access-dashboard']);

        $rbac->allow($user)->to('edit-site');
        $rbac->allow($user)->to([$banUsers, $accessDashboard->id]);

        self::assertTrue($rbac->can('edit-site'));
        self::assertTrue($rbac->can('ban-users'));
        self::assertTrue($rbac->can('access-dashboard'));

        $rbac->disallow($user)->to($editSite);
        $rbac->disallow($user)->to('ban-users');
        $rbac->disallow($user)->to($accessDashboard->id);

        self::assertTrue($rbac->cannot('edit-site'));
        self::assertTrue($rbac->cannot('ban-users'));
        self::assertTrue($rbac->cannot('access-dashboard'));
    }


    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_give_and_remove_abilities_for_everyone($provider)
    {
        [$rbac, $user] = $provider();

        $editSite = Ability::create(['name' => 'edit-site']);
        $banUsers = Ability::create(['name' => 'ban-users']);
        $accessDashboard = Ability::create(['name' => 'access-dashboard']);

        $rbac->allowEveryone()->to('edit-site');
        $rbac->allowEveryone()->to([$banUsers, $accessDashboard->id]);

        self::assertTrue($rbac->can('edit-site'));
        self::assertTrue($rbac->can('ban-users'));
        self::assertTrue($rbac->can('access-dashboard'));

        $rbac->disallowEveryone()->to($editSite);
        $rbac->disallowEveryone()->to('ban-users');
        $rbac->disallowEveryone()->to($accessDashboard->id);

        self::assertTrue($rbac->cannot('edit-site'));
        self::assertTrue($rbac->cannot('ban-users'));
        self::assertTrue($rbac->cannot('access-dashboard'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_give_and_remove_wildcard_abilities($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->to('*');

        self::assertTrue($rbac->can('edit-site'));
        self::assertTrue($rbac->can('ban-users'));
        self::assertTrue($rbac->can('*'));

        $rbac->disallow($user)->to('*');

        self::assertTrue($rbac->cannot('edit-site'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_ignore_duplicate_ability_allowances($provider)
    {
        [$rbac, $user1, $user2] = $provider(2);

        $rbac->allow($user1)->to('ban-users');
        $rbac->allow($user1)->to('ban-users');

        $rbac->allow($user1)->to('ban', $user2);
        $rbac->allow($user1)->to('ban', $user2);

        self::assertCount(2, $user1->abilities);

        $admin = $rbac->role(['name' => 'admin']);
        $admin->save();

        $rbac->allow($admin)->to('ban-users');
        $rbac->allow($admin)->to('ban-users');

        $rbac->allow($admin)->to('ban', $user1);
        $rbac->allow($admin)->to('ban', $user1);

        self::assertCount(2, $admin->abilities);
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_give_and_remove_roles($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow('admin')->to('ban-users');
        $rbac->assign('admin')->to($user);

        $editor = $rbac->role()->create(['name' => 'editor']);
        $rbac->allow($editor)->to('ban-users');
        $rbac->assign($editor)->to($user);

        self::assertTrue($rbac->can('ban-users'));

        $rbac->retract('admin')->from($user);
        $rbac->retract($editor)->from($user);

        self::assertTrue($rbac->cannot('ban-users'));
    }

    /**
     * @test
     */
    public function deleting_a_role_deletes_the_pivot_table_records()
    {
        $rbac = $this->inspector();

        $admin = $rbac->role()->create(['name' => 'admin']);
        $editor = $rbac->role()->create(['name' => 'editor']);

        $rbac->allow($admin)->everything();
        $rbac->allow($editor)->to('edit', User::class);

        self::assertEquals(2, $this->db()->table('permissions')->count());

        $admin->delete();

        self::assertEquals(1, $this->db()->table('permissions')->count());
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_give_and_remove_multiple_roles_at_once($provider)
    {
        [$rbac, $user] = $provider();

        $admin    = $this->role('admin');
        $editor   = $this->role('editor');
        $reviewer = $this->role('reviewer');

        $rbac->assign(collect([$admin, 'editor', $reviewer->id]))->to($user);

        self::assertTrue($rbac->is($user)->all($admin->id, $editor, 'reviewer'));

        $rbac->retract(['admin', $editor])->from($user);

        self::assertTrue($rbac->is($user)->notAn($admin, 'editor'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_give_and_remove_roles_for_multiple_users_at_once($provider)
    {
        [$rbac, $user1, $user2] = $provider(2);

        $rbac->assign(['admin', 'editor'])->to([$user1, $user2]);

        self::assertTrue($rbac->is($user1)->all('admin', 'editor'));
        self::assertTrue($rbac->is($user2)->an('admin', 'editor'));

        $rbac->retract('admin')->from($user1);
        $rbac->retract(collect(['admin', 'editor']))->from($user2);

        self::assertTrue($rbac->is($user1)->notAn('admin'));
        self::assertTrue($rbac->is($user1)->an('editor'));
        self::assertTrue($rbac->is($user1)->an('admin', 'editor'));
    }

    /**
     * @test
     */
    public function can_ignore_duplicate_role_assignments()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->assign('admin')->to($user);
        $rbac->assign('admin')->to($user);

        self::assertCount(1, $user->roles);
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_disallow_abilities_on_roles($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow('admin')->to('edit-site');
        $rbac->disallow('admin')->to('edit-site');
        $rbac->assign('admin')->to($user);

        self::assertTrue($rbac->cannot('edit-site'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function disallow_on_roles_does_not_disallow_for_users_with_matching_id($provider)
    {
        [$rbac, $user] = $provider();

        // Since the user is the first user created, its ID is 1.
        // Creating admin as the first role, it'll have its ID
        // set to 1. Let's test that they're kept separate.
        $rbac->allow($user)->to('edit-site');
        $rbac->allow('admin')->to('edit-site');
        $rbac->disallow('admin')->to('edit-site');

        self::assertTrue($rbac->can('edit-site'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_check_user_roles($provider)
    {
        [$rbac, $user] = $provider();

        self::assertTrue($rbac->is($user)->notA('moderator'));
        self::assertTrue($rbac->is($user)->notAn('editor'));
        self::assertFalse($rbac->is($user)->an('admin'));

        $rbac = $this->inspector($user = User::create());

        $rbac->assign('moderator')->to($user);
        $rbac->assign('editor')->to($user);

        self::assertTrue($rbac->is($user)->a('moderator'));
        self::assertTrue($rbac->is($user)->an('editor'));
        self::assertFalse($rbac->is($user)->notAn('editor'));
        self::assertFalse($rbac->is($user)->an('admin'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_check_multiple_user_roles($provider)
    {
        [$rbac, $user] = $provider();

        self::assertTrue($rbac->is($user)->notAn('editor', 'moderator'));
        self::assertTrue($rbac->is($user)->notAn('admin', 'moderator'));

        $rbac = $this->inspector($user = User::create());
        $rbac->assign('moderator')->to($user);
        $rbac->assign('editor')->to($user);

        self::assertTrue($rbac->is($user)->a('subscriber', 'moderator'));
        self::assertTrue($rbac->is($user)->an('admin', 'editor'));
        self::assertTrue($rbac->is($user)->all('editor', 'moderator'));
        self::assertFalse($rbac->is($user)->notAn('editor', 'moderator'));
        self::assertFalse($rbac->is($user)->all('admin', 'moderator'));
    }

    /**
     * @test
     */
    public function can_get_an_empty_role_model()
    {
        $rbac = $this->inspector($user = User::create());

        self::assertInstanceOf(Role::class, $rbac->role());
    }

    /**
     * @test
     */
    public function can_fill_a_role_model()
    {
        $rbac = $this->inspector($user = User::create());
        $role = $rbac->role(['name' => 'test-role']);

        self::assertInstanceOf(Role::class, $role);
        self::assertEquals('test-role', $role->name);
    }

    /**
     * @test
     */
    public function can_get_an_empty_ability_model()
    {
        $rbac = $this->inspector($user = User::create());

        self::assertInstanceOf(Ability::class, $rbac->ability());
    }

    /**
     * @test
     */
    public function can_fill_an_ability_model()
    {
        $rbac = $this->inspector($user = User::create());
        $ability = $rbac->ability(['name' => 'test-ability']);

        self::assertInstanceOf(Ability::class, $ability);
        self::assertEquals('test-ability', $ability->name);
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_allow_abilities_from_a_defined_callback($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->define('edit', function ($user, $account) {
            if ( ! $account instanceof Account) {
                return null;
            }

            return $user->id == $account->user_id;
        });

        self::assertTrue($rbac->can('edit', new Account(['user_id' => $user->id])));
        self::assertFalse($rbac->can('edit', new Account(['user_id' => 99])));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function authorize_method_returns_response_with_correct_message($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->to('have-fun');
        $rbac->allow($user)->to('enjoy-life');

        self::assertEquals(
            'Rbac granted permission via ability #2',
            $rbac->authorize('enjoy-life')->message()
        );

        self::assertEquals(
            'Rbac granted permission via ability #1',
            $rbac->authorize('have-fun')->message()
        );
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function authorize_method_throws_for_unauthorized_abilities($provider)
    {
        [$rbac] = $provider();

        // The exception class thrown from the "authorize" method
        // has changed between different versions of Laravel,
        // so we cannot check for a specific error class.
        $threw = false;

        try {
            $rbac->authorize('be-miserable');
        } catch (Exception $e) {
            $threw = true;
        }

        self::assertTrue($threw);
    }

    /**
     * Create a new role with the given name.
     *
     * @param  string  $name
     * @return \Eternity\Laravel\Rbac\Database\Role
     */
    protected function role($name)
    {
        return Role::create(compact('name'));
    }
}
