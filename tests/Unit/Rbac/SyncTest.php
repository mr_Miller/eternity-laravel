<?php

namespace Eternity\Laravel\Tests\Unit\Rbac;

use Eternity\Laravel\Rbac\Database\Ability;
use Eternity\Laravel\Rbac\Database\Role;

class SyncTest extends BaseTestCase
{
    use Concerns\TestsClipboards;

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function syncing_roles($provider)
    {
        [$rbac, $user] = $provider();

        $admin      = $this->role('admin');
        $editor     = $this->role('editor');
        $reviewer   = $this->role('reviewer');
        $subscriber = $this->role('subscriber');

        $user->assign([$admin, $editor]);

        self::assertTrue($rbac->is($user)->all($admin, $editor));

        $rbac->sync($user)->roles([$editor->id, $reviewer->name, $subscriber]);

        self::assertTrue($rbac->is($user)->all($editor, $reviewer, $subscriber));
        self::assertTrue($rbac->is($user)->notAn($admin));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function syncing_abilities($provider)
    {
        [$rbac, $user] = $provider();

        $editSite = Ability::create(['name' => 'edit-site']);
        $banUsers = Ability::create(['name' => 'ban-users']);
        Ability::create(['name' => 'access-dashboard']);

        $rbac->allow($user)->to([$editSite, $banUsers]);

        self::assertTrue($rbac->can('edit-site'));
        self::assertTrue($rbac->can('ban-users'));
        self::assertTrue($rbac->cannot('access-dashboard'));

        $rbac->sync($user)->abilities([$banUsers->id, 'access-dashboard']);

        self::assertTrue($rbac->cannot('edit-site'));
        self::assertTrue($rbac->can('ban-users'));
        self::assertTrue($rbac->can('access-dashboard'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function syncing_abilities_with_a_map($provider)
    {
        [$rbac, $user] = $provider();

        $deleteUser = Ability::createForModel($user, 'delete');
        $createAccounts = Ability::createForModel(Account::class, 'create');

        $rbac->allow($user)->to([$deleteUser, $createAccounts]);

        self::assertTrue($rbac->can('delete', $user));
        self::assertTrue($rbac->can('create', Account::class));

        $rbac->sync($user)->abilities([
            'access-dashboard',
            'create' => Account::class,
            'view' => $user,
        ]);

        self::assertTrue($rbac->cannot('delete', $user));
        self::assertTrue($rbac->cannot('view', User::class));
        self::assertTrue($rbac->can('create', Account::class));
        self::assertTrue($rbac->can('view', $user));
        self::assertTrue($rbac->can('access-dashboard'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function syncing_forbidden_abilities($provider)
    {
        [$rbac, $user] = $provider();

        $editSite = Ability::create(['name' => 'edit-site']);
        $banUsers = Ability::create(['name' => 'ban-users']);
        Ability::create(['name' => 'access-dashboard']);

        $rbac->allow($user)->everything();
        $rbac->forbid($user)->to([$editSite, $banUsers->id]);

        self::assertTrue($rbac->cannot('edit-site'));
        self::assertTrue($rbac->cannot('ban-users'));
        self::assertTrue($rbac->can('access-dashboard'));

        $rbac->sync($user)->forbiddenAbilities([$banUsers->id, 'access-dashboard']);

        self::assertTrue($rbac->can('edit-site'));
        self::assertTrue($rbac->cannot('ban-users'));
        self::assertTrue($rbac->cannot('access-dashboard'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function syncing_a_roles_abilities($provider)
    {
        [$rbac, $user] = $provider();

        $editSite = Ability::create(['name' => 'edit-site']);
        $banUsers = Ability::create(['name' => 'ban-users']);
        $accessDashboard = Ability::create(['name' => 'access-dashboard']);

        $rbac->assign('admin')->to($user);
        $rbac->allow('admin')->to([$editSite, $banUsers]);

        self::assertTrue($rbac->can('edit-site'));
        self::assertTrue($rbac->can('ban-users'));
        self::assertTrue($rbac->cannot('access-dashboard'));

        $rbac->sync('admin')->abilities([$banUsers->id, 'access-dashboard']);

        self::assertTrue($rbac->cannot('edit-site'));
        self::assertTrue($rbac->can('ban-users'));
        self::assertTrue($rbac->can('access-dashboard'));
    }

    /**
     * @test
     */
    public function syncing_user_abilities_does_not_alter_role_abilities_with_same_id()
    {
        $user = User::create(['id' => 1]);
        $rbac = $this->inspector($user);
        $role = $rbac->role()->create(['id' => 1, 'name' => 'alcoholic']);

        $rbac->allow($user)->to(['eat', 'drink']);
        $rbac->allow($role)->to('drink');

        $rbac->sync($user)->abilities(['eat']);

        self::assertTrue($user->can('eat'));
        self::assertTrue($user->cannot('drink'));
        self::assertTrue($role->can('drink'));
    }

    /**
     * @test
     */
    public function syncing_abilities_does_not_affect_another_entity_type_with_same_id()
    {
        $user = User::create(['id' => 1]);
        $account = Account::create(['id' => 1]);

        $bouncer = $this->inspector();

        $bouncer->allow($user)->to('relax');
        $bouncer->allow($account)->to('relax');

        self::assertTrue($user->can('relax'));
        self::assertTrue($account->can('relax'));

        $bouncer->sync($user)->abilities([]);

        self::assertTrue($user->cannot('relax'));
        self::assertTrue($account->can('relax'));
    }

    /**
     * @test
     */
    public function syncing_roles_does_not_affect_another_entity_type_with_same_id()
    {
        $user = User::create(['id' => 1]);
        $account = Account::create(['id' => 1]);

        $bouncer = $this->inspector();

        $bouncer->assign('admin')->to($user);
        $bouncer->assign('admin')->to($account);

        self::assertTrue($user->isAn('admin'));
        self::assertTrue($account->isAn('admin'));

        $bouncer->sync($user)->roles([]);

        self::assertTrue($user->isNotAn('admin'));
        self::assertTrue($account->isAn('admin'));
    }

    /**
     * Create a new role with the given name.
     *
     * @param  string  $name
     * @return \Eternity\Laravel\Rbac\Database\Role
     */
    protected function role($name)
    {
        return Role::create(compact('name'));
    }
}
