<?php

namespace Eternity\Laravel\Tests\Unit\Rbac;

use Eternity\Laravel\Rbac\Database\Role;

class RoleLevelsTest extends BaseTestCase
{
    use Concerns\TestsClipboards;

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function a_role_is_allowed_abilities_from_a_lower_level($provider)
    {
        $rbac = $this->prepareLevelsTest($provider, 2, 1);

        self::assertTrue($rbac->can('edit-site'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function a_role_is_not_allowed_abilities_from_the_same_level($provider)
    {
        $rbac = $this->prepareLevelsTest($provider, 2, 2);

        self::assertFalse($rbac->can('edit-site'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function a_role_is_not_allowed_abilities_from_another_role_with_no_level($provider)
    {
        $rbac = $this->prepareLevelsTest($provider, 2, null);

        self::assertFalse($rbac->can('edit-site'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function a_role_with_no_level_is_not_allowed_abilities_from_another_level($provider)
    {
        $rbac = $this->prepareLevelsTest($provider, null, 1);

        self::assertFalse($rbac->can('edit-site'));
    }

    protected function prepareLevelsTest($provider, $grantedLevel, $otherLevel)
    {
        [$rbac, $user] = $provider();

        $admin = Role::create(['name' => 'admin', 'level' => $grantedLevel]);
        $editor = Role::create(['name' => 'editor', 'level' => $otherLevel]);

        $rbac->allow($editor)->to('edit-site');
        $rbac->assign($admin)->to($user);

        return $rbac;
    }
}
