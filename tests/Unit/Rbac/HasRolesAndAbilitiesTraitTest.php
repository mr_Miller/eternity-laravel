<?php

namespace Eternity\Laravel\Tests\Unit\Rbac;

use Eternity\Laravel\Rbac\Database\Models;

class HasRolesAndAbilitiesTraitTest extends BaseTestCase
{
    use Concerns\TestsClipboards;

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function get_abilities_gets_all_allowed_abilities($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow('admin')->to('edit-site');
        $rbac->allow($user)->to('create-posts');
        $rbac->assign('admin')->to($user);

        $rbac->forbid($user)->to('create-sites');
        $rbac->allow('editor')->to('edit-posts');

        self::assertEquals(
            ['create-posts', 'edit-site'],
            $user->getAbilities()->pluck('name')->sort()->values()->all()
        );
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function get_forbidden_abilities_gets_all_forbidden_abilities($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->forbid('admin')->to('edit-site');
        $rbac->forbid($user)->to('create-posts');
        $rbac->assign('admin')->to($user);

        $rbac->allow($user)->to('create-sites');
        $rbac->forbid('editor')->to('edit-posts');

        self::assertEquals(
            ['create-posts', 'edit-site'],
            $user->getForbiddenAbilities()->pluck('name')->sort()->values()->all()
        );
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_give_and_remove_abilities($provider)
    {
        [$rbac, $user] = $provider();

        $user->allow('edit-site');

        self::assertTrue($rbac->can('edit-site'));

        $user->disallow('edit-site');

        self::assertTrue($rbac->cannot('edit-site'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_give_and_remove_model_abilities($provider)
    {
        [$rbac, $user] = $provider();

        $user->allow('delete', $user);

        self::assertTrue($rbac->cannot('delete'));
        self::assertTrue($rbac->cannot('delete', User::class));
        self::assertTrue($rbac->can('delete', $user));

        $user->disallow('delete', $user);

        self::assertTrue($rbac->cannot('delete', $user));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_give_and_remove_ability_for_everything($provider)
    {
        [$rbac, $user] = $provider();

        $user->allow()->everything();

        self::assertTrue($rbac->can('delete'));
        self::assertTrue($rbac->can('delete', '*'));
        self::assertTrue($rbac->can('*', '*'));

        $user->disallow()->everything();

        self::assertTrue($rbac->cannot('delete'));
        self::assertTrue($rbac->cannot('delete', '*'));
        self::assertTrue($rbac->cannot('*', '*'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_forbid_and_unforbid_abilities($provider)
    {
        [$rbac, $user] = $provider();

        $user->allow('edit-site');
        $user->forbid('edit-site');

        self::assertTrue($rbac->cannot('edit-site'));

        $user->unforbid('edit-site');

        self::assertTrue($rbac->can('edit-site'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_forbid_and_unforbid_model_abilities($provider)
    {
        [$rbac, $user] = $provider();

        $user->allow('delete', $user);
        $user->forbid('delete', $user);

        self::assertTrue($rbac->cannot('delete', $user));

        $user->unforbid('delete', $user);

        self::assertTrue($rbac->can('delete', $user));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_forbid_and_unforbid_everything($provider)
    {
        [$rbac, $user] = $provider();

        $user->allow('delete', $user);
        $user->forbid()->everything();

        self::assertTrue($rbac->cannot('delete', $user));

        $user->unforbid()->everything();

        self::assertTrue($rbac->can('delete', $user));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_assign_and_retract_roles($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow('admin')->to('edit-site');
        $user->assign('admin');

        self::assertEquals(['admin'], $user->getRoles()->all());
        self::assertTrue($rbac->can('edit-site'));

        $user->retract('admin');

        self::assertEquals([], $user->getRoles()->all());
        self::assertTrue($rbac->cannot('edit-site'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_check_roles($provider)
    {
        [$rbac, $user] = $provider();

        self::assertTrue($user->isNotAn('admin'));
        self::assertFalse($user->isAn('admin'));

        self::assertTrue($user->isNotA('admin'));
        self::assertFalse($user->isA('admin'));

        $user->assign('admin');

        self::assertTrue($user->isAn('admin'));
        self::assertFalse($user->isAn('editor'));
        self::assertFalse($user->isNotAn('admin'));
        self::assertTrue($user->isNotAn('editor'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_check_multiple_roles($provider)
    {
        [$rbac, $user] = $provider();

        self::assertFalse($user->isAn('admin', 'editor'));

        $user->assign('moderator');
        $user->assign('editor');

        self::assertTrue($user->isAn('admin', 'moderator'));
        self::assertTrue($user->isAll('editor', 'moderator'));
        self::assertFalse($user->isAll('moderator', 'admin'));
    }

    /**
     * @test
     */
    public function deleting_a_model_deletes_the_permissions_pivot_table_records()
    {
        $rbac = $this->inspector();

        $user1 = User::create();
        $user2 = User::create();

        $rbac->allow($user1)->everything();
        $rbac->allow($user2)->everything();

        self::assertEquals(2, $this->db()->table('permissions')->count());

        $user1->delete();

        self::assertEquals(1, $this->db()->table('permissions')->count());
    }

    /**
     * @test
     */
    public function soft_deleting_a_model_persists_the_permissions_pivot_table_records()
    {
        Models::setUsersModel(UserWithSoftDeletes::class);

        $rbac = $this->inspector();

        $user1 = UserWithSoftDeletes::create();
        $user2 = UserWithSoftDeletes::create();

        $rbac->allow($user1)->everything();
        $rbac->allow($user2)->everything();

        self::assertEquals(2, $this->db()->table('permissions')->count());

        $user1->delete();

        self::assertEquals(2, $this->db()->table('permissions')->count());
    }

    /**
     * @test
     */
    public function deleting_a_model_deletes_the_assigned_roles_pivot_table_records()
    {
        $rbac = $this->inspector();

        $user1 = User::create();
        $user2 = User::create();

        $rbac->assign('admin')->to($user1);
        $rbac->assign('admin')->to($user2);

        self::assertEquals(2, $this->db()->table('assigned_roles')->count());

        $user1->delete();

        self::assertEquals(1, $this->db()->table('assigned_roles')->count());
    }

    /**
     * @test
     */
    public function soft_deleting_a_model_persists_the_assigned_roles_pivot_table_records()
    {
        Models::setUsersModel(UserWithSoftDeletes::class);

        $rbac = $this->inspector();

        $user1 = UserWithSoftDeletes::create();
        $user2 = UserWithSoftDeletes::create();

        $rbac->assign('admin')->to($user1);
        $rbac->assign('admin')->to($user2);

        self::assertEquals(2, $this->db()->table('assigned_roles')->count());

        $user1->delete();

        self::assertEquals(2, $this->db()->table('assigned_roles')->count());
    }
}
