<?php

namespace Eternity\Laravel\Tests\Unit\Rbac;

use Eternity\Laravel\Rbac\Database\Models;

class OwnershipTest extends BaseTestCase
{
    use Concerns\TestsClipboards;

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_own_a_model_class($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->toOwn(Account::class);

        $account = Account::create(['user_id' => $user->id]);

        self::assertTrue($rbac->cannot('update', Account::class));
        self::assertTrue($rbac->can('update', $account));

        $account->user_id = 99;

        self::assertTrue($rbac->cannot('update', $account));

        $rbac->allow($user)->to('update', $account);
        $rbac->disallow($user)->toOwn(Account::class);

        self::assertTrue($rbac->can('update', $account));

        $rbac->disallow($user)->to('update', $account);

        self::assertTrue($rbac->cannot('update', $account));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_own_a_model($provider)
    {
        [$rbac, $user] = $provider();

        $account1 = Account::create(['user_id' => $user->id]);
        $account2 = Account::create(['user_id' => $user->id]);

        $rbac->allow($user)->toOwn($account1);

        self::assertTrue($rbac->cannot('update', Account::class));
        self::assertTrue($rbac->cannot('update', $account2));
        self::assertTrue($rbac->can('update', $account1));

        $account1->user_id = 99;

        self::assertTrue($rbac->cannot('update', $account1));

        $rbac->allow($user)->to('update', $account1);
        $rbac->disallow($user)->toOwn($account1);

        self::assertTrue($rbac->can('update', $account1));

        $rbac->disallow($user)->to('update', $account1);

        self::assertTrue($rbac->cannot('update', $account1));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_own_a_model_for_a_given_ability($provider)
    {
        [$rbac, $user] = $provider();

        $account1 = Account::create(['user_id' => $user->id]);
        $account2 = Account::create(['user_id' => $user->id]);

        $rbac->allow($user)->toOwn($account1)->to('update');
        $rbac->allow($user)->toOwn($account2)->to(['view', 'update']);

        self::assertTrue($rbac->cannot('update', Account::class));
        self::assertTrue($rbac->can('update', $account1));
        self::assertTrue($rbac->cannot('delete', $account1));
        self::assertTrue($rbac->can('view', $account2));
        self::assertTrue($rbac->can('update', $account2));
        self::assertTrue($rbac->cannot('delete', $account2));

        $account1->user_id = 99;

        self::assertTrue($rbac->cannot('update', $account1));

        $rbac->allow($user)->to('update', $account1);
        $rbac->disallow($user)->toOwn($account1)->to('update');

        self::assertTrue($rbac->can('update', $account1));

        $rbac->disallow($user)->to('update', $account1);

        self::assertTrue($rbac->cannot('update', $account1));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_own_everything($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->toOwnEverything();

        $account = Account::create(['user_id' => $user->id]);

        self::assertTrue($rbac->cannot('delete', Account::class));
        self::assertTrue($rbac->can('delete', $account));

        $account->user_id = 99;

        self::assertTrue($rbac->cannot('delete', $account));

        $account->user_id = $user->id;

        $rbac->disallow($user)->toOwnEverything();

        self::assertTrue($rbac->cannot('delete', $account));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_own_everything_for_a_given_ability($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->toOwnEverything()->to('view');

        $account = Account::create(['user_id' => $user->id]);

        self::assertTrue($rbac->cannot('delete', Account::class));
        self::assertTrue($rbac->cannot('delete', $account));
        self::assertTrue($rbac->can('view', $account));

        $account->user_id = 99;

        self::assertTrue($rbac->cannot('view', $account));

        $account->user_id = $user->id;

        $rbac->disallow($user)->toOwnEverything()->to('view');

        self::assertTrue($rbac->cannot('view', $account));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_use_custom_ownership_attribute($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->ownedVia('userId');

        $account = Account::create()->fill(['userId' => $user->id]);

        $rbac->allow($user)->toOwn(Account::class);

        self::assertTrue($rbac->can('view', $account));

        Models::reset();
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_use_custom_ownership_attribute_for_model_type($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->ownedVia(Account::class, 'userId');

        $account = Account::create()->fill(['userId' => $user->id]);

        $rbac->allow($user)->toOwn(Account::class);

        self::assertTrue($rbac->can('view', $account));

        Models::reset();
    }
    
    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_forbid_abilities_after_owning_a_model_class($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->toOwn(Account::class);
        $rbac->forbid($user)->to('publish', Account::class);

        $account = Account::create(['user_id' => $user->id]);

        self::assertTrue($rbac->can('update', $account));
        self::assertTrue($rbac->cannot('publish', $account));
    }
}
