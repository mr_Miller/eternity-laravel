<?php

namespace Eternity\Laravel\Tests\Unit\Rbac;

use Eternity\Laravel\Rbac\Database\Ability;

class AbilitiesForModelsTest extends BaseTestCase
{
    use Concerns\TestsClipboards;

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function model_blanket_ability($provider)
    {
        [$rbac, $user1, $user2] = $provider(2);

        $rbac->allow($user1)->to('edit', User::class);

        self::assertTrue($rbac->cannot('edit'));
        self::assertTrue($rbac->can('edit', User::class));
        self::assertTrue($rbac->can('edit', $user2));

        $rbac->disallow($user1)->to('edit', User::class);

        self::assertTrue($rbac->cannot('edit'));
        self::assertTrue($rbac->cannot('edit', User::class));
        self::assertTrue($rbac->cannot('edit', $user2));

        $rbac->disallow($user1)->to('edit');

        self::assertTrue($rbac->cannot('edit'));
        self::assertTrue($rbac->cannot('edit', User::class));
        self::assertTrue($rbac->cannot('edit', $user2));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function individual_model_ability($provider)
    {
        [$rbac, $user1, $user2] = $provider(2);

        $rbac->allow($user1)->to('edit', $user2);

        self::assertTrue($rbac->cannot('edit'));
        self::assertTrue($rbac->cannot('edit', User::class));
        self::assertTrue($rbac->cannot('edit', $user1));
        self::assertTrue($rbac->can('edit', $user2));

        $rbac->disallow($user1)->to('edit', User::class);

        self::assertTrue($rbac->can('edit', $user2));

        $rbac->disallow($user1)->to('edit', $user1);

        self::assertTrue($rbac->can('edit', $user2));

        $rbac->disallow($user1)->to('edit', $user2);

        self::assertTrue($rbac->cannot('edit'));
        self::assertTrue($rbac->cannot('edit', User::class));
        self::assertTrue($rbac->cannot('edit', $user1));
        self::assertTrue($rbac->cannot('edit', $user2));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function blanket_ability_and_individual_model_ability_are_kept_separate($provider)
    {
        [$rbac, $user1, $user2] = $provider(2);

        $rbac->allow($user1)->to('edit', User::class);
        $rbac->allow($user1)->to('edit', $user2);

        self::assertTrue($rbac->can('edit', User::class));
        self::assertTrue($rbac->can('edit', $user2));

        $rbac->disallow($user1)->to('edit', User::class);

        self::assertTrue($rbac->cannot('edit', User::class));
        self::assertTrue($rbac->can('edit', $user2));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function allowing_on_non_existent_model_throws($provider)
    {
        $this->expectException('InvalidArgumentException');

        [$rbac, $user] = $provider();

        $rbac->allow($user)->to('delete', new User);
    }

    /**
     * @test
     */
    public function can_create_an_ability_for_a_model()
    {
        $ability = Ability::createForModel(Account::class, 'delete');

        self::assertEquals(Account::class, $ability->entity_type);
        self::assertEquals('delete', $ability->name);
        self::assertNull($ability->entity_id);
    }

    /**
     * @test
     */
    public function can_create_an_ability_for_a_model_plus_extra_attributes()
    {
        $ability = Ability::createForModel(Account::class, [
            'name' => 'delete',
            'title' => 'Delete Accounts',
        ]);

        self::assertEquals('Delete Accounts', $ability->title);
        self::assertEquals(Account::class, $ability->entity_type);
        self::assertEquals('delete', $ability->name);
        self::assertNull($ability->entity_id);
    }

    /**
     * @test
     */
    public function can_create_an_ability_for_a_model_instance()
    {
        $user = User::create();

        $ability = Ability::createForModel($user, 'delete');

        self::assertEquals($user->id, $ability->entity_id);
        self::assertEquals(User::class, $ability->entity_type);
        self::assertEquals('delete', $ability->name);
    }

    /**
     * @test
     */
    public function can_create_an_ability_for_a_model_instance_plus_extra_attributes()
    {
        $user = User::create();

        $ability = Ability::createForModel($user, [
            'name' => 'delete',
            'title' => 'Delete this user',
        ]);

        self::assertEquals('Delete this user', $ability->title);
        self::assertEquals($user->id, $ability->entity_id);
        self::assertEquals(User::class, $ability->entity_type);
        self::assertEquals('delete', $ability->name);
    }

    /**
     * @test
     */
    public function can_create_an_ability_for_all_models()
    {
        $ability = Ability::createForModel('*', 'delete');

        self::assertEquals('*', $ability->entity_type);
        self::assertEquals('delete', $ability->name);
        self::assertNull($ability->entity_id);
    }

    /**
     * @test
     */
    public function can_create_an_ability_for_all_models_plus_extra_attributes()
    {
        $ability = Ability::createForModel('*', [
            'name' => 'delete',
            'title' => 'Delete everything',
        ]);

        self::assertEquals('Delete everything', $ability->title);
        self::assertEquals('*', $ability->entity_type);
        self::assertEquals('delete', $ability->name);
        self::assertNull($ability->entity_id);
    }
}
