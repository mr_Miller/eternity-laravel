<?php

namespace Eternity\Laravel\Tests\Unit\Rbac\Unit\Constraints;

use Eternity\Laravel\Rbac\Constraints\Constraint;
use Eternity\Laravel\Rbac\Constraints\Group;
use Eternity\Laravel\Tests\Unit\Rbac\Account;
use Eternity\Laravel\Tests\Unit\Rbac\User;
use PHPUnit\Framework\TestCase;

class GroupsTest extends TestCase
{
    /**
     * @test
     */
    public function named_and_constructor()
    {
        $group = Group::withAnd();

        self::assertInstanceOf(Group::class, $group);
        self::assertEquals('and', $group->logicalOperator());
    }

    /**
     * @test
     */
    public function named_or_constructor()
    {
        $group = Group::withOr();

        self::assertInstanceOf(Group::class, $group);
        self::assertEquals('or', $group->logicalOperator());
    }

    /**
     * @test
     */
    public function group_of_constraints_only_passes_if_all_constraints_pass_the_check()
    {
        $account = new Account([
            'name' => 'the-account',
            'active' => false,
        ]);

        $groupA = new Group([
            Constraint::where('name', 'the-account'),
            Constraint::where('active', false),
        ]);

        $groupB = new Group([
            Constraint::where('name', 'the-account'),
            Constraint::where('active', true),
        ]);

        self::assertTrue($groupA->check($account, new User));
        self::assertFalse($groupB->check($account, new User));
    }

    /**
     * @test
     */
    public function group_of_ors_passes_if_any_constraint_passes_the_check()
    {
        $account = new Account([
            'name' => 'the-account',
            'active' => false,
        ]);

        $groupA = new Group([
            Constraint::orWhere('name', 'the-account'),
            Constraint::orWhere('active', true),
        ]);

        $groupB = new Group([
            Constraint::orWhere('name', 'a-different-account'),
            Constraint::orWhere('active', true),
        ]);

        self::assertTrue($groupA->check($account, new User));
        self::assertFalse($groupB->check($account, new User));
    }

    /**
     * @test
     */
    public function group_can_be_serialized_and_deserialized()
    {
        $activeAccount = new Account([
            'name' => 'the-account',
            'active' => true,
        ]);

        $inactiveAccount = new Account([
            'name' => 'the-account',
            'active' => false,
        ]);

        $group = $this->serializeAndDeserializeGroup(new Group([
            Constraint::where('name', 'the-account'),
            Constraint::where('active', true),
        ]));

        self::assertInstanceOf(Group::class, $group);
        self::assertTrue($group->check($activeAccount, new User));
        self::assertFalse($group->check($inactiveAccount, new User));
    }

    /**
     * @test
     */
    public function group_can_be_added_to()
    {
        $activeAccount = new Account([
            'name' => 'account',
            'active' => true,
        ]);

        $inactiveAccount = new Account([
            'name' => 'account',
            'active' => false,
        ]);

        $group = (new Group)
            ->add(Constraint::where('name', 'account'))
            ->add(Constraint::where('active', true));

        self::assertTrue($group->check($activeAccount, new User));
        self::assertFalse($group->check($inactiveAccount, new User));
    }

    /**
     * Convert the given object to JSON, then back.
     *
     * @param  \Eternity\Laravel\Rbac\Constraints\Group  $group
     * @return \Eternity\Laravel\Rbac\Constraints\Group
     */
    protected function serializeAndDeserializeGroup(Group $group)
    {
        $data = json_decode(json_encode($group->data()), true);

        return $data['class']::fromData($data['params']);
    }
}
