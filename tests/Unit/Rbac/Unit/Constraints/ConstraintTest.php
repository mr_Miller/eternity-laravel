<?php

namespace Eternity\Laravel\Tests\Unit\Rbac\Unit\Constraints;

use Eternity\Laravel\Rbac\Constraints\ColumnConstraint;
use Eternity\Laravel\Rbac\Constraints\Constraint;
use Eternity\Laravel\Rbac\Constraints\ValueConstraint;
use Eternity\Laravel\Tests\Unit\Rbac\Account;
use Eternity\Laravel\Tests\Unit\Rbac\User;
use PHPUnit\Framework\TestCase;

class ConstraintTest extends TestCase
{
    /**
     * @test
     */
    public function value_constraint_implicit_equals()
    {
        $authority = new User();
        $activeAccount = new Account(['active' => true]);
        $inactiveAccount = new Account(['active' => false]);

        $constraint = Constraint::where('active', true);

        self::assertTrue($constraint->check($activeAccount, $authority));
        self::assertFalse($constraint->check($inactiveAccount, $authority));
    }

    /**
     * @test
     */
    public function value_constraint_explicit_equals()
    {
        $authority = new User();
        $activeAccount = new Account(['active' => true]);
        $inactiveAccount = new Account(['active' => false]);

        $constraint = Constraint::where('active', '=', true);

        self::assertTrue($constraint->check($activeAccount, $authority));
        self::assertFalse($constraint->check($inactiveAccount, $authority));
    }

    /**
     * @test
     */
    public function value_constraint_explicit_double_equals()
    {
        $authority = new User();
        $activeAccount = new Account(['active' => true]);
        $inactiveAccount = new Account(['active' => false]);

        $constraint = Constraint::where('active', '==', true);

        self::assertTrue($constraint->check($activeAccount, $authority));
        self::assertFalse($constraint->check($inactiveAccount, $authority));
    }

    /**
     * @test
     */
    public function value_constraint_not_equals()
    {
        $authority = new User();
        $activeAccount = new Account(['active' => true]);
        $inactiveAccount = new Account(['active' => false]);

        $constraint = Constraint::where('active', '!=', false);

        self::assertTrue($constraint->check($activeAccount, $authority));
        self::assertFalse($constraint->check($inactiveAccount, $authority));
    }

    /**
     * @test
     */
    public function value_constraint_greater_than()
    {
        $authority = new User();
        $forty = new User(['age' => 40]);
        $fortyOne = new User(['age' => 41]);

        $constraint = Constraint::where('age', '>', 40);

        self::assertTrue($constraint->check($fortyOne, $authority));
        self::assertFalse($constraint->check($forty, $authority));
    }

    /**
     * @test
     */
    public function value_constraint_less_than()
    {
        $authority = new User();
        $thirtyNine = new User(['age' => 39]);
        $forty = new User(['age' => 40]);

        $constraint = Constraint::where('age', '<', 40);

        self::assertTrue($constraint->check($thirtyNine, $authority));
        self::assertFalse($constraint->check($forty, $authority));
    }

    /**
     * @test
     */
    public function value_constraint_greater_than_or_equal()
    {
        $authority = new User();
        $minor = new User(['age' => 17]);
        $adult = new User(['age' => 18]);
        $senior = new User(['age' => 80]);

        $constraint = Constraint::where('age', '>=', 18);

        self::assertTrue($constraint->check($adult, $authority));
        self::assertTrue($constraint->check($senior, $authority));
        self::assertFalse($constraint->check($minor, $authority));
    }

    /**
     * @test
     */
    public function value_constraint_less_than_or_equal()
    {
        $authority = new User();
        $youngerTeen = new User(['age' => 18]);
        $olderTeen = new User(['age' => 19]);
        $adult = new User(['age' => 20]);

        $constraint = Constraint::where('age', '<=', 19);

        self::assertTrue($constraint->check($youngerTeen, $authority));
        self::assertTrue($constraint->check($olderTeen, $authority));
        self::assertFalse($constraint->check($adult, $authority));
    }

    /**
     * @test
     */
    public function column_constraint_implicit_equals()
    {
        $authority = new User(['age' => 1]);
        $one = new User(['age' => 1]);
        $two = new User(['age' => 2]);

        $constraint = Constraint::whereColumn('age', 'age');

        self::assertTrue($constraint->check($one, $authority));
        self::assertFalse($constraint->check($two, $authority));
    }

    /**
     * @test
     */
    public function column_constraint_explicit_equals()
    {
        $authority = new User(['age' => 1]);
        $one = new User(['age' => 1]);
        $two = new User(['age' => 2]);

        $constraint = Constraint::whereColumn('age', '=', 'age');

        self::assertTrue($constraint->check($one, $authority));
        self::assertFalse($constraint->check($two, $authority));
    }

    /**
     * @test
     */
    public function column_constraint_explicit_double_equals()
    {
        $authority = new User(['age' => 1]);
        $one = new User(['age' => 1]);
        $two = new User(['age' => 2]);

        $constraint = Constraint::whereColumn('age', '=', 'age');

        self::assertTrue($constraint->check($one, $authority));
        self::assertFalse($constraint->check($two, $authority));
    }

    /**
     * @test
     */
    public function column_constraint_not_equals()
    {
        $authority = new User(['age' => 1]);
        $one = new User(['age' => 1]);
        $two = new User(['age' => 2]);

        $constraint = Constraint::whereColumn('age', '!=', 'age');

        self::assertTrue($constraint->check($two, $authority));
        self::assertFalse($constraint->check($one, $authority));
    }

    /**
     * @test
     */
    public function column_constraint_greater_than()
    {
        $authority = new User(['age' => 18]);

        $younger = new User(['age' => 17]);
        $same = new User(['age' => 18]);
        $older = new User(['age' => 19]);

        $constraint = Constraint::whereColumn('age', '>', 'age');

        self::assertTrue($constraint->check($older, $authority));
        self::assertFalse($constraint->check($younger, $authority));
        self::assertFalse($constraint->check($same, $authority));
    }

    /**
     * @test
     */
    public function column_constraint_less_than()
    {
        $authority = new User(['age' => 18]);

        $younger = new User(['age' => 17]);
        $same = new User(['age' => 18]);
        $older = new User(['age' => 19]);

        $constraint = Constraint::whereColumn('age', '<', 'age');

        self::assertTrue($constraint->check($younger, $authority));
        self::assertFalse($constraint->check($older, $authority));
        self::assertFalse($constraint->check($same, $authority));
    }

    /**
     * @test
     */
    public function column_constraint_greater_than_or_equal()
    {
        $authority = new User(['age' => 18]);

        $younger = new User(['age' => 17]);
        $same = new User(['age' => 18]);
        $older = new User(['age' => 19]);

        $constraint = Constraint::whereColumn('age', '>=', 'age');

        self::assertTrue($constraint->check($same, $authority));
        self::assertTrue($constraint->check($older, $authority));
        self::assertFalse($constraint->check($younger, $authority));
    }

    /**
     * @test
     */
    public function column_constraint_less_than_or_equal()
    {
        $authority = new User(['age' => 18]);

        $younger = new User(['age' => 17]);
        $same = new User(['age' => 18]);
        $older = new User(['age' => 19]);

        $constraint = Constraint::whereColumn('age', '<=', 'age');

        self::assertTrue($constraint->check($younger, $authority));
        self::assertTrue($constraint->check($same, $authority));
        self::assertFalse($constraint->check($older, $authority));
    }

    /**
     * @test
     */
    public function value_constraint_can_be_properly_serialized_and_deserialized()
    {
        $authority = new User();
        $activeAccount = new Account(['active' => true]);
        $inactiveAccount = new Account(['active' => false]);

        $constraint = $this->serializeAndDeserializeConstraint(
            Constraint::where('active', true)
        );

        self::assertInstanceOf(ValueConstraint::class, $constraint);
        self::assertTrue($constraint->check($activeAccount, $authority));
        self::assertFalse($constraint->check($inactiveAccount, $authority));
    }

    /**
     * @test
     */
    public function column_constraint_can_be_properly_serialized_and_deserialized()
    {
        $authority = new User(['age' => 1]);
        $one = new User(['age' => 1]);
        $two = new User(['age' => 2]);

        $constraint = $this->serializeAndDeserializeConstraint(
            Constraint::whereColumn('age', 'age')
        );

        self::assertInstanceOf(ColumnConstraint::class, $constraint);
        self::assertTrue($constraint->check($one, $authority));
        self::assertFalse($constraint->check($two, $authority));
    }

    /**
     * Convert the given object to JSON, then back.
     *
     * @param  \Eternity\Laravel\Rbac\Constraints\Constraint  $group
     * @return \Eternity\Laravel\Rbac\Constraints\Constraint
     */
    protected function serializeAndDeserializeConstraint(Constraint $constraint)
    {
        $data = json_decode(json_encode($constraint->data()), true);

        return $data['class']::fromData($data['params']);
    }
}
