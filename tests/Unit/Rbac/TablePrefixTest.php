<?php

namespace Eternity\Laravel\Tests\Unit\Rbac;

class TablePrefixTest extends BaseTestCase
{
    use Concerns\TestsClipboards;

    protected function registerDatabaseContainerBindings()
    {
        parent::registerDatabaseContainerBindings();

        $this->db()->connection()->setTablePrefix('rbac_');
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function test_ability_queries_work_with_prefix($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->everything();

        self::assertTrue($rbac->can('do-something'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function test_role_queries_work_with_prefix($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->assign('artisan')->to($user);

        self::assertTrue($rbac->is($user)->an('artisan'));
    }
}
