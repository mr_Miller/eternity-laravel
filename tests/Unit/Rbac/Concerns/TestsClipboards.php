<?php

namespace Eternity\Laravel\Tests\Unit\Rbac\Concerns;

use Eternity\Laravel\Rbac\Clipboard;
use Eternity\Laravel\Rbac\Clipboards\CachedClipboard;
use Eternity\Laravel\Tests\Unit\Rbac\User;
use Illuminate\Cache\NullStore;

trait TestsClipboards
{
    /**
     * Provides a rbac instance (and users) for each clipboard, respectively.
     *
     * @return array
     */
    public function rbacProvider()
    {
        return [
            'basic clipboard' => [
                function ($authoriesCount = 1, $authority = User::class) {
                    return $this->configure(
                        new Clipboard, $authoriesCount, $authority
                    );
                }
            ],
            'null cached clipboard' => [
                function ($authoriesCount = 1, $authority = User::class) {
                    return $this->configure(
                        new CachedClipboard(new NullStore), $authoriesCount, $authority
                    );
                }
            ],
        ];
    }

    /**
     * Provide the rbac instance (with its user) using the given clipboard.
     *
     * @param  \Eternity\Laravel\Rbac\Clipboard  $clipboard
     * @param  int  $authoriesCount
     * @param  string  $authority
     * @return array
     */
    protected function configure($clipboard, $authoriesCount, $authority)
    {
        $authorities = array_map(function () use ($authority) {
            return $authority::create();
        }, range(0, $authoriesCount));

        $this->clipboard = $clipboard;

        $rbac = $this->inspector($authorities[0]);

        return array_merge([$rbac], $authorities);
    }
}
