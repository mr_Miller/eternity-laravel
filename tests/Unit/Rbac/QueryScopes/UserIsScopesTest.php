<?php

namespace Eternity\Laravel\Tests\Unit\Rbac\QueryScopes;

use Eternity\Laravel\Tests\Unit\Rbac\BaseTestCase;
use Eternity\Laravel\Tests\Unit\Rbac\User;

class UserIsScopesTest extends BaseTestCase
{
    /**
     * @test
     */
    public function users_can_be_constrained_to_having_a_role()
    {
        $user1 = User::create(['name' => 'Joseph']);
        $user2 = User::create(['name' => 'Silber']);

        $user1->assign('reader');
        $user2->assign('subscriber');

        $users = User::whereIs('reader')->get();

        self::assertCount(1, $users);
        self::assertEquals('Joseph', $users->first()->name);
    }

    /**
     * @test
     */
    public function users_can_be_constrained_to_having_one_of_many_roles()
    {
        $user1 = User::create(['name' => 'Joseph']);
        $user2 = User::create(['name' => 'Silber']);

        $user1->assign('reader');
        $user2->assign('subscriber');

        $users = User::whereIs('admin', 'subscriber')->get();

        self::assertCount(1, $users);
        self::assertEquals('Silber', $users->first()->name);
    }

    /**
     * @test
     */
    public function users_can_be_constrained_to_having_all_provided_roles()
    {
        $user1 = User::create(['name' => 'Joseph']);
        $user2 = User::create(['name' => 'Silber']);

        $user1->assign('reader')->assign('subscriber');
        $user2->assign('subscriber');

        $users = User::whereIsAll('subscriber', 'reader')->get();

        self::assertCount(1, $users);
        self::assertEquals('Joseph', $users->first()->name);
    }

    /**
     * @test
     */
    public function users_can_be_constrained_to_not_having_a_role()
    {
        $user1 = User::create();
        $user2 = User::create();
        $user3 = User::create();

        $user1->assign('admin');
        $user2->assign('editor');
        $user3->assign('subscriber');

        $users = User::whereIsNot('admin')->get();

        self::assertCount(2, $users);
        self::assertFalse($users->contains($user1));
        self::assertTrue($users->contains($user2));
        self::assertTrue($users->contains($user3));
    }

    /**
     * @test
     */
    public function users_can_be_constrained_to_not_having_any_of_the_given_roles()
    {
        $user1 = User::create();
        $user2 = User::create();
        $user3 = User::create();

        $user1->assign('admin');
        $user2->assign('editor');
        $user3->assign('subscriber');

        $users = User::whereIsNot('superadmin', 'editor', 'subscriber')->get();

        self::assertCount(1, $users);
        self::assertTrue($users->contains($user1));
        self::assertFalse($users->contains($user2));
        self::assertFalse($users->contains($user3));
    }
}
