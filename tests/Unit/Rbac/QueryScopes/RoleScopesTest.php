<?php

namespace Eternity\Laravel\Tests\Unit\Rbac\QueryScopes;

use Eternity\Laravel\Rbac\Database\Role;
use Eternity\Laravel\Tests\Unit\Rbac\BaseTestCase;
use Eternity\Laravel\Tests\Unit\Rbac\User;

class RoleScopesTest extends BaseTestCase
{
    /**
     * @test
     */
    public function roles_can_be_constrained_by_a_user()
    {
        $rbac = $this->inspector($user = User::create());

        Role::create(['name' => 'admin']);
        Role::create(['name' => 'editor']);
        Role::create(['name' => 'manager']);
        Role::create(['name' => 'subscriber']);

        $rbac->assign('admin')->to($user);
        $rbac->assign('manager')->to($user);

        $roles = Role::whereAssignedTo($user)->get();

        self::assertCount(2, $roles);
        self::assertTrue($roles->contains('name', 'admin'));
        self::assertTrue($roles->contains('name', 'manager'));
        self::assertFalse($roles->contains('name', 'editor'));
        self::assertFalse($roles->contains('name', 'subscriber'));
    }

    /**
     * @test
     */
    public function roles_can_be_constrained_by_a_collection_of_users()
    {
        $user1 = User::create();
        $user2 = User::create();

        $rbac = $this->inspector($user1);

        Role::create(['name' => 'admin']);
        Role::create(['name' => 'editor']);
        Role::create(['name' => 'manager']);
        Role::create(['name' => 'subscriber']);

        $rbac->assign('editor')->to($user1);
        $rbac->assign('manager')->to($user1);
        $rbac->assign('subscriber')->to($user2);

        $roles = Role::whereAssignedTo(User::all())->get();

        self::assertCount(3, $roles);
        self::assertTrue($roles->contains('name', 'manager'));
        self::assertTrue($roles->contains('name', 'editor'));
        self::assertTrue($roles->contains('name', 'subscriber'));
        self::assertFalse($roles->contains('name', 'admin'));
    }

    /**
     * @test
     */
    public function roles_can_be_constrained_by_a_model_name_and_keys()
    {
        $user1 = User::create();
        $user2 = User::create();

        $rbac = $this->inspector($user1);

        Role::create(['name' => 'admin']);
        Role::create(['name' => 'editor']);
        Role::create(['name' => 'manager']);
        Role::create(['name' => 'subscriber']);

        $rbac->assign('editor')->to($user1);
        $rbac->assign('manager')->to($user1);
        $rbac->assign('subscriber')->to($user2);

        $roles = Role::whereAssignedTo(User::class, User::all()->modelKeys())->get();

        self::assertCount(3, $roles);
        self::assertTrue($roles->contains('name', 'manager'));
        self::assertTrue($roles->contains('name', 'editor'));
        self::assertTrue($roles->contains('name', 'subscriber'));
        self::assertFalse($roles->contains('name', 'admin'));
    }
}
