<?php

namespace Eternity\Laravel\Tests\Unit\Rbac;

use Eternity\Laravel\Rbac\Database\Role;

class AuthorizableTest extends BaseTestCase
{
    use Concerns\TestsClipboards;

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function checking_simple_abilities_on_roles($provider)
    {
        $provider();

        $role = Role::create(['name' => 'admin']);

        $role->allow('scream');

        self::assertTrue($role->can('scream'));
        self::assertTrue($role->cannot('cry'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function checking_model_abilities_on_roles($provider)
    {
        $provider();

        $role = Role::create(['name' => 'admin']);

        $role->allow('create', User::class);

        self::assertTrue($role->can('create', User::class));
        self::assertTrue($role->cannot('create', Account::class));
        self::assertTrue($role->cannot('update', User::class));
        self::assertTrue($role->cannot('create'));
    }
}
