<?php

namespace Eternity\Laravel\Tests\Unit\Rbac;

use Eternity\Laravel\Rbac\Inspector;

class AfterPoliciesTest extends BaseTestCase
{
    use Concerns\TestsClipboards;

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function fails_auth_check_when_policy_fails_even_if_rbac_allows($provider)
    {
        [$rbac, $user] = $provider();

        $this->setUpWithPolicy($rbac);

        $account = Account::create(['name' => 'false']);

        $rbac->allow($user)->to('view', $account);

        self::assertTrue($rbac->cannot('view', $account));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function passes_auth_check_when_policy_passes_even_if_rbac_fails($provider)
    {
        [$rbac, $user] = $provider();

        $this->setUpWithPolicy($rbac);

        $account = Account::create(['name' => 'true']);

        $rbac->forbid($user)->to('view', $account);

        self::assertTrue($rbac->can('view', $account));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function passes_auth_check_when_rbac_allows($provider)
    {
        [$rbac, $user] = $provider();

        $this->setUpWithPolicy($rbac);

        $account = Account::create(['name' => 'ignored by policy']);

        $rbac->allow($user)->to('view', $account);

        self::assertTrue($rbac->can('view', $account));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function fails_auth_check_when_rbac_does_not_allow($provider)
    {
        [$rbac, $user] = $provider();

        $this->setUpWithPolicy($rbac);

        $account = Account::create(['name' => 'ignored by policy']);

        self::assertTrue($rbac->cannot('view', $account));
    }

    /**
     * Set up the given RBAC instance with the test policy.
     *
     * @param \Eternity\Laravel\Rbac\Inspector  $rbac
     */
    protected function setUpWithPolicy(Inspector $rbac)
    {
        $rbac->runAfterPolicies();

        $rbac->gate()->policy(Account::class, AccountPolicyForAfter::class);
    }
}

class AccountPolicyForAfter
{
    public function view($user, $account)
    {
        if ($account->name == 'true') {
            return true;
        }

        if ($account->name == 'false') {
            return false;
        }
    }
}
