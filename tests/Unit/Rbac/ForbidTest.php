<?php

namespace Eternity\Laravel\Tests\Unit\Rbac;

class ForbidTest extends BaseTestCase
{
    use Concerns\TestsClipboards;

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function an_allowed_simple_ability_is_not_granted_when_forbidden($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->to('edit-site');
        $rbac->forbid($user)->to('edit-site');

        self::assertTrue($rbac->cannot('edit-site'));

        $rbac->unforbid($user)->to('edit-site');

        self::assertTrue($rbac->can('edit-site'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function an_allowed_model_ability_is_not_granted_when_forbidden($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->to('delete', $user);
        $rbac->forbid($user)->to('delete', $user);

        self::assertTrue($rbac->cannot('delete', $user));

        $rbac->unforbid($user)->to('delete', $user);

        self::assertTrue($rbac->can('delete', $user));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function an_allowed_model_class_ability_is_not_granted_when_forbidden($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->to('delete', User::class);
        $rbac->forbid($user)->to('delete', User::class);

        self::assertTrue($rbac->cannot('delete', User::class));

        $rbac->unforbid($user)->to('delete', User::class);

        self::assertTrue($rbac->can('delete', User::class));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function forbidding_a_single_model_forbids_even_with_allowed_model_class_ability($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->to('delete', User::class);
        $rbac->forbid($user)->to('delete', $user);

        self::assertTrue($rbac->cannot('delete', $user));

        $rbac->unforbid($user)->to('delete', $user);

        self::assertTrue($rbac->can('delete', $user));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function forbidding_a_single_model_does_not_forbid_other_models($provider)
    {
        [$rbac, $user1, $user2] = $provider(2);

        $rbac->allow($user1)->to('delete', User::class);
        $rbac->forbid($user1)->to('delete', $user2);

        self::assertTrue($rbac->can('delete', $user1));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function forbidding_a_model_class_forbids_individual_models($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->to('delete', $user);
        $rbac->forbid($user)->to('delete', User::class);

        self::assertTrue($rbac->cannot('delete', $user));

        $rbac->unforbid($user)->to('delete', $user);

        self::assertTrue($rbac->cannot('delete', $user));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function forbidding_an_ability_through_a_role($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->forbid('admin')->to('delete', User::class);
        $rbac->allow($user)->to('delete', User::class);
        $rbac->assign('admin')->to($user);

        self::assertTrue($rbac->cannot('delete', User::class));
        self::assertTrue($rbac->cannot('delete', $user));

        $rbac->unforbid('admin')->to('delete', User::class);

        self::assertTrue($rbac->can('delete', User::class));
        self::assertTrue($rbac->can('delete', $user));

        $rbac->forbid('admin')->to('delete', $user);

        self::assertTrue($rbac->can('delete', User::class));
        self::assertTrue($rbac->cannot('delete', $user));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function forbidding_an_ability_allowed_through_a_role($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow('admin')->to('delete', User::class);
        $rbac->forbid($user)->to('delete', User::class);
        $rbac->assign('admin')->to($user);

        self::assertTrue($rbac->cannot('delete', User::class));
        self::assertTrue($rbac->cannot('delete', $user));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function forbidding_an_ability_when_everything_is_allowed($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->everything();
        $rbac->forbid($user)->toManage(User::class);

        self::assertTrue($rbac->can('create', Account::class));
        self::assertTrue($rbac->cannot('create', User::class));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function forbid_an_ability_on_everything($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->everything();
        $rbac->forbid($user)->to('delete')->everything();

        self::assertTrue($rbac->can('create', Account::class));
        self::assertTrue($rbac->cannot('delete', User::class));
        self::assertTrue($rbac->cannot('delete', $user));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function forbidding_and_unforbidding_an_ability_for_everyone($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->everything();
        $rbac->forbidEveryone()->to('delete', Account::class);

        self::assertTrue($rbac->can('delete', User::class));
        self::assertTrue($rbac->cannot('delete', Account::class));

        $rbac->unforbidEveryone()->to('delete', Account::class);

        self::assertTrue($rbac->can('delete', Account::class));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function forbidding_an_ability_stops_all_further_checks($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->define('sleep', function () {
            return true;
        });

        self::assertTrue($rbac->can('sleep'));

        $rbac->forbid($user)->to('sleep');

        self::assertTrue($rbac->cannot('sleep'));
    }
}
