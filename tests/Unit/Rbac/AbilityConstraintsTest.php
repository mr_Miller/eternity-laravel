<?php

namespace Eternity\Laravel\Tests\Unit\Rbac;

use Eternity\Laravel\Rbac\Constraints\Constraint;
use Eternity\Laravel\Rbac\Constraints\Group;
use Eternity\Laravel\Rbac\Database\Ability;

class AbilityConstraintsTest extends BaseTestCase
{
    /**
     * @test
     */
    public function can_get_empty_constraints()
    {
        $group = Ability::createForModel(Account::class, '*')->getConstraints();

        self::assertInstanceOf(Group::class, $group);
    }

    /**
     * @test
     */
    public function can_check_if_has_constraints()
    {
        $empty = Ability::makeForModel(Account::class, '*');

        $full = Ability::makeForModel(Account::class, '*')->setConstraints(
            Group::withAnd()->add(Constraint::where('active', true))
        );

        self::assertFalse($empty->hasConstraints());
        self::assertTrue($full->hasConstraints());
    }

    /**
     * @test
     */
    public function can_set_and_get_constraints()
    {
        $ability = Ability::makeForModel(Account::class, '*')->setConstraints(
            new Group([
                Constraint::where('active', true)
            ])
        );

        $ability->save();

        $constraints = Ability::find($ability->id)->getConstraints();

        self::assertInstanceOf(Group::class, $constraints);
        self::assertTrue($constraints->check(new Account(['active' => true]), new User));
        self::assertFalse($constraints->check(new Account(['active' => false]), new User));
    }
}
