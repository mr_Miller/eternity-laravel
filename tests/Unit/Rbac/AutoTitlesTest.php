<?php

namespace Eternity\Laravel\Tests\Unit\Rbac;

use Eternity\Laravel\Rbac\Database\Role;

class AutoTitlesTest extends BaseTestCase
{
    /**
     * @test
     */
    public function role_title_is_never_overwritten()
    {
        $role = Role::create(['name' => 'admin', 'title' => 'Something Else']);

        self::assertEquals('Something Else', $role->title);
    }

    /**
     * @test
     */
    public function role_title_is_capitalized()
    {
        $role = Role::create(['name' => 'admin']);

        self::assertEquals('Admin', $role->title);
    }

    /**
     * @test
     */
    public function role_title_with_spaces()
    {
        $role = Role::create(['name' => 'site admin']);

        self::assertEquals('Site admin', $role->title);
    }

    /**
     * @test
     */
    public function role_title_with_dashes()
    {
        $role = Role::create(['name' => 'site-admin']);

        self::assertEquals('Site admin', $role->title);
    }

    /**
     * @test
     */
    public function role_title_with_underscores()
    {
        $role = Role::create(['name' => 'site_admin']);

        self::assertEquals('Site admin', $role->title);
    }

    /**
     * @test
     */
    public function role_title_with_camel_casing()
    {
        $role = Role::create(['name' => 'siteAdmin']);

        self::assertEquals('Site admin', $role->title);
    }

    /**
     * @test
     */
    public function role_title_with_studly_casing()
    {
        $role = Role::create(['name' => 'SiteAdmin']);

        self::assertEquals('Site admin', $role->title);
    }

    /**
     * @test
     */
    public function ability_title_is_never_overwritten()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->allow($user)->to('ban-users', null, [
            'title' => 'Something Else',
        ]);

        self::assertEquals('Something Else', $rbac->ability()->first()->title);
    }

    /**
     * @test
     */
    public function ability_title_is_set_for_wildcards()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->allow($user)->everything();

        self::assertEquals('All abilities', $rbac->ability()->first()->title);
    }

    /**
     * @test
     */
    public function ability_title_is_set_for_restricted_wildcards()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->allow($user)->to('*');

        self::assertEquals('All simple abilities', $rbac->ability()->first()->title);
    }

    /**
     * @test
     */
    public function ability_title_is_set_for_simple_abilities()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->allow($user)->to('ban-users');

        self::assertEquals('Ban users', $rbac->ability()->first()->title);
    }

    /**
     * @test
     */
    public function ability_title_is_set_for_blanket_ownership_ability()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->allow($user)->toOwnEverything();

        self::assertEquals('Manage everything owned', $rbac->ability()->first()->title);
    }

    /**
     * @test
     */
    public function ability_title_is_set_for_restricted_ownership_ability()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->allow($user)->toOwnEverything()->to('edit');

        self::assertEquals('Edit everything owned', $rbac->ability()->first()->title);
    }

    /**
     * @test
     */
    public function ability_title_is_set_for_management_ability()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->allow($user)->toManage(User::class);

        self::assertEquals('Manage users', $rbac->ability()->first()->title);
    }

    /**
     * @test
     */
    public function ability_title_is_set_for_blanket_model_ability()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->allow($user)->to('create', User::class);

        self::assertEquals('Create users', $rbac->ability()->first()->title);
    }

    /**
     * @test
     */
    public function ability_title_is_set_for_regular_model_ability()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->allow($user)->to('delete', User::create());

        self::assertEquals('Delete user #2', $rbac->ability()->first()->title);
    }

    /**
     * @test
     */
    public function ability_title_is_set_for_a_global_action_ability()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->allow($user)->to('delete')->everything();

        self::assertEquals('Delete everything', $rbac->ability()->first()->title);
    }
}
