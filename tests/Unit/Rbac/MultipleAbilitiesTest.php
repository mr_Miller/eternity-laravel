<?php

namespace Eternity\Laravel\Tests\Unit\Rbac;

class MultipleAbilitiesTest extends BaseTestCase
{
    use Concerns\TestsClipboards;

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function allowing_multiple_abilities($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->to(['edit', 'delete']);

        self::assertTrue($rbac->can('edit'));
        self::assertTrue($rbac->can('delete'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function allowing_multiple_model_abilities($provider)
    {
        [$rbac, $user1, $user2] = $provider(2);

        $rbac->allow($user1)->to(['edit', 'delete'], $user1);

        self::assertTrue($rbac->can('edit', $user1));
        self::assertTrue($rbac->can('delete', $user1));
        self::assertTrue($rbac->cannot('edit', $user2));
        self::assertTrue($rbac->cannot('delete', $user2));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function allowing_multiple_blanket_model_abilities($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->to(['edit', 'delete'], User::class);

        self::assertTrue($rbac->can('edit', User::class));
        self::assertTrue($rbac->can('delete', User::class));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function allowing_an_ability_on_multiple_models($provider)
    {
        [$rbac, $user1, $user2] = $provider(2);

        $rbac->allow($user1)->to('delete', [Account::class, $user1]);

        self::assertTrue($rbac->can('delete', Account::class));
        self::assertTrue($rbac->can('delete', $user1));
        self::assertTrue($rbac->cannot('delete', $user2));
        self::assertTrue($rbac->cannot('delete', User::class));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function allowing_multiple_abilities_on_multiple_models($provider)
    {
        [$rbac, $user1, $user2] = $provider(2);

        $rbac->allow($user1)->to(['update', 'delete'], [Account::class, $user1]);

        self::assertTrue($rbac->can('update', Account::class));
        self::assertTrue($rbac->can('delete', Account::class));
        self::assertTrue($rbac->can('update', $user1));
        self::assertTrue($rbac->cannot('update', $user2));
        self::assertTrue($rbac->cannot('update', User::class));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function allowing_multiple_abilities_via_a_map($provider)
    {
        [$rbac, $user1, $user2] = $provider(2);

        $account1 = Account::create();
        $account2 = Account::create();

        $rbac->allow($user1)->to([
            'edit'   => User::class,
            'delete' => $user1,
            'view'   => Account::class,
            'update' => $account1,
            'access-dashboard',
        ]);

        self::assertTrue($rbac->can('edit', User::class));
        self::assertTrue($rbac->cannot('view', User::class));
        self::assertTrue($rbac->can('delete', $user1));
        self::assertTrue($rbac->cannot('delete', $user2));

        self::assertTrue($rbac->can('view', Account::class));
        self::assertTrue($rbac->cannot('update', Account::class));
        self::assertTrue($rbac->can('update', $account1));
        self::assertTrue($rbac->cannot('update', $account2));

        self::assertTrue($rbac->can('access-dashboard'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function disallowing_multiple_abilties($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->to(['edit', 'delete']);
        $rbac->disallow($user)->to(['edit', 'delete']);

        self::assertTrue($rbac->cannot('edit'));
        self::assertTrue($rbac->cannot('delete'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function disallowing_multiple_model_abilities($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->to(['view', 'edit', 'delete'], $user);
        $rbac->disallow($user)->to(['edit', 'delete'], $user);

        self::assertTrue($rbac->can('view', $user));
        self::assertTrue($rbac->cannot('edit', $user));
        self::assertTrue($rbac->cannot('delete', $user));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function disallowing_multiple_blanket_model_abilities($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->to(['edit', 'delete'], User::class);
        $rbac->disallow($user)->to(['edit', 'delete'], User::class);

        self::assertTrue($rbac->cannot('edit', User::class));
        self::assertTrue($rbac->cannot('delete', User::class));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function disallowing_multiple_abilities_via_a_map($provider)
    {
        [$rbac, $user1, $user2] = $provider(2);

        $account1 = Account::create();
        $account2 = Account::create();

        $rbac->allow($user1)->to([
            'edit'   => User::class,
            'delete' => $user1,
            'view'   => Account::class,
            'update' => $account1,
        ]);

        $rbac->disallow($user1)->to([
            'edit'   => User::class,
            'update' => $account1,
        ]);

        self::assertTrue($rbac->cannot('edit', User::class));
        self::assertTrue($rbac->can('delete', $user1));
        self::assertTrue($rbac->can('view', $account1));
        self::assertTrue($rbac->cannot('update', $account1));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function forbidding_multiple_abilities($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->to(['edit', 'delete']);
        $rbac->forbid($user)->to(['edit', 'delete']);

        self::assertTrue($rbac->cannot('edit'));
        self::assertTrue($rbac->cannot('delete'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function forbidding_multiple_model_abilities($provider)
    {
        [$rbac, $user1, $user2] = $provider(2);

        $rbac->allow($user1)->to(['view', 'edit', 'delete']);
        $rbac->allow($user1)->to(['view', 'edit', 'delete'], $user1);
        $rbac->allow($user1)->to(['view', 'edit', 'delete'], $user2);
        $rbac->forbid($user1)->to(['edit', 'delete'], $user1);

        self::assertTrue($rbac->can('view'));
        self::assertTrue($rbac->can('edit'));

        self::assertTrue($rbac->can('view', $user1));
        self::assertTrue($rbac->cannot('edit', $user1));
        self::assertTrue($rbac->cannot('delete', $user1));
        self::assertTrue($rbac->can('edit', $user2));
        self::assertTrue($rbac->can('delete', $user2));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function forbidding_multiple_blanket_model_abilities($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->to(['edit', 'delete']);
        $rbac->allow($user)->to(['edit', 'delete'], Account::class);
        $rbac->allow($user)->to(['view', 'edit', 'delete'], User::class);
        $rbac->forbid($user)->to(['edit', 'delete'], User::class);

        self::assertTrue($rbac->can('edit'));
        self::assertTrue($rbac->can('delete'));

        self::assertTrue($rbac->can('edit', Account::class));
        self::assertTrue($rbac->can('delete', Account::class));

        self::assertTrue($rbac->can('view', User::class));
        self::assertTrue($rbac->cannot('edit', User::class));
        self::assertTrue($rbac->cannot('delete', User::class));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function forbidding_multiple_abilities_via_a_map($provider)
    {
        [$rbac, $user1, $user2] = $provider(2);

        $account1 = Account::create();
        $account2 = Account::create();

        $rbac->allow($user1)->to([
            'edit'   => User::class,
            'delete' => $user1,
            'view'   => Account::class,
            'update' => $account1,
        ]);

        $rbac->forbid($user1)->to([
            'edit'   => User::class,
            'update' => $account1,
        ]);

        self::assertTrue($rbac->cannot('edit', User::class));
        self::assertTrue($rbac->can('delete', $user1));
        self::assertTrue($rbac->can('view', $account1));
        self::assertTrue($rbac->cannot('update', $account1));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function unforbidding_multiple_abilities($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->to(['view', 'edit', 'delete']);
        $rbac->forbid($user)->to(['view', 'edit', 'delete']);
        $rbac->unforbid($user)->to(['edit', 'delete']);

        self::assertTrue($rbac->cannot('view'));
        self::assertTrue($rbac->can('edit'));
        self::assertTrue($rbac->can('delete'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function unforbidding_multiple_model_abilities($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->to(['view', 'edit', 'delete'], $user);
        $rbac->forbid($user)->to(['view', 'edit', 'delete'], $user);
        $rbac->unforbid($user)->to(['edit', 'delete'], $user);

        self::assertTrue($rbac->cannot('view', $user));
        self::assertTrue($rbac->can('edit', $user));
        self::assertTrue($rbac->can('delete', $user));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function unforbidding_multiple_blanket_model_abilities($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->to(['view', 'edit', 'delete'], User::class);
        $rbac->forbid($user)->to(['view', 'edit', 'delete'], User::class);
        $rbac->unforbid($user)->to(['edit', 'delete'], User::class);

        self::assertTrue($rbac->cannot('view', User::class));
        self::assertTrue($rbac->can('edit', User::class));
        self::assertTrue($rbac->can('delete', User::class));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function unforbidding_multiple_abilities_via_a_map($provider)
    {
        [$rbac, $user1, $user2] = $provider(2);

        $account1 = Account::create();
        $account2 = Account::create();

        $rbac->allow($user1)->to([
            'edit'   => User::class,
            'delete' => $user1,
            'view'   => Account::class,
            'update' => $account1,
        ]);

        $rbac->forbid($user1)->to([
            'edit'   => User::class,
            'delete' => $user1,
            'view'   => Account::class,
            'update' => $account1,
        ]);

        $rbac->unforbid($user1)->to([
            'edit'   => User::class,
            'update' => $account1,
        ]);

        self::assertTrue($rbac->can('edit', User::class));
        self::assertTrue($rbac->cannot('delete', $user1));
        self::assertTrue($rbac->cannot('view', $account1));
        self::assertTrue($rbac->can('update', $account1));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function passthru_can_any_check($provider)
    {
        list($bouncer, $user1) = $provider();

        $user1->allow('create', User::class);

        self::assertTrue($bouncer->canAny(['create', 'delete'], User::class));
        self::assertFalse($bouncer->canAny(['update', 'delete'], User::class));
    }
}
