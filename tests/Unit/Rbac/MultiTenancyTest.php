<?php

namespace Eternity\Laravel\Tests\Unit\Rbac;

use Eternity\Laravel\Rbac\Contracts\Scope as ScopeContract;
use Eternity\Laravel\Rbac\Database\Models;
use Eternity\Laravel\Rbac\Database\Scope\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class MultiTenancyTest extends BaseTestCase
{
    use Concerns\TestsClipboards;

    /**
     * Reset any scopes that have been applied in a test.
     *
     * @return void
     */
    public function tearDown(): void
    {
        Models::scope(new Scope);

        parent::tearDown();
    }

    /**
     * @test
     */
    public function can_set_and_get_the_current_scope()
    {
        $rbac = $this->inspector();

        self::assertNull($rbac->scope()->get());

        $rbac->scope()->to(1);
        self::assertEquals(1, $rbac->scope()->get());
    }

    /**
     * @test
     */
    public function can_remove_the_current_scope()
    {
        $rbac = $this->inspector();

        $rbac->scope()->to(1);
        self::assertEquals(1, $rbac->scope()->get());

        $rbac->scope()->remove();
        self::assertNull($rbac->scope()->get());
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function creating_roles_and_abilities_automatically_scopes_them($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->scope()->to(1);

        $rbac->allow('admin')->to('create', User::class);
        $rbac->assign('admin')->to($user);

        self::assertEquals(1, $rbac->ability()->query()->value('scope'));
        self::assertEquals(1, $rbac->role()->query()->value('scope'));
        self::assertEquals(1, $this->db()->table('permissions')->value('scope'));
        self::assertEquals(1, $this->db()->table('assigned_roles')->value('scope'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function syncing_roles_is_properly_scoped($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->scope()->to(1);
        $rbac->assign(['writer', 'reader'])->to($user);

        $rbac->scope()->to(2);
        $rbac->assign(['eraser', 'thinker'])->to($user);

        $rbac->scope()->to(1);
        $rbac->sync($user)->roles(['writer']);

        self::assertTrue($rbac->is($user)->a('writer'));
        self::assertEquals(1, $user->roles()->count());

        $rbac->scope()->to(2);
        self::assertTrue($rbac->is($user)->all('eraser', 'thinker'));
        self::assertFalse($rbac->is($user)->a('writer', 'reader'));

        $rbac->sync($user)->roles(['thinker']);

        self::assertTrue($rbac->is($user)->a('thinker'));
        self::assertEquals(1, $user->roles()->count());
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function syncing_abilities_is_properly_scoped($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->scope()->to(1);
        $rbac->allow($user)->to(['write', 'read']);

        $rbac->scope()->to(2);
        $rbac->allow($user)->to(['erase', 'think']);

        $rbac->scope()->to(1);
        $rbac->sync($user)->abilities(['write', 'color']); // "read" is not deleted

        self::assertTrue($rbac->can('write'));
        self::assertEquals(2, $user->abilities()->count());

        $rbac->scope()->to(2);
        self::assertTrue($rbac->can('erase'));
        self::assertTrue($rbac->can('think'));
        self::assertFalse($rbac->can('write'));
        self::assertFalse($rbac->can('read'));

        $rbac->sync($user)->abilities(['think']);

        self::assertTrue($rbac->can('think'));
        self::assertEquals(1, $user->abilities()->count());
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function relation_queries_are_properly_scoped($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->scope()->to(1);
        $rbac->allow($user)->to('create', User::class);

        $rbac->scope()->to(2);
        $rbac->allow($user)->to('delete', User::class);

        $rbac->scope()->to(1);
        $abilities = $user->abilities()->get();

        self::assertCount(1, $abilities);
        self::assertEquals(1, $abilities->first()->scope);
        self::assertEquals('create', $abilities->first()->name);
        self::assertTrue($rbac->can('create', User::class));
        self::assertTrue($rbac->cannot('delete', User::class));

        $rbac->scope()->to(2);
        $abilities = $user->abilities()->get();

        self::assertCount(1, $abilities);
        self::assertEquals(2, $abilities->first()->scope);
        self::assertEquals('delete', $abilities->first()->name);
        self::assertTrue($rbac->can('delete', User::class));
        self::assertTrue($rbac->cannot('create', User::class));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function relation_queries_can_be_scoped_exclusively($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->scope()->to(1)->onlyRelations();
        $rbac->allow($user)->to('create', User::class);

        $rbac->scope()->to(2);
        $rbac->allow($user)->to('delete', User::class);

        $rbac->scope()->to(1);
        $abilities = $user->abilities()->get();

        self::assertCount(1, $abilities);
        self::assertNull($abilities->first()->scope);
        self::assertEquals('create', $abilities->first()->name);
        self::assertTrue($rbac->can('create', User::class));
        self::assertTrue($rbac->cannot('delete', User::class));

        $rbac->scope()->to(2);
        $abilities = $user->abilities()->get();

        self::assertCount(1, $abilities);
        self::assertNull($abilities->first()->scope);
        self::assertEquals('delete', $abilities->first()->name);
        self::assertTrue($rbac->can('delete', User::class));
        self::assertTrue($rbac->cannot('create', User::class));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function scoping_also_returns_global_abilities($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->to('create', User::class);

        $rbac->scope()->to(1)->onlyRelations();
        $rbac->allow($user)->to('delete', User::class);

        $abilities = $user->abilities()->orderBy('id')->get();

        self::assertCount(2, $abilities);
        self::assertNull($abilities->first()->scope);
        self::assertEquals('create', $abilities->first()->name);
        self::assertTrue($rbac->can('create', User::class));
        self::assertTrue($rbac->can('delete', User::class));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function forbidding_abilities_only_affects_the_current_scope($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->scope()->to(1);
        $rbac->allow($user)->to('create', User::class);

        $rbac->scope()->to(2);
        $rbac->allow($user)->to('create', User::class);
        $rbac->forbid($user)->to('create', User::class);

        $rbac->scope()->to(1);

        self::assertTrue($rbac->can('create', User::class));

        $rbac->unforbid($user)->to('create', User::class);

        $rbac->scope()->to(2);

        self::assertTrue($rbac->cannot('create', User::class));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function disallowing_abilities_only_affects_the_current_scope($provider)
    {
        [$rbac, $user] = $provider();

        $admin = $rbac->role()->create(['name' => 'admin']);
        $user->assign($admin);

        $rbac->scope()->to(1)->onlyRelations();
        $admin->allow('create', User::class);

        $rbac->scope()->to(2);
        $admin->allow('create', User::class);
        $admin->disallow('create', User::class);

        $rbac->scope()->to(1);

        self::assertTrue($rbac->can('create', User::class));

        $rbac->scope()->to(2);

        self::assertTrue($rbac->cannot('create', User::class));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function unforbidding_abilities_only_affects_the_current_scope($provider)
    {
        [$rbac, $user] = $provider();

        $admin = $rbac->role()->create(['name' => 'admin']);
        $user->assign($admin);

        $rbac->scope()->to(1)->onlyRelations();
        $admin->allow()->everything();
        $admin->forbid()->to('create', User::class);

        $rbac->scope()->to(2);
        $admin->allow()->everything();
        $admin->forbid()->to('create', User::class);
        $admin->unforbid()->to('create', User::class);

        $rbac->scope()->to(1);

        self::assertTrue($rbac->cannot('create', User::class));

        $rbac->scope()->to(2);

        self::assertTrue($rbac->can('create', User::class));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function assigning_and_retracting_roles_scopes_them_properly($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->scope()->to(1)->onlyRelations();
        $rbac->assign('admin')->to($user);

        $rbac->scope()->to(2);
        $rbac->assign('admin')->to($user);
        $rbac->retract('admin')->from($user);

        $rbac->scope()->to(1);
        self::assertTrue($rbac->is($user)->an('admin'));

        $rbac->scope()->to(2);
        self::assertFalse($rbac->is($user)->an('admin'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function role_abilities_can_be_excluded_from_scopes($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->scope()->to(1)
                ->onlyRelations()
                ->dontScopeRoleAbilities();

        $rbac->allow('admin')->to('delete', User::class);

        $rbac->scope()->to(2);

        $rbac->assign('admin')->to($user);

        self::assertTrue($rbac->can('delete', User::class));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_set_custom_scope($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->scope(new MultiTenancyNullScopeStub)->to(1);

        $rbac->allow($user)->to('delete', User::class);

        $rbac->scope()->to(2);

        self::assertTrue($rbac->can('delete', User::class));
    }

    /**
     * @test
     */
    public function can_set_the_scope_temporarily()
    {
        $rbac = $this->inspector();

        self::assertNull($rbac->scope()->get());

        $result = $rbac->scope()->onceTo(1, function () use ($rbac) {
            self::assertEquals(1, $rbac->scope()->get());

            return 'result';
        });

        self::assertEquals('result', $result);
        self::assertNull($rbac->scope()->get());
    }

    /**
     * @test
     */
    public function can_remove_the_scope_temporarily()
    {
        $rbac = $this->inspector();

        $rbac->scope()->to(1);

        $result = $rbac->scope()->removeOnce(function () use ($rbac) {
            self::assertEquals(null, $rbac->scope()->get());

            return 'result';
        });

        self::assertEquals('result', $result);
        self::assertEquals(1, $rbac->scope()->get());
    }
}



class MultiTenancyNullScopeStub implements ScopeContract
{
    public function to()
    {
        //
    }

    public function appendToCacheKey($key): string
    {
        return $key;
    }

    public function applyToModel(Model $model): Model
    {
        return $model;
    }

    public function applyToModelQuery($query, $table = null)
    {
        return $query;
    }

    public function applyToRelationQuery($query, $table)
    {
        return $query;
    }

    public function applyToRelation(BelongsToMany $relation): BelongsToMany
    {
        return $relation;
    }

    public function get()
    {
        return null;
    }

    public function getAttachAttributes($authority = null): array
    {
        return [];
    }

    public function onceTo($scope, callable $callback)
    {
        //
    }

    public function remove(): void
    {
        //
    }

    public function removeOnce(callable $callback)
    {
        //
    }
}
