<?php

namespace Eternity\Laravel\Tests\Unit\Rbac\Broadcast;

use Eternity\Laravel\Rbac\Broadcast\Ability;
use Eternity\Laravel\Rbac\Broadcast\Rbac;
use PHPUnit\Framework\TestCase;
use stdClass;

/**
 * @group Rbac
 * Class RbacTest
 * @package Eternity\Laravel\Tests\Unit\Rbac\Broadcast
 */
class RbacTest extends TestCase
{
    /**
     * @var array
     */
    private $rbac;

    /**
     *
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->rbac = [
            'roles'     => ['user', 'admin'],
            'abilities' => [
                'view' => 'Description'
            ]
        ];
    }

    public function testConstructAbilitiesSuccess(): void
    {
        $abilities = [
            'view' => 'Description'
        ];
        self::assertEquals([new Ability('view', 'Description')], Rbac::constructAbilities($abilities));
    }

    public function testSerialize()
    {
        $serializedString = json_encode($this->rbac);
        self::assertEquals(Rbac::deserialize($serializedString)->serialize(), $serializedString);
    }

    /**
     * @throws \JsonException
     */
    public function testEncrypt()
    {
        $serializedString = json_encode($this->rbac);
        $encrypted = Rbac::deserialize($serializedString)->encrypt();
        self::assertEquals(Rbac::decrypt($encrypted)->serialize(), $serializedString);
    }

    public function authorizedDataProvider(): array
    {
        return [
            ['view'],
            [['view']],
        ];
    }

    /**
     * @dataProvider authorizedDataProvider
     * @param $abilities
     */
    public function testIsAuthorizedFor($abilities): void
    {
        $rbac = Rbac::deserialize(json_encode($this->rbac));
        self::assertTrue($rbac->isAuthorizedFor($abilities));
    }

    public function testIsAuthorizedForThrowsException()
    {
        $rbac = Rbac::deserialize(json_encode($this->rbac));
        $this->expectException(\InvalidArgumentException::class);
        $rbac->isAuthorizedFor(new stdClass());
    }

    public function hasRolesDataProvider(): array
    {
        return [
            ['user'],
            ['admin'],
            [['user','admin']]
        ];
    }

    /**
     * @dataProvider hasRolesDataProvider
     * @param $roles
     */
    public function testHasRoles($roles): void
    {
        $rbac = Rbac::deserialize(json_encode($this->rbac));
        $this->assertTrue($rbac->hasRole($roles));
    }

    public function testToArraySuccess()
    {
        $rbac = Rbac::deserialize(json_encode($this->rbac));
        $this->assertEquals($rbac->toArray(), $this->rbac);
    }
}