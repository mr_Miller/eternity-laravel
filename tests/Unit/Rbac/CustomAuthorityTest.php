<?php

namespace Eternity\Laravel\Tests\Unit\Rbac;

class CustomAuthorityTest extends BaseTestCase
{
    use Concerns\TestsClipboards;

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_give_and_remove_abilities($provider)
    {
        [$rbac, $account] = $provider(1, Account::class);

        $rbac->allow($account)->to('edit-site');

        self::assertTrue($rbac->can('edit-site'));

        $rbac->disallow($account)->to('edit-site');

        self::assertTrue($rbac->cannot('edit-site'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_give_and_remove_roles($provider)
    {
        [$rbac, $account] = $provider(1, Account::class);

        $rbac->allow('admin')->to('edit-site');
        $rbac->assign('admin')->to($account);

        $editor = $rbac->role()->create(['name' => 'editor']);
        $rbac->allow($editor)->to('edit-site');
        $rbac->assign($editor)->to($account);

        self::assertTrue($rbac->can('edit-site'));

        $rbac->retract('admin')->from($account);
        $rbac->retract($editor)->from($account);

        self::assertTrue($rbac->cannot('edit-site'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_disallow_abilities_on_roles($provider)
    {
        [$rbac, $account] = $provider(1, Account::class);

        $rbac->allow('admin')->to('edit-site');
        $rbac->disallow('admin')->to('edit-site');
        $rbac->assign('admin')->to($account);

        self::assertTrue($rbac->cannot('edit-site'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_check_roles($provider)
    {
        [$rbac, $account] = $provider(1, Account::class);

        self::assertTrue($rbac->is($account)->notA('moderator'));
        self::assertTrue($rbac->is($account)->notAn('editor'));
        self::assertFalse($rbac->is($account)->an('admin'));

        $rbac = $this->inspector($account = Account::create());

        $rbac->assign('moderator')->to($account);
        $rbac->assign('editor')->to($account);

        self::assertTrue($rbac->is($account)->a('moderator'));
        self::assertTrue($rbac->is($account)->an('editor'));
        self::assertFalse($rbac->is($account)->notAn('editor'));
        self::assertFalse($rbac->is($account)->an('admin'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function can_check_multiple_roles($provider)
    {
        [$rbac, $account] = $provider(1, Account::class);

        self::assertTrue($rbac->is($account)->notAn('editor', 'moderator'));
        self::assertTrue($rbac->is($account)->notAn('admin', 'moderator'));

        $rbac = $this->inspector($account = Account::create());
        $rbac->assign('moderator')->to($account);
        $rbac->assign('editor')->to($account);

        self::assertTrue($rbac->is($account)->a('subscriber', 'moderator'));
        self::assertTrue($rbac->is($account)->an('admin', 'editor'));
        self::assertTrue($rbac->is($account)->all('editor', 'moderator'));
        self::assertFalse($rbac->is($account)->notAn('editor', 'moderator'));
        self::assertFalse($rbac->is($account)->all('admin', 'moderator'));
    }
}
