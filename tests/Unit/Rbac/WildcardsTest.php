<?php

namespace Eternity\Laravel\Tests\Unit\Rbac;

class WildcardsTest extends BaseTestCase
{
    use Concerns\TestsClipboards;

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function a_wildard_ability_allows_everything($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->to('*');

        self::assertTrue($rbac->can('edit-site'));
        self::assertTrue($rbac->can('*'));

        $rbac->disallow($user)->to('*');

        self::assertTrue($rbac->cannot('edit-site'));
        self::assertTrue($rbac->cannot('*'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function manage_allows_all_actions_on_a_model($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->toManage($user);

        self::assertTrue($rbac->can('*', $user));
        self::assertTrue($rbac->can('edit', $user));
        self::assertTrue($rbac->cannot('*', User::class));
        self::assertTrue($rbac->cannot('edit', User::class));

        $rbac->disallow($user)->toManage($user);

        self::assertTrue($rbac->cannot('*', $user));
        self::assertTrue($rbac->cannot('edit', $user));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function manage_on_a_model_class_allows_all_actions_on_all_its_models($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->toManage(User::class);

        self::assertTrue($rbac->can('*', $user));
        self::assertTrue($rbac->can('edit', $user));
        self::assertTrue($rbac->can('*', User::class));
        self::assertTrue($rbac->can('edit', User::class));
        self::assertTrue($rbac->cannot('edit', Account::class));
        self::assertTrue($rbac->cannot('edit', Account::class));

        $rbac->disallow($user)->toManage(User::class);

        self::assertTrue($rbac->cannot('*', $user));
        self::assertTrue($rbac->cannot('edit', $user));
        self::assertTrue($rbac->cannot('*', User::class));
        self::assertTrue($rbac->cannot('edit', User::class));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function always_allows_the_action_on_all_models($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->to('delete')->everything();

        self::assertTrue($rbac->can('delete', $user));
        self::assertTrue($rbac->cannot('update', $user));
        self::assertTrue($rbac->can('delete', User::class));
        self::assertTrue($rbac->can('delete', '*'));

        $rbac->disallow($user)->to('delete')->everything();

        self::assertTrue($rbac->cannot('delete', $user));
        self::assertTrue($rbac->cannot('delete', User::class));
        self::assertTrue($rbac->cannot('delete', '*'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function everything_allows_everything($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->everything();

        self::assertTrue($rbac->can('*'));
        self::assertTrue($rbac->can('*', '*'));
        self::assertTrue($rbac->can('*', $user));
        self::assertTrue($rbac->can('*', User::class));
        self::assertTrue($rbac->can('ban', '*'));
        self::assertTrue($rbac->can('ban-users'));
        self::assertTrue($rbac->can('ban', $user));
        self::assertTrue($rbac->can('ban', User::class));

        $rbac->disallow($user)->everything();

        self::assertTrue($rbac->cannot('*'));
        self::assertTrue($rbac->cannot('*', '*'));
        self::assertTrue($rbac->cannot('*', $user));
        self::assertTrue($rbac->cannot('*', User::class));
        self::assertTrue($rbac->cannot('ban', '*'));
        self::assertTrue($rbac->cannot('ban-users'));
        self::assertTrue($rbac->cannot('ban', $user));
        self::assertTrue($rbac->cannot('ban', User::class));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function a_simple_wildard_ability_denies_model_abilities($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->to('*');

        self::assertTrue($rbac->cannot('edit', $user));
        self::assertTrue($rbac->cannot('edit', User::class));
        self::assertTrue($rbac->cannot('*', $user));
        self::assertTrue($rbac->cannot('*', User::class));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function manage_denies_simple_abilities($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->toManage($user);

        self::assertTrue($rbac->cannot('edit'));
        self::assertTrue($rbac->cannot('*'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function manage_on_a_model_class_denies_simple_abilities($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->toManage(User::class);

        self::assertTrue($rbac->cannot('*'));
        self::assertTrue($rbac->cannot('edit'));
    }

    /**
     * @test
     * @dataProvider rbacProvider
     */
    public function always_denies_simple_abilities($provider)
    {
        [$rbac, $user] = $provider();

        $rbac->allow($user)->to('delete')->everything();

        self::assertTrue($rbac->cannot('delete'));
    }
}
