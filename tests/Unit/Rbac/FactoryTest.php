<?php

namespace Eternity\Laravel\Tests\Unit\Rbac;

use Eternity\Laravel\Rbac\Inspector;
use Illuminate\Auth\Access\Gate;
use Illuminate\Container\Container;

class FactoryTest extends BaseTestCase
{
    /**
     * @test
     */
    public function can_create_default_rbac_instance()
    {
        $rbac = Inspector::create();

        self::assertInstanceOf(Inspector::class, $rbac);
        self::assertInstanceOf(Gate::class, $rbac->getGate());
        self::assertTrue($rbac->usesCachedClipboard());
    }

    /**
     * @test
     */
    public function can_create_rbac_instance_for_given_the_user()
    {
        $rbac = Inspector::create($user = User::create());

        $rbac->allow($user)->to('create-capabilities');

        self::assertTrue($rbac->can('create-capabilities'));
        self::assertTrue($rbac->cannot('delete-capabilities'));
    }

    /**
     * @test
     */
    public function can_build_up_rbac_with_the_given_user()
    {
        $rbac = Inspector::make()->withUser($user = User::create())->create();

        $rbac->allow($user)->to('create-capabilities');

        self::assertTrue($rbac->can('create-capabilities'));
        self::assertTrue($rbac->cannot('delete-capabilities'));
    }

    /**
     * @test
     */
    public function can_build_up_rbac_with_the_given_gate()
    {
        $user = User::create();

        $gate = new Gate(new Container, function () use ($user) {
            return $user;
        });

        $rbac = Inspector::make()->withGate($gate)->create();

        $rbac->allow($user)->to('create-capabilities');

        self::assertTrue($rbac->can('create-capabilities'));
        self::assertTrue($rbac->cannot('delete-capabilities'));
    }
}
