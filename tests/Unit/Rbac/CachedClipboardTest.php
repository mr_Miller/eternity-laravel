<?php

namespace Eternity\Laravel\Tests\Unit\Rbac;

use Eternity\Laravel\Rbac\Clipboards\CachedClipboard;
use Illuminate\Cache\ArrayStore;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class CachedClipboardTest extends BaseTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->clipboard = new CachedClipboard(new ArrayStore);
    }

    /**
     * @test
     */
    public function it_caches_abilities()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->allow($user)->to('ban-users');

        self::assertEquals(['ban-users'], $this->getAbilities($user));

        $rbac->allow($user)->to('create-users');

        self::assertEquals(['ban-users'], $this->getAbilities($user));
    }

    /**
     * @test
     */
    public function it_caches_empty_abilities()
    {
        $user = User::create();

        self::assertInstanceOf(Collection::class, $this->clipboard->getAbilities($user));
        self::assertInstanceOf(Collection::class, $this->clipboard->getAbilities($user));
    }

    /**
     * @test
     */
    public function it_caches_roles()
    {
        $rbac = $this->inspector($user = User::create());

        $rbac->assign('editor')->to($user);

        self::assertTrue($rbac->is($user)->an('editor'));

        $rbac->assign('moderator')->to($user);

        self::assertFalse($rbac->is($user)->a('moderator'));
    }

    /**
     * @test
     */
    public function it_always_checks_roles_in_the_cache()
    {
        $rbac = $this->inspector($user = User::create());
        $admin = $rbac->role()->create(['name' => 'admin']);

        $rbac->assign($admin)->to($user);

        self::assertTrue($rbac->is($user)->an('admin'));

        $this->db()->connection()->enableQueryLog();

        self::assertTrue($rbac->is($user)->an($admin));
        self::assertTrue($rbac->is($user)->an('admin'));
        self::assertTrue($rbac->is($user)->an($admin->id));

        self::assertEmpty($this->db()->connection()->getQueryLog());

        $this->db()->connection()->disableQueryLog();
    }

    /**
     * @test
     */
    public function it_can_refresh_the_cache()
    {
        $cache = new ArrayStore;

        $rbac = $this->inspector($user = User::create());

        $rbac->allow($user)->to('create-posts');
        $rbac->assign('editor')->to($user);
        $rbac->allow('editor')->to('delete-posts');

        self::assertEquals(['create-posts', 'delete-posts'], $this->getAbilities($user));

        $rbac->disallow('editor')->to('delete-posts');
        $rbac->allow('editor')->to('edit-posts');

        self::assertEquals(['create-posts', 'delete-posts'], $this->getAbilities($user));

        $rbac->refresh();

        self::assertEquals(['create-posts', 'edit-posts'], $this->getAbilities($user));
    }

    /**
     * @test
     */
    public function it_can_refresh_the_cache_only_for_one_user()
    {
        $user1 = User::create();
        $user2 = User::create();

        $rbac = $this->inspector($user = User::create());

        $rbac->allow('admin')->to('ban-users');
        $rbac->assign('admin')->to($user1);
        $rbac->assign('admin')->to($user2);

        self::assertEquals(['ban-users'], $this->getAbilities($user1));
        self::assertEquals(['ban-users'], $this->getAbilities($user2));

        $rbac->disallow('admin')->to('ban-users');
        $rbac->refreshFor($user1);

        self::assertEquals([], $this->getAbilities($user1));
        self::assertEquals(['ban-users'], $this->getAbilities($user2));
    }

    /**
     * Get the name of all of the user's abilities.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $user
     * @return array
     */
    protected function getAbilities(Model $user)
    {
        return $user->getAbilities($user)->pluck('name')->sort()->values()->all();
    }
}
