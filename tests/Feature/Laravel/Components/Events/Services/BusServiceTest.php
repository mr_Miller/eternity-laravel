<?php

namespace Eternity\Laravel\Tests\Feature\Laravel\Components\Events\Services;

use Eternity\Events\Consumer;
use Eternity\Laravel\Components\Event\Brokers\RedisBroker;
use Eternity\Laravel\Components\Event\Services\BusService;
use Eternity\Laravel\Components\Redis\RedisManager;
use Eternity\Laravel\Tests\Feature\TestCase;
use Eternity\Microservices;

/**
 * Class BusServiceTest
 * @package Eternity\Tests\Feature\Laravel\Components\Events\Services
 */
class BusServiceTest extends TestCase
{
    /**
     * @doesNotPerformAssertions
     *
     * @throws \Eternity\Events\Exceptions\EventException
     */
    public function testHandleSuccess()
    {
        $redisManager = $this->createMock(RedisManager::class);
        $redisManager->method('setPrefix');
        $broker = $this->createMock(RedisBroker::class);
        $broker->method('send')->with('test-event-type', 'some json with data', Microservices::MARKETPLACE);
        $service = new BusService($broker, new Consumer(['test-event-type' => [Microservices::MARKETPLACE]]));
        $service->handle('test-event-type', 'some json with data');
    }
}