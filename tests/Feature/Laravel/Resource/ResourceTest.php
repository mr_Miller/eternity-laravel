<?php

namespace Eternity\Laravel\Tests\Feature\Laravel\Resource;

use Eternity\Definitions\HeadersDefinition;
use Eternity\Laravel\Tests\Feature\Laravel\Resource\Stubs\ProductResource;
use Eternity\Laravel\Tests\Feature\Laravel\Resource\Stubs\ProductService;
use Eternity\Laravel\Tests\Feature\TestCase;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

/**
 * @group LaravelResource
 * Class ResourceTest
 * @package Eternity\Laravel\Tests\Feature\Laravel\Resource
 */
class ResourceTest extends TestCase
{
    /**
     * @return \Illuminate\Http\Request
     */
    protected function getRequest()
    {
        $request = new Request();
        $request->setMethod('post');
        $request->headers->set('content-type', 'application/json');
        $request->headers->set(HeadersDefinition::ORDER_DIRECTION, 'ASC');
        $request->headers->set(HeadersDefinition::ORDER_BY, 'Some_field');
        $request->headers->set(HeadersDefinition::PAGE_CURRENT, 1);
        $request->headers->set(HeadersDefinition::PAGE_SIZE, 15);
        $request->headers->set(HeadersDefinition::EXPAND, json_encode(['product']));

        return $request;
    }

    public function testResourceExpandCacheSuccess()
    {

        $this->app->singleton(ProductService::class, function () {
            $mock = \Mockery::mock(ProductService::class);
            $mock->shouldReceive('get')->andReturn(['id' => 5, 'data'=> 'product_data'])->once();

            return $mock;
        });

        $collection = new Collection();
        $collection->add(['id' => 1, 'product_id' => 5]);
        $collection->add(['id' => 2, 'product_id' => 5]);
        $collection->add(['id' => 3, 'product_id' => 5]);
        $collection->add(['id' => 4, 'product_id' => 5]);
        $collection->add(['id' => 5, 'product_id' => 5]);

        $resource = new ProductResource($collection);
        $response = $resource->toResponse($this->getRequest());
        $payload = $response->getOriginalContent()['payload'];

        $this->assertEquals('product_data', $payload[0]['product']['data']);
        $this->assertEquals('product_data', $payload[1]['product']['data']);
        $this->assertEquals('product_data', $payload[2]['product']['data']);
        $this->assertEquals('product_data', $payload[3]['product']['data']);
        $this->assertEquals('product_data', $payload[4]['product']['data']);
    }
}