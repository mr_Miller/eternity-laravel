<?php

namespace Eternity\Laravel\Tests\Feature\Laravel\Resource\Stubs;

/**
 * Class ProductService
 * @package Eternity\Laravel\Tests\Unit\Resource\Stubs
 */
class ProductService
{
    /**
     * @param int $id
     * @return int
     */
    public function getProduct(int $id)
    {
        return 1;
    }
}