<?php

namespace Eternity\Laravel\Tests\Feature\Laravel\Resource\Stubs;

use Eternity\Laravel\Resource\Resource;

/**
 * Class CachedResource
 * @package Eternity\Laravel\Tests\Unit\Resource\Stubs
 */
class ProductResource extends Resource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = [
            'id' => 1,
        ];

        if ($this->isExpanded('product')) {
            $resource['product'] = $this->remember($this->resource['product_id'], 'product', function () {
                $productService = app(ProductService::class);

                return $productService->get($this->resource['product_id']);
            });
        }

        return $resource;
    }
}